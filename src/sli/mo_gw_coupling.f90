MODULE mo_gw_coupling
    use mo_kind, only : i4, dp

    implicit none
    private
    public :: gw_depth_basefow

CONTAINS

    SUBROUTINE gw_depth_basefow(L0_gwdepth, L0_gwbaseflow)
        use mo_sli_numbers, only: L1_gwdepth, L1_gwbaseflow

        use mo_upscaling_operators, only : upscale_harmonic_mean
        use mo_common_variables, only : level0, level1, domainMeta, l0_l1_remap
        use mo_common_constants, only : nodata_dp
        use mo_common_types, only: Grid

        real(dp), dimension(:), optional, intent(in) :: L0_gwdepth
        real(dp), dimension(:), optional, intent(in) :: L0_gwbaseflow
        integer(i4) :: iDomain
        type(Grid), pointer :: level0_iDomain
        integer(i4) :: s0, e0

        ! Upscaling
        iDomain = 1
        level0_iDomain => level0(domainMeta%L0DataFrom(iDomain))
        s0 = level0(domainMeta%L0DataFrom(iDomain))%iStart
        e0 = level0(domainMeta%L0DataFrom(iDomain))%iEnd
        if (.not. allocated(L1_gwdepth)) allocate(L1_gwdepth(level1(iDomain)%nCells))
        if (.not. allocated(L1_gwbaseflow)) allocate(L1_gwbaseflow(level1(iDomain)%nCells))

        if (present(L0_gwdepth)) then
            L1_gwdepth = 5.0_dp ! meter from surface (positive)
            L1_gwdepth(:) = upscale_harmonic_mean(l0_l1_remap(iDomain)%n_subcells, & 
                                            l0_l1_remap(iDomain)%upper_bound, &
                                            l0_l1_remap(iDomain)%lower_bound, &
                                            l0_l1_remap(iDomain)%left_bound, & 
                                            l0_l1_remap(iDomain)%right_bound, & 
                                            level0(domainMeta%L0DataFrom(iDomain))%Id, & 
                                            level0_iDomain%mask, &
                                            nodata_dp, &
                                            L0_gwdepth(s0 : e0))
        end if

        if (present(L0_gwbaseflow)) then
            L1_gwbaseflow = 0.5_dp ! mm/day
            L1_gwbaseflow(:) = upscale_harmonic_mean(l0_l1_remap(iDomain)%n_subcells, & 
                                            l0_l1_remap(iDomain)%upper_bound, &
                                            l0_l1_remap(iDomain)%lower_bound, &
                                            l0_l1_remap(iDomain)%left_bound, & 
                                            l0_l1_remap(iDomain)%right_bound, & 
                                            level0(domainMeta%L0DataFrom(iDomain))%Id, & 
                                            level0_iDomain%mask, &
                                            nodata_dp, &
                                            L0_gwbaseflow(s0 : e0))
        end if

    END SUBROUTINE gw_depth_basefow
    
END MODULE mo_gw_coupling