MODULE mo_sli_utils
    use mo_kind, only : i4, dp
    use mo_sli_numbers, only : zero, half, one, two, four, e3, thousand,  &
                              solve_type, vars, vars_met, params, Tzero, lambdaf, &
                              freezefac, Mw, gravity, Rgas, rhmin, Dva, &
                              ithermalcond, rhow, cswat, lambdas, vars_snow, nsnow_max, &
                              csice, snmin, gf, cpa, rmair, hmin, rhocp, rlambda, dpmaxr, & 
                              vars_aquifer, botbc

    implicit none
    private

    !function
    PUBLIC :: esat, Tfrz, thetalmax, zerovars, Sofh, JSoilLayer, GTfrozen, rtbis_Tfrozen

    !subroutine
    PUBLIC :: setsol, hyofh, hyofS, snow_adjust, flux, SEB, getfluxes_vp, getheatfluxes, tri, &
                massman_sparse, aquifer_props

    !soil water parameters
    PUBLIC :: sol
    TYPE(solve_type), DIMENSION(:), ALLOCATABLE :: sol

    INTERFACE getfluxes_vp
        MODULE PROCEDURE getfluxes_vp_1d
        MODULE PROCEDURE getfluxes_vp_2d
    END INTERFACE getfluxes_vp

    INTERFACE getheatfluxes
        MODULE PROCEDURE getheatfluxes_1d
        MODULE PROCEDURE getheatfluxes_2d
    END INTERFACE getheatfluxes

    INTERFACE tri
        MODULE PROCEDURE tri_1d
        MODULE PROCEDURE tri_2d
    END INTERFACE tri

    INTERFACE massman_sparse
        MODULE PROCEDURE massman_sparse_1d
        MODULE PROCEDURE massman_sparse_2d
    END INTERFACE massman_sparse

    INTERFACE generic_thomas
        MODULE PROCEDURE generic_thomas_1d
        MODULE PROCEDURE generic_thomas_2d
    END INTERFACE generic_thomas

contains

    REAL(dp) ELEMENTAL PURE FUNCTION esat(T)
        !returns sat vapour pressure curve in Pa
        USE  mo_sli_numbers, ONLY: esata, esatb, esatc

        IMPLICIT NONE

        real(dp), intent(in) :: T

        esat = esata * exp(esatb*T/(T+esatc))

    END FUNCTION esat

    REAL(dp) ELEMENTAL PURE FUNCTION Tfrz(S,he,b)
        ! determines freezing point temperature for a given moisture content
        USE mo_sli_numbers, ONLY: one, two, four, gravity, lambdaf, csol, Rgas, Tzero

        IMPLICIT NONE

        real(dp), intent(in) :: S, he, b

        if (csol > 1.e-3_dp) then
            Tfrz = (gravity*he - exp(b*log(min(S,one))) * (lambdaf + two*csol*Rgas*Tzero) + &
                    sqrt(gravity**2 * he**2 - two*gravity*he*lambdaf*exp(b*log(min(S,one))) + &
                    lambdaf*exp(two*b*log(min(S,one)))*(lambdaf + four*csol*Rgas*Tzero))) / &
                    (two*csol*Rgas*exp(b*log(min(S,one))))
        else
            Tfrz = (gravity*he*Tzero) / (-gravity*he + lambdaf*exp(b*log(max(min(S,one),0.01_dp))))
        endif

    END FUNCTION Tfrz

    REAL(dp) ELEMENTAL PURE FUNCTION thetalmax(Tin,S,he,b,thre,the)
        ! determines maximum liquid water content, given T
        USE mo_sli_numbers, ONLY: one, gravity, lambdaf, csol, Rgas, Tzero

        IMPLICIT NONE

        real(dp), intent(in) :: Tin, S, he, b, thre, the
        real(dp)             :: opsi, psi, h, T

        T    = min(Tfrz(min(S,one),he,b),Tin)
        opsi = -csol *Rgas *(T+Tzero)/gravity ! osmotic potential (m)
        psi  = lambdaf*T/(gravity*(T+Tzero))  ! matric potential in presence of ice
        h    = psi-opsi                       ! moisture potential in presence of ice

        ! thetalmax = thre*(h/he)**(-1/b) + (the-thre)
        thetalmax = thre*exp(-one/b*log(h/he)) + (the-thre)
        if (S > one) thetalmax = S * thetalmax

    END FUNCTION thetalmax

    REAL(dp) ELEMENTAL PURE FUNCTION Sofh(h,parin)
        ! Get saturation S from matric head h.
        ! Definitions of arguments:
        ! h   - matric head.
        IMPLICIT NONE

        REAL(dp),    INTENT(IN) :: h
        TYPE(params), INTENT(IN) :: parin

        ! Get saturation S from matric head h.
        ! Definitions of arguments:
        ! h   - matric head.

        ! Sofh = (h/parin%he)**(-parin%lam) ! Sofh not used much so ** not an issue
        Sofh = exp(-parin%lam*log(h/parin%he))

    END FUNCTION Sofh

    REAL(dp) ELEMENTAL PURE FUNCTION dthetalmaxdT(Tin,S,he,b,thre,the)
        ! determines derivative of thetalmax wrt T
        USE mo_sli_numbers, ONLY: gravity, lambdaf, csol, Rgas, Tzero

        IMPLICIT NONE

        real(dp), intent(in) :: Tin,S,he,b,thre,the
        real(dp)             :: dthetaldh, h, psi, PI, T, thetalmax

        T = min(Tfrz(min(S,one),he,b), Tin)
        PI  = -csol*Rgas*(T+Tzero)/gravity ! osmotic potential (m)
        psi = lambdaf*T/(gravity*(T+Tzero))! total matric potential in presence of ice
        h   = psi-PI       ! moisture potential in presence of ice

        ! thetalmax = thre*(h/he)**(-one/b) + (the-thre)
        thetalmax = thre*exp(-one/b*log(h/he)) + (the-thre)
        dthetaldh = -thetalmax/b/h

        dthetalmaxdT = dthetaldh*(lambdaf/(T+Tzero)/gravity-lambdaf*T/gravity/(T+Tzero)**2+csol*Rgas/gravity)
        
        if (S>1) dthetalmaxdT = dthetalmaxdT * S

    END FUNCTION dthetalmaxdT

    REAL(dp) ELEMENTAL PURE FUNCTION gammln(z)
        !  Uses Lanczos-type approximation to ln(gamma) for z > 0.
        !  Reference:
        !       Lanczos, C. 'A precision approximation of the gamma
        !               function', J. SIAM Numer. Anal., B, 1, 86-96, 1964.
        !  Accuracy: About 14 significant digits except for small regions
        !            in the vicinity of 1 and 2.
        !  Programmer: Alan Miller
        !              1 Creswick Street, Brighton, Vic. 3187, Australia
        !  e-mail: amiller @ bigpond.net.au
        !  Latest revision - 14 October 1996
        IMPLICIT NONE

        REAL(dp), INTENT(IN) :: z
        ! Local variables
        REAL(dp), PARAMETER :: a(9) = (/ &
            0.9999999999995183_dp, 676.5203681218835_dp, -1259.139216722289_dp, &
            771.3234287757674_dp, -176.6150291498386_dp, 12.50734324009056_dp, &
            -0.1385710331296526_dp, 0.9934937113930748E-05_dp, 0.1659470187408462E-06_dp /)
        REAL(dp), PARAMETER :: lnsqrt2pi =  0.9189385332046727_dp
        REAL(dp), PARAMETER :: sixpt5 = 6.5_dp
        REAL(dp), PARAMETER :: seven = 7.0_dp
        REAL(dp)    :: tmp, tmpgammln
        INTEGER(i4) :: j

        tmpgammln = zero
        tmp = z + seven

        DO j = 9, 2, -1
            tmpgammln = tmpgammln + a(j)/tmp
            tmp = tmp - one
        END DO

        tmpgammln = tmpgammln + a(1)
        gammln = LOG(tmpgammln) + lnsqrt2pi - (z + sixpt5) + (z - half)*LOG(z + sixpt5)
        RETURN

    END FUNCTION gammln

    REAL(dp) ELEMENTAL PURE FUNCTION gcf(a,x)

        IMPLICIT NONE

        REAL(dp), INTENT(IN) :: a,x
        INTEGER(i4), PARAMETER :: ITMAX=100
        REAL(dp),    PARAMETER :: EPS=epsilon(x)
        REAL(dp),    PARAMETER :: FPMIN=tiny(x)/EPS
        INTEGER(i4) :: i
        REAL(dp)     :: an, b, c, d, del, h

        if (x == 0.0_dp) then
            gcf=1.0_dp
            RETURN
        end if

        b = x + 1.0_dp - a
        c = 1.0_dp/FPMIN
        d = 1.0_dp/b
        h = d

        do i=1, ITMAX
            an  = -i*(i-a)
            b   = b + 2.0_dp
            d   = an*d + b
            if (abs(d)<FPMIN) d=FPMIN
            c   = b + an/c
            if (abs(c)<FPMIN) c=FPMIN
            d   = 1.0_dp/d
            del = d*c
            h   = h*del
            if (abs(del-1.0_dp) <= EPS) exit
        end do

        gcf = exp(-x + a*log(x) - gammln(a)) * h

    END FUNCTION gcf

    REAL(dp) ELEMENTAL PURE FUNCTION gser(a,x)

        IMPLICIT NONE

        REAL(dp), INTENT(IN) :: a, x
        INTEGER(i4), PARAMETER :: ITMAX=100
        REAL(dp),     PARAMETER :: EPS=epsilon(x)
        INTEGER(i4) :: n
        REAL(dp)     :: ap, del, summ

        if (x == 0.0_dp) then
            gser = 0.0_dp
            RETURN
        end if

        ap   = a
        summ = 1.0_dp/a
        del  = summ

        do n=1, ITMAX
            ap   = ap + 1.0_dp
            del  = del*x/ap
            summ = summ + del
            if (abs(del) < abs(summ)*EPS) exit
        end do
        gser=summ*exp(-x+a*log(x)-gammln(a))

    END FUNCTION gser

    REAL(dp) ELEMENTAL PURE FUNCTION igamma(a,x)
        IMPLICIT NONE
        
        REAL(dp), INTENT(IN) :: a, x
        REAL(dp) :: gln

        gln = gammln(a)

        if (x < a+1.0_dp) then
            igamma = 1.0_dp - gser(a,x)
        else
            igamma = gcf(a,x)
        end if
        
        igamma = igamma * exp(gln)

    END FUNCTION igamma

    REAL(dp) ELEMENTAL PURE FUNCTION slope_esat(T)
        !returns slope of sat vapour pressure curve in Pa K^-1
        USE mo_sli_numbers, ONLY: esata, esatb, esatc

        IMPLICIT NONE

        real(dp), intent(in) :: T
        real(dp) :: esat

        esat       = esata * exp(esatb*T/(T+esatc))
        slope_esat = esat * esatb*esatc/(T+esatc)**2

    END FUNCTION slope_esat

    REAL(dp) ELEMENTAL PURE FUNCTION JSoilLayer(T, dx, theta, csoil, rhosoil, h0, thre, the, he, b)
        ! JSsoilLayer = sensible heat + latent heat (total energy in soil layer J/m2)
        USE mo_sli_numbers, ONLY: one, csice, cswat, rhow, lambdaf

        IMPLICIT NONE

        real(dp), intent(in) :: T,  dx, theta, csoil, rhosoil, h0, thre, the, he, b
        real(dp) :: thetal, S

        S = (theta-(the-thre))/thre

        if (T<Tfrz(S,he,b)) then
            thetal = thetalmax(T,S,he,b,thre,the)
        else
            thetal = theta
        endif

        JSoilLayer =  dx*(csoil*rhosoil*T + rhow*(-lambdaf + csice*T)*(theta - thetal) + &
            cswat*rhow*T*thetal) + cswat*T*h0*rhow*(one - (theta - thetal)/thre) + &
            (h0*rhow*(-lambdaf + csice*T)*(theta - thetal)/thre)

    END FUNCTION JSoilLayer

    REAL(dp) ELEMENTAL PURE FUNCTION GTfrozen(T, J, dx, theta, csoil, rhosoil, h0, thre, the, he, b)
        ! GTfrozen = sensible heat + latent heat - total energy (should be zero)
        USE mo_sli_numbers, ONLY: csice, cswat, rhow, lambdaf

        IMPLICIT NONE

        real(dp), intent(in) :: T, J, dx, theta, csoil, rhosoil, h0, thre, the, he, b
        real(dp) :: thetal, S

        S = (theta-(the-thre))/thre

        if (T<Tfrz(min(S,one),he,b)) then
            thetal = thetalmax(T,S,he,b,thre,the)
        else
            thetal = theta
        endif

        GTfrozen = -J + dx*(csoil*rhosoil*T + rhow*(-lambdaf + csice*T)*(theta - thetal) + &
            cswat*rhow*T*thetal) + cswat*T*h0*rhow*(1 - (theta - thetal)/thre) + &
            (h0*rhow*(-lambdaf + csice*T)*(theta - thetal)/thre)

    END FUNCTION GTfrozen

    REAL(dp) ELEMENTAL PURE FUNCTION rtbis_Tfrozen(J, dxsoil, theta,csoil, rhosoil, h0, thre, the, he, b, x1, x2, xacc)

        IMPLICIT NONE

        real(dp), intent(in) :: J, dxsoil, theta, csoil, rhosoil, h0, thre, the, he, b
        REAL(dp), INTENT(IN) :: x1, x2, xacc

        INTEGER(i4), PARAMETER :: MAXIT=80
        INTEGER(i4) :: k
        REAL(dp)    :: dx, f, fmid, xmid

        fmid = GTfrozen(x2, J, dxsoil, theta, csoil, rhosoil, h0, thre, the, he, b)
        f    = GTfrozen(x1, J, dxsoil, theta, csoil, rhosoil, h0, thre, the, he, b)
        if (f < zero) then
            rtbis_Tfrozen = x1
            dx            = x2-x1
        else
            rtbis_Tfrozen = x2
            dx            = x1-x2
        end if

        do k=1, MAXIT
            dx   = dx*half
            xmid = rtbis_Tfrozen+dx
            fmid = GTfrozen(xmid, J, dxsoil, theta, csoil, rhosoil, h0, thre, the, he, b)
            if (fmid <= zero) rtbis_Tfrozen = xmid
            if (abs(fmid) < one) then
                rtbis_Tfrozen = xmid
                return
            endif
        end do

    END FUNCTION rtbis_Tfrozen

    REAL(dp) ELEMENTAL FUNCTION slope_esat_ice(T)
        !returns slope of sat vapour pressure curve in Pa K^-1
        USE mo_sli_numbers, ONLY:  esata_ice, esatb_ice, esatc_ice

        IMPLICIT NONE

        real(dp), intent(in) :: T
        real(dp) :: esat_ice

        esat_ice       = esata_ice * exp(esatb_ice*T/(T+esatc_ice))
        slope_esat_ice = esat_ice * esatb_ice*esatc_ice/(T+esatc_ice)**2

    END FUNCTION slope_esat_ice

    REAL(dp) ELEMENTAL FUNCTION esat_ice(T)
        !returns sat vapour pressure curve in Pa
        USE mo_sli_numbers, ONLY:  esata_ice, esatb_ice, esatc_ice

        IMPLICIT NONE

        real(dp), intent(in) :: T

        esat_ice = esata_ice * exp(esatb_ice*T/(T+esatc_ice))

    END FUNCTION esat_ice

    REAL(dp) ELEMENTAL PURE FUNCTION weight(parin, h, K, phi, dz)
        ! Get conductivity weighting for gravity flux calculations.
        ! Definitions of arguments:
        ! l   - land point
        ! j   - soil type no.
        ! h   - matric head.
        ! K   - conductivity.
        ! phi - MFP.
        ! dz  - flow path length.
        IMPLICIT NONE

        TYPE(params), INTENT(IN) :: parin
        REAL(dp),    INTENT(IN) :: h
        REAL(dp),    INTENT(IN) :: K
        REAL(dp),    INTENT(IN) :: phi
        REAL(dp),    INTENT(IN) :: dz
        
        LOGICAL   :: done
        REAL(dp) :: a, hz, Khz, Kz, phiz, w, x

        done = .false.
        hz   = h-gf*dz ! gf is gravity fac in direction of dz

        if (h<parin%he) then
            a = parin%lam*parin%eta
            x = -gf*dz/h

            if (a <= 3.0_dp .or. x*(a-3.0_dp) <= 4.0_dp) then ! use predetermined approx.
                w = (60.0_dp+x*(70.0_dp+10.0_dp*a+x*(16.0_dp+a*(5.0_dp+a))))/ &
                    (120.0_dp+x*(120.0_dp+x*(22.0_dp+2.0_dp*a**2)))
                done = .true.
            end if
        end if

        if (.not. done) then
            call hyofh(hz, parin%lam, parin%eta, parin%Ke, parin%he, Kz, Khz, phiz) ! accurate but slower
            if ((Kz-K) == zero) then
               w = 0.5_dp
           else
               w = -((phiz-phi)/(gf*dz)+K)/(Kz-K)
           end if
        end if

        weight = min(max(w,zero),one)

    END FUNCTION weight

    REAL(dp) ELEMENTAL PURE FUNCTION csat(T)
        !returns  sat vapour pressure curve in kg m-3
        USE mo_sli_numbers, ONLY: Tzero, Rgas, Mw, esata, esatb, esatc

        IMPLICIT NONE

        real(dp), intent(in) :: T

        csat = esata * exp(esatb*T/(T+esatc)) * Mw/Rgas/(T+Tzero)

    END FUNCTION csat

    REAL(dp) ELEMENTAL PURE FUNCTION slope_csat(T)
        !returns slope of sat vapour pressure curve in kg m-3 K-1
        USE mo_sli_numbers, ONLY: Tzero, Rgas, Mw, esata, esatb, esatc

        IMPLICIT NONE

        real(dp), intent(in) :: T
        real(dp) :: csat

        csat       = esata * exp(esatb*T/(T+esatc)) * Mw/Rgas/(T+Tzero)
        slope_csat = csat * esatb*esatc/(T+esatc)**2

    END FUNCTION slope_csat


    FUNCTION zerovars()
        ! Sets all fields of type vars to zero
    
        IMPLICIT NONE
    
        TYPE(vars) :: zerovars
    
        zerovars%isat      = 0
        zerovars%h         = zero
        zerovars%phi       = zero
        zerovars%phiS      = zero
        zerovars%K         = zero
        zerovars%KS        = zero
        zerovars%Dv        = zero
        zerovars%cvsat     = zero
        zerovars%rh        = zero
        zerovars%phiv      = zero
        zerovars%phivS     = zero
        zerovars%kH        = zero
        zerovars%kE        = zero
        zerovars%kth       = zero
        zerovars%csoil     = zero
        zerovars%eta_th    = zero
        zerovars%hS        = zero
        zerovars%rhS       = zero
        zerovars%sl        = zero
        zerovars%cv        = zero
        zerovars%cvsatT    = zero
        zerovars%cvS       = zero
        zerovars%kv        = zero
        zerovars%iice      = 0
        zerovars%thetai    = zero
        zerovars%thetal    = zero
        zerovars%phiT      = zero
        zerovars%KT        = zero
        zerovars%lambdav   = zero
        zerovars%lambdaf   = zero
        zerovars%he        = zero
        zerovars%phie      = zero
        zerovars%Ksat      = zero
        zerovars%dthetaldT = zero
        zerovars%Tfrz      = zero
        zerovars%csoileff  = zero
        zerovars%zsat      = zero
        zerovars%macropore_factor = zero
    
    END FUNCTION zerovars

    SUBROUTINE setsol(mp)

        IMPLICIT NONE
    
        INTEGER(i4), INTENT(IN) :: mp

        
        allocate(sol(mp))
    
        sol%T1      = zero
        sol%Ta      = zero
        sol%cva     = zero
        sol%Rnet    = zero
        sol%hr1     = zero
        sol%hra     = zero
        sol%Dv      = zero
        sol%gv      = zero
        sol%gh      = zero
        sol%Dh      = zero
        sol%dz      = zero
        sol%phie    = zero
        sol%he      = zero
        sol%K1      = zero
        sol%eta     = zero
        sol%lambda  = zero
        sol%Ks      = zero
        sol%lambdav = zero
    
    END SUBROUTINE setsol

    SUBROUTINE SEB(n, par, vmet, var, qprec, dx, h0, Tsoil, irec, Epot_mhm, &
                    Tsurface, G0, lE0, Epot, qsurface, qevap, qliq, qv, qh, qadv, & ! OUT
                    qyb, qTb, qlyb, qvyb, qlTb, qvTb, qhyb, qhTb, qadvyb, qadvTb) ! OUT
 
     ! Surface Energy Balance

     !***************************************************
     !     (1) Declaring Variable
     !***************************************************
        IMPLICIT NONE
    
        INTEGER(i4),                    INTENT(IN) :: n
        TYPE(params),    DIMENSION(1:n), INTENT(IN) :: par
        TYPE(vars_met),                  INTENT(IN) :: vmet
        TYPE(vars),      DIMENSION(1:n), INTENT(IN) :: var
        REAL(dp),                       INTENT(IN) :: qprec
        INTEGER(i4),                    INTENT(IN) :: irec
        REAL(dp),       DIMENSION(1:n), INTENT(IN) :: dx
        REAL(dp),                       INTENT(IN) :: h0
        REAL(dp),       DIMENSION(1:n), INTENT(IN) :: Tsoil
    
        REAL(dp),                       INTENT(OUT)           :: Tsurface, G0, lE0, Epot ! SEB
        REAL(dp),                       INTENT(OUT)           :: qsurface          ! water flux into surface
        REAL(dp),                       INTENT(OUT)           :: qevap             ! evaporative water flux
        ! liquid and vapour components of water flux from surface into soil
        REAL(dp),                       INTENT(OUT)           :: qliq, qv
        ! derivatives of water fluxes wrt moisture and T
        REAL(dp),                       INTENT(OUT)           :: qyb, qTb, qlyb, qvyb, qlTb, qvTb
        ! total and advective components of heat flux into surface
        REAL(dp),                       INTENT(OUT)           :: qh, qadv
        ! derivatives of heat fluxes wrt moiture and T
        REAL(dp),                       INTENT(OUT)           :: qhyb, qhTb, qadvyb, qadvTb

        REAL(dp),                       INTENT(INOUT) :: Epot_mhm
    
        ! local variables
        REAL(dp) :: Tsurface_pot,  Hpot, Gpot, dEpotdrha, dEpotdTs, dEpotdTsoil, dGdTa, dGdTsoil
        REAL(dp) :: E_vap, dEpot_vapdT1, E_liq
        REAL(dp) :: Kmin, Khmin, phimin
        REAL(dp) :: Tqw, dtqwdtb, rhocp1, cs
        LOGICAL   :: isEpot
    
     !***************************************************
     !     (3) No Snow Calculation
     !**************************************************
       
        !-------------------------------
        !           3.1. Call potential_evap
        !                  Pennman-Monteith equation, 
        !                  with additional account for heat flux into the surface
        !------------------------------ 
        ! Epot_mhm = Epot_mhm * (thousand*var(1)%lambdav) ! conversion from m/s to [J/(m^2·s)]

        call potential_evap( & !>> INPUT..............................
                            vmet%Rn, & ! net radiation absorbed by soil 
                            vmet%rbh, & ! resistance to heat transfer beween surface and lowest atmospheric layer
                            vmet%rbw, & ! resistance to water vapour transfer beween surface and lowest atmospheric layer
                            vmet%Ta, & ! air temperature --> observed Tmean dailiy
                            vmet%rha, & ! relative humidity
                            Tsoil(1), & ! soil temperature at surface (first layer)
                            var(1)%kth, & ! thermal conductivity at surface
                            half*dx(1)+h0, &  ! middle layer (dz) at surface (if any pond, h0)
                            var(1)%lambdav, &  ! rlambda   = 2.442e6_dp, latent heat of condensation at 25 degC [J/kg]
                            Epot_mhm, &
                            !>> ....................................
                            !>> OUTPUT
                            Tsurface_pot, & ! potential surface temperature 
                            Epot, & ! potential evaporation
                            Hpot, & ! potential heat at surface
                            Gpot, & ! potential Belinda's stomatal model intercept
                            ! >> Derivative 
                            dEpotdrha, & ! Derivative of Epot wrt rha (relative humidity)
                            dEpotdTs, & ! Derivative of Epot wrt Ts (surface temperature)
                            dEpotdTsoil, & ! Derivative of Epot wrt Tsoil (soil temperature)
                            dGdTa, & ! Derivative of G wrt Ta (air surface temperature)
                            dGdTsoil) ! Derivative of G wrt Tsoil (soil temperature)
 
        !------------------------------------
        !           3.2. Calculate SEB if surface iice == 1
        !                top layer frozen (1) or not (0)
        !------------------------------------
        if (var(1)%iice.eq.1.and.Tsurface_pot> zero) then
            Tsurface_pot = 0.0_dp
            Tsurface = 0.0_dp
    
            Epot = (esat(Tsurface)*0.018_dp/thousand/8.314_dp/(vmet%Ta+Tzero)  - & ! m3 H2O (liq) m-3 (air)
                    vmet%cva)*rhow*var(1)%lambdav/vmet%rbw
            dEpotdTsoil = zero
            dGdTsoil = zero
            Hpot = rhocp*(Tsurface - vmet%Ta)/vmet%rbh
            Gpot = vmet%Rn - Hpot - Epot
            dEpotdTs = zero
        endif
 
        !------------------------------------
        !           3.3. Calculate SEB if surace soil saturated
        !       
        !------------------------------------
        if (var(1)%isat.eq.1) then  ! saturated surface =>. potential evporation
            isEpot = .true.
            Tsurface = Tsurface_pot !temperature at surface (soil, pond or litter) 
            lE0 = Epot !latent heat flux at current time step, assuming Tg of previous time-step  [W m-2]
            G0 = Gpot !Belinda's stomatal model intercept, at current time-step
            E_vap = zero !Evaporation of vapor
            dEpot_vapdT1 = zero !derivative of E_vap
            E_liq = lE0 !Evaporation of liquid
        
        !------------------------------------
        !           3.4. Calculate SEB if unsaturated
        !       
        !-----------------------------------
        else ! unsaturated surface: finite vapour transfer; surface flux may be supply limited
            !......................................
            !              a. Call hyofh
            !                 to get phi, K, Kh at hmin
            !......................................
            ! hmin = -1.0e6_dp       - minimum matric head h
            ! par(1)%lam              - lambda: shape parameter for soil moisture retention curve
            ! par(1)%eta              - shape parameter for hydraulic conductivity retention curve
            ! par(1)%he               - matric potential at air entry point [m]
            
            ! -------- UNKNOWNS ---------------
            ! Kmin                    - hydraulic conductivity at min value
            ! Khmin1                  - derivative dK/dh at min value
            ! phimin                  - matric flux potential (MFP) at min value
            call hyofh(hmin, par(1)%lam, par(1)%eta, par(1)%Ke, par(1)%he, &
                    Kmin, Khmin, phimin)
           
            !......................................
            !              b. Calculate Eliq
            !                 Evaporation of liquid    
            !......................................
            E_liq = ((var(1)%phi-phimin)/(half*dx(1))-var(1)%K)*thousand*var(1)%lambdav
          
            !......................................
            !              c. Calculate E_vap & dEpot_vapdT1
            !                 Evaporation of vapour 
            !                 Dv, diffusivity of water vapour in the bulk soil (m2/s)
            !                 cs, heat capacity of soil or snow at surface [J m-3 K-1]
            !......................................
            if (var(1)%Dv > 1.e-12_dp) then
                ! E = (cs-ca)/rbw = E_liq + E_vap = E_liq + (c1-cs)/Dv/dx/2
                cs = ( E_liq/var(1)%lambdav + var(1)%rh*csat(Tsoil(1))*var(1)%Dv/(half*dx(1)) + &
                    vmet%cva*thousand/vmet%rbw ) &
                    / (one/vmet%rbw + var(1)%Dv/(half*dx(1)))
    
                E_vap = (var(1)%rh*csat(Tsoil(1)) - cs) * var(1)%Dv/(half*dx(1)) * var(1)%lambdav
    
                ! dEpot_vapdT1 = var(1)%Dv/(half*dx(1))*var(1)%rh*slope_csat(Tsoil(1)) &
                !      * (one - vmet%rbw*var(1)%Dv/(half*dx(1))/(one+vmet%rbw*var(1)%Dv/(half*dx(1)))) &
                !      * var(1)%lambdav
    
                dEpot_vapdT1 = var(1)%Dv/(half*dx(1)) * var(1)%rh*slope_csat(Tsoil(1)) * var(1)%lambdav
            else
                E_vap     = zero
                dEpot_vapdT1 = zero
            endif
            
            !......................................
            !              d. Calculate lE0 
            !                  IE0, latent heat flux at current time step, assuming Tg of previous time-step  [W m-2]
            !                  cs, heat capacity of soil or snow at surface [J m-3 K-1]
            !.....................................
            if (Epot <= (E_vap+E_liq)) then
                isEpot = .true.
                lE0 = Epot
                cs  = csat(Tsurface_pot)
                E_vap = (var(1)%rh*csat(Tsoil(1)) - cs) * var(1)%Dv/(half*dx(1)) * var(1)%lambdav
                E_liq = lE0 - E_vap
                dEpot_vapdT1 = var(1)%Dv/(half*dx(1)) * var(1)%rh*slope_csat(Tsoil(1)) * var(1)%lambdav
            else
                isEpot = .false.
                lE0 = E_vap+E_liq
                dEpotdTs = zero
            endif          
 
            !......................................
            !              e. Calculate Tsurface
            !                    dx, thickness of soil layer
            !                    lE0, latent heat flux at current time step
            !                    vmet%Rn,  net radiation absorbed by soil 
            !                    var(1)%kth, thermal conductivity
            !                    Tsoil, soil temperature
            !                    vmet%rbh, resistance to heat transfer beween surface and lowest atmospheric layer
            !                    rhocp, densitiy of dry air (rhoa) * specific heat capacity of dry air at 0-40 degC [J/kgK] (cpa)
            !                    vmet%Ta,  air temperature --> observed Tmean dailiy --> subroutine read_input()
            !......................................
            Tsurface = (-half*dx(1)*lE0 + half*dx(1)*vmet%Rn + &
                    var(1)%kth*Tsoil(1) + half*dx(1)*(one/vmet%rbh*rhocp)*vmet%Ta) &
                    /(var(1)%kth + half*dx(1)*(one/vmet%rbh*rhocp))
           
            !......................................
            !              f. Calculate G0 
            !                 (!Belinda's stomatal model intercept, at current time-step)
            !......................................
            G0       = var(1)%kth/(half*dx(1))*(Tsurface-Tsoil(1))
            dGdTsoil  =  -var(1)%kth/(half*dx(1))
          
            !......................................
            !              g. Calculate G0 & Tsurface -- IF (var(1)%iice.eq.1 & Tsurface>zero
            !......................................
            if ((var(1)%iice.eq.1) .and. (Tsurface>zero)) then
                Tsurface = 0.0_dp
                rhocp1 = rmair*101325._dp/rgas/(vmet%Ta+Tzero)*cpa
                G0 = vmet%Rn - rhocp1*(Tsurface - vmet%Ta)/vmet%rbh - lE0
                dGdTsoil = 0.0_dp
            endif

        endif
 
        !------------------------------------
        !           3.5. Calculate qevap & qsurface
        !       
        !-----------------------------------
        qevap = lE0/(thousand*var(1)%lambdav) !conversion to meter
        ! qsurface  = qprec - qevap
        qsurface  = qprec

         !------------------------------------
        !           3.6. Calculate derivatives: q, qliq, qh
        !       
        !-----------------------------------
        ! derivatives
        ! q refers to moisture in numerator
        ! qh refers to heat in numerator
        ! y refers to moisture in denominator
        ! T refers to temperature in denominator
        ! a refers to the layer above
        ! b refers to the layer below
        
        ! initialise derivatives to zero
        qyb = zero
        qTb = zero
        qhyb = zero
        qhTb = zero
        ! liquid and vapour fluxes
        qlyb = zero
        qvyb = zero

        ! potential evap independent of S(1), dependent on T1
        if (var(1)%isat.eq.1) then
            qyb  = zero
            qTb  = -dEpotdTsoil/(thousand*var(1)%lambdav)
            qliq = -Epot/(thousand*var(1)%lambdav)
            qv   = zero
            qlyb = zero
            qvyb = zero
            qlTb = qTb
            qvTb = zero
        elseif (isEpot) then
            qTb  = -dEpot_vapdT1/(thousand*var(1)%lambdav)
            qyb  = zero
            qliq = -E_liq/(thousand*var(1)%lambdav)
            qv   = -E_vap/(thousand*var(1)%lambdav)
            qlyb = zero
            qvyb = zero
            qlTb = qTb
            qvTb = zero
        else ! supply limited
            qTb = -dEpot_vapdT1/(thousand*var(1)%lambdav)
            qyb = -(var(1)%phiS/(half*dx(1)) - var(1)%KS)  !!vh!! include vapour component??
            qliq = -E_liq/(thousand*var(1)%lambdav)
            qv   = -E_vap/(thousand*var(1)%lambdav)
            qlyb = -(var(1)%phiS/(half*dx(1)) - var(1)%KS)
            qvyb = zero
            qlTb = zero
            qvTb = qTb
        endif ! end of partial derivative evaluation
            
       
        !------------------------------------
        !           3.7. Calculate qadv
        !                  advective component of heat flux  
        !-----------------------------------
        qadv = rhow*cswat*qprec*(vmet%Ta)

        Tqw  = merge(vmet%Ta, Tsoil(1), (-qevap)>zero)
        dTqwdTb = merge(zero, one, (-qevap)>zero)
        qadv = qadv + rhow*cswat*Tqw*(-qevap)

        qadvTb = dTqwdTb + rhow*cswat*Tqw*qTb
        qadvyb =  rhow*cswat*qyb*Tqw
    
        !-----------------------------------
        !           3.8. Calculate qh: total heat flux into surface
        !-----------------------------------
        qh = qadv + G0
        qhyb = qadvyb
        qhTb = dGdTsoil + qadvTb
     
    END SUBROUTINE SEB ! finished all the surfaces

    SUBROUTINE snow_adjust(irec, mp, n, kk, ns, h0, hice, thetai, dx, vsnow, var, par, S, Tsoil, &
        Jcol_latent_S, Jcol_latent_T, Jcol_sensible, deltaJ_sensible_S, qmelt, qtransfer, j0snow)
 
     INTEGER(i4),                               INTENT(IN)    :: irec  ! # of grid-cells
     INTEGER(i4),                               INTENT(IN)    :: mp    ! # of grid-cells
     INTEGER(i4),                               INTENT(IN)    :: n     ! # of soil layers
     INTEGER(i4),                               INTENT(IN)    :: kk    ! grid-cell reference
     INTEGER(i4),    DIMENSION(mp),             INTENT(INOUT) :: ns    ! pond (0), np ond (1)
     REAL(dp),       DIMENSION(1:n),            INTENT(IN)     :: dx    ! soil depths
     REAL(dp),       DIMENSION(1:n),            INTENT(INOUT) :: Tsoil ! soil temperatures soil
     REAL(dp),       DIMENSION(1:n),            INTENT(INOUT) :: S     ! soil temperatures soil
     REAL(dp),       DIMENSION(mp),             INTENT(INOUT) :: h0, hice, j0snow ! pond
     REAL(dp),       DIMENSION(1:n),            INTENT(INOUT) :: thetai
     REAL(dp),       DIMENSION(1:mp),           INTENT(INOUT) :: Jcol_latent_S, Jcol_latent_T, Jcol_sensible
     REAL(dp),       DIMENSION(1:n),            INTENT(INOUT) :: deltaJ_sensible_S
     REAL(dp),       DIMENSION(nsnow_max),      INTENT(INOUT) :: qmelt
     REAL(dp),       DIMENSION(1:mp),           INTENT(OUT) :: qtransfer
     TYPE(vars),      DIMENSION(1:n),           INTENT(INOUT) :: var
     TYPE(params),    DIMENSION(1:n),           INTENT(IN) :: par
     TYPE(vars_snow), DIMENSION(1:mp),          INTENT(INOUT) :: vsnow


     REAL(dp),       DIMENSION(1:mp)            :: tmp1d1, tmp1d2, tmp1d3,  tmp1d4
     REAL(dp),       DIMENSION(1:mp)            :: h0_tmp, hice_tmp
     REAL(dp)                                   :: theta, tmp1, tmp2 ,Tfreezing(1:mp), Jsoil, theta_tmp
     INTEGER(i4) :: i,j ! counters
     LOGICAL :: melt_transfer=.FALSE.
 
     tmp1d1(kk) = h0(kk)+dx(1)*(var(1)%thetai+var(1)%thetal) ! total moisture content of top soil layer + pond     
     vsnow(kk)%melt = zero
     qmelt = zero
     qtransfer = zero
 
     ! no dedicated snow pack if solid part of cumulated snow is less than min thresshold
     ! also, don't initialise dedicated snowpack if this would deplete water in top soil layer to less than 1 mm
     if (((vsnow(kk)%wcol-sum(vsnow(kk)%hliq(:)))<snmin*(vsnow(kk)%dens(1)/rhow)).or. &
          ((vsnow(kk)%nsnow==0).and.(tmp1d1(kk)-vsnow(kk)%wcol)<0.001_dp)) then
        vsnow(kk)%nsnow = 0
        theta         = S(1)*(par(1)%thre) + (par(1)%the - par(1)%thre)
        if (vsnow(kk)%hsnow(1)>zero)  then ! termination of dedicated snow layer
           ! total energy in old snow layer
           tmp1d1(kk) = (vsnow(kk)%tsn(1))*rhow*vsnow(kk)%hliq(1)*cswat + &
                (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*((vsnow(kk)%tsn(1))*csice-lambdaf)
           vsnow(kk)%Qadv_transfer = vsnow(kk)%Qadv_transfer - tmp1d1(kk) ! transfer of energy from soil to snow
           vsnow(kk)%Qtransfer = vsnow(kk)%Qtransfer -vsnow(kk)%hsnow(1) ! transfer of water from soil to snow
           qtransfer(kk) = -vsnow(kk)%hsnow(1) ! transfer of water from soil to snow
           vsnow(kk)%deltaJlatent(1) = vsnow(kk)%deltaJlatent(1) +lambdaf*(vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow
           vsnow(kk)%deltaJsensible(1) = vsnow(kk)%deltaJsensible(1) -(vsnow(kk)%tsn(1))*rhow*vsnow(kk)%hliq(1)*cswat- &
                (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*(vsnow(kk)%tsn(1))*csice
 
           ! total energy in old top soil layer
           tmp1d2(kk) = JSoilLayer(Tsoil(1), &
                        dx(1), theta,par(1)%css, par(1)%rho, &
                        h0(kk), par(1)%thre, par(1)%the, &
                        par(1)%he, one/(par(1)%lambc*freezefac))
 
           tmp1d3(kk) = Tsoil(1)
           !calculate new thetal, consistent with total energy and new pond height
           h0_tmp(kk) = h0(kk)
           if (h0(kk)>zero) then
              h0(kk) = h0(kk) + vsnow(kk)%hsnow(1)
           elseif ((theta+vsnow(kk)%hsnow(1)/dx(1))<par(1)%the) then
              h0(kk)=zero
              theta = theta+vsnow(kk)%hsnow(1)/dx(1)
              S(1) = (theta - (par(1)%the - par(1)%thre) )/(par(1)%thre)
           elseif ((theta+vsnow(kk)%hsnow(1)/dx(1))>par(1)%the) then
              h0(kk) = ((theta+vsnow(kk)%hsnow(1)/dx(1))-par(1)%the)*dx(1)
              theta = par(1)%the
              S(1) = one
              var(1)%isat = 1
              ns(kk) = 0
           endif
 
           Tfreezing(kk) = Tfrz(S(1), par(1)%he, one/(par(1)%lambc*freezefac))
 
           ! check if total energy in old snow layer and top soil (+pond) is enough for complete melting
           tmp1d3(kk) = var(1)%thetai*dx(1)*rhow*lambdaf + &
                var(1)%thetai*h0_tmp(kk)/par(1)%thre*rhow*lambdaf
           tmp1d4(kk) = (Tfreezing(kk))*(rhow*cswat*(theta*dx(1)+h0(kk))+par(1)%rho*par(1)%css*dx(1))
 
           if ((tmp1d1(kk)+tmp1d2(kk)-tmp1d4(kk))>zero) then !  complete melting
 
              if (Tsoil(1).gt.var(1)%Tfrz) then
                 Jcol_latent_T(kk) =     Jcol_latent_T(kk) + tmp1d3(kk)
                 deltaJ_sensible_S(1) = zero
                 Tsoil(1) = Tsoil(1) + tmp1d1(kk)/(rhow*cswat*(theta*dx(1)+h0(kk))+par(1)%rho*par(1)%css*dx(1))
              else
 
                 Jcol_latent_T(kk) =     Jcol_latent_T(kk) + tmp1d3(kk)
                 deltaJ_sensible_S(1) = zero
                 Tsoil(1) = var(1)%Tfrz + (tmp1d1(kk)+tmp1d2(kk) -tmp1d4(kk))/ &
                      (rhow*cswat*(theta*dx(1)+h0(kk))+par(1)%rho*par(1)%css*dx(1))
              endif
              var(1)%iice = 0
              var(1)%thetai = zero
              var(1)%thetal = theta
              hice(kk) = zero
              vsnow(kk)%hsnow(1) = zero
              vsnow(kk)%hliq(1)= zero
 
           else  ! soil remains frozen
 
              Jsoil = tmp1d1(kk)+tmp1d2(kk)! total energy in  soil layer
              !check there is a zero
              tmp1 = GTfrozen(Tsoil(1)-50._dp, Jsoil, dx(1), theta,par(1)%css, par(1)%rho, &
                   h0(kk), par(1)%thre, par(1)%the, par(1)%he, one/(par(1)%lambc*freezefac))
 
              tmp2 = GTFrozen(Tfreezing(kk), Jsoil, dx(1), theta,par(1)%css, par(1)%rho, &
                   h0(kk), par(1)%thre, par(1)%the, par(1)%he, one/(par(1)%lambc*freezefac))
 
              ! there is a zero in between
              if ((tmp1*tmp2) < zero) then
                 tmp1d3(kk) = rtbis_Tfrozen(tmp1d2(kk), dx(1), theta,par(1)%css, par(1)%rho, &
                      h0(kk), par(1)%thre, par(1)%the, par(1)%he, one/(par(1)%lambc*freezefac), &
                      Tsoil(1)-50._dp, Tfreezing(kk), 0.0001_dp)
 
                 tmp1d4(kk) = thetalmax(tmp1d3(kk), S(1), par(1)%he, one/(par(1)%lambc*freezefac), &
                      par(1)%thre, par(1)%the) ! liquid content at new Tsoil
              else
                 write(*,*) "Found no solution for Tfrozen 2. Stop. " , kk
                 stop
              endif
 
              hice_tmp(kk) = hice(kk)
              hice(kk) = h0(kk)*var(1)%thetai/par(1)%thre
              vsnow(kk)%hsnow(1) = zero
              vsnow(kk)%hliq(1) = zero
              var(1)%thetal = max(tmp1d4(kk),zero)
              var(1)%thetai = theta - tmp1d4(kk)
 
              ! correct total energy stored in pond + soil
              Jcol_latent_S(kk) = Jcol_latent_S(kk)  - rhow*lambdaf*((hice(kk)-hice_tmp(kk)) + &
                   dx(1)*(var(1)%thetai-thetai(1)))
 
 
              Jcol_sensible(kk) = Jcol_sensible(kk) + &
                   JSoilLayer(tmp1d3(kk), &
                   dx(1), theta,par(1)%css, par(1)%rho, &
                   h0(kk), par(1)%thre, par(1)%the, &
                   par(1)%he, one/(par(1)%lambc*freezefac)) - &
                   tmp1d2(kk) - &
                   (-rhow*lambdaf*((hice(kk)-hice_tmp(kk)) + &
                   dx(1)*(var(1)%thetai-thetai(1))))
              Tsoil(1) = tmp1d3(kk)
 
              if (var(1)%thetai>zero) then
                 var(1)%iice = 1
              endif
           endif
 
           vsnow(kk)%Jsensible(1) = (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*csice*(vsnow(kk)%Tsn(1))
           vsnow(kk)%Jlatent(1) = (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*(-lambdaf)
           vsnow(kk)%J = sum(vsnow(kk)%Jsensible(1:vsnow(kk)%nsnow)+vsnow(kk)%Jlatent(1:vsnow(kk)%nsnow))
           vsnow(kk)%deltaJ = vsnow(kk)%J - J0snow(kk)
           vsnow(kk)%FluxDivergence = vsnow(kk)%Qadv_rain + vsnow(kk)%Qadv_snow + vsnow(kk)%Qadv_vap + &
                vsnow(kk)%Qadv_melt + vsnow(kk)%Qcond_net  +  vsnow(kk)%Qadv_transfer
 
           vsnow(kk)%hsnow(1) = zero
           vsnow(kk)%hliq(1) = zero
           vsnow(kk)%depth(1) = zero
 
        endif ! termination of dedicated snow layer
 
     else
        if  (vsnow(kk)%nsnow_last==0) then ! snow layer initialisation (transfer pond/soil water to dedicated snow layer)
           vsnow(kk)%nsnow = 1
           vsnow(kk)%hsnow(1) = vsnow(kk)%wcol
           vsnow(kk)%depth(1) = vsnow(kk)%hsnow(1)*rhow/vsnow(kk)%dens(1)
 
           if (h0(kk)>vsnow(kk)%wcol) then ! extract new snow layer from pond
              ! total energy in new snow layer
              tmp1d1(kk) = (Tsoil(1))*rhow*vsnow(kk)%hsnow(1)*(cswat*(h0(kk)-hice(kk))/h0(kk) + &
                   csice*hice(kk)/h0(kk)) - rhow*lambdaf*vsnow(kk)%hsnow(1)*hice(kk)/h0(kk)
              ! correct total energy stored in pond + soil
              Jcol_latent_S(kk) = Jcol_latent_S(kk) + rhow*lambdaf*vsnow(kk)%hsnow(1)*hice(kk)/h0(kk)
              Jcol_sensible(kk) = Jcol_sensible(kk) - (Tsoil(1))*rhow*vsnow(kk)%hsnow(1) &
                   *(cswat*(h0(kk)-hice(kk))/h0(kk) +  csice*hice(kk)/h0(kk))
 
              ! total energy in snowpack totally frozen at 0degC
              tmp1d2(kk) = rhow*vsnow(kk)%hsnow(1)*(csice*zero - lambdaf)
              if (tmp1d1(kk)<=tmp1d2(kk)) then
                 ! alll snow water frozen
                 vsnow(kk)%hliq(1)=zero
                 vsnow(kk)%tsn(1) = (tmp1d1(kk)+rhow*lambdaf*vsnow(kk)%hsnow(1))/(csice*rhow*vsnow(kk)%hsnow(1))
              else
                 ! liquid snow water
                 vsnow(kk)%hliq(1)=(tmp1d1(kk)-vsnow(kk)%hsnow(1)*rhow*(zero*csice-lambdaf))/ &
                      (rhow*(zero*cswat-zero*csice+lambdaf))
                 vsnow(kk)%tsn(1) = zero
              endif
 
              h0(kk) = h0(kk) - vsnow(kk)%wcol
              hice(kk) = h0(kk)*var(1)%thetai/par(1)%thre
 
           else  ! extract new snow layer from soil ice + pond
              ! total energy in new snow layer (component extracted from soil ice)
              tmp1d1(kk) = (Tsoil(1))*rhow*(vsnow(kk)%hsnow(1)-h0(kk))*csice - &
                   rhow*lambdaf*(vsnow(kk)%hsnow(1)-h0(kk))
              h0_tmp(kk) = h0(kk)
              hice_tmp(kk) = hice(kk)
              if (h0(kk)>zero) then
                 tmp1d1(kk) = tmp1d1(kk) + (Tsoil(1))*rhow*(cswat*(h0(kk)-hice(kk)) + &
                      csice*hice(kk)) - rhow*lambdaf*hice(kk)
              endif
              ! total energy in snowpack totally frozen at 0degC
              tmp1d2(kk) = rhow*vsnow(kk)%hsnow(1)*(csice*zero - lambdaf)
              if (tmp1d1(kk)<=tmp1d2(kk)) then
                 ! alll snow water frozen
                 vsnow(kk)%hliq(1)=zero
                 vsnow(kk)%tsn(1) = (tmp1d1(kk)+rhow*lambdaf*vsnow(kk)%hsnow(1))/(csice*rhow*vsnow(kk)%hsnow(1))
              else
                 ! liquid snow water
                 vsnow(kk)%hliq(1)=(tmp1d1(kk)-vsnow(kk)%hsnow(1)*rhow*(zero*csice-lambdaf))/ &
                      (rhow*(zero*cswat-zero*csice+lambdaf))
                 vsnow(kk)%tsn(1) = zero
              endif
 
              ! correct soil moisture
              S(1) = S(1) - (vsnow(kk)%hsnow(1)-h0(kk))/dx(1)/par(1)%thre
              if (S(1).lt.one) then
                 var(1)%isat= 0
              endif
              Tfreezing(kk) = Tfrz(S(1), par(1)%he, one/(par(1)%lambc*freezefac))
              if (S(1)<zero) then
                 write(*,*) "error: over-extraction of soil water during snow pack init"
                 write(*,*) "S(1), snow-col, deltaS"
                 write(*,*) S(1) , vsnow(kk)%hsnow(1), - (vsnow(kk)%hsnow(1)-h0(kk))/dx(1)/par(1)%thre
              endif
              h0(kk) = zero
              hice(kk) = zero
              ! correct total energy stored in pond + soil
              ! correct soil temperature
              theta         = S(1)*(par(1)%thre) + (par(1)%the - par(1)%thre)
              ! total energy added to top soil layer
              tmp1d1(kk) = - tmp1d1(kk)
              ! total energy in old top soil layer
              tmp1d2(kk) = var(1)%csoil*dx(1)*(Tsoil(1)) -lambdaf*dx(1)*var(1)%thetai + &
                   (h0_tmp(kk)-hice_tmp(kk))*cswat*rhow*(Tsoil(1)) + &
                   hice_tmp(kk)*rhow*(csice*(Tsoil(1))-lambdaf)
              ! calculate energy in new top soil layer
              if (var(1)%iice==0) then
                 var(1)%csoil = theta*rhow*cswat+par(1)%rho*par(1)%css
                 tmp1d3(kk) = (tmp1d1(kk)+tmp1d2(kk))/((theta*rhow*cswat+par(1)%rho*par(1)%css)*dx(1)+ &
                      h0(kk)*rhow*cswat)
                 Jcol_sensible(kk) = Jcol_sensible(kk) - &
                      var(1)%csoil*dx(1)*(Tsoil(1)) - &
                      (h0_tmp(kk))*cswat*rhow*(Tsoil(1)) + &
                      (theta*rhow*cswat+par(1)%rho*par(1)%css)*dx(1)*(tmp1d3(kk)) + &
                      (h0_tmp(kk))*cswat*rhow*(tmp1d3(kk))
 
                 var(1)%csoil = theta*rhow*cswat+par(1)%rho*par(1)%css
                 Tsoil(1) = tmp1d3(kk)
 
              else
                 ! check if total energy in melt water and top soil (+pond) is enough for complete melting
                 tmp1d3(kk) = var(1)%thetai*dx(1)*rhow*lambdaf + &
                      var(1)%thetai*h0_tmp(kk)/par(1)%thre*rhow*lambdaf
 
                 tmp1d4(kk) = (Tfreezing(kk))*(rhow*cswat*(theta*dx(1)+h0(kk))+par(1)%rho*par(1)%css*dx(1))
                 if ((tmp1d1(kk)+tmp1d2(kk)-tmp1d4(kk))>zero) then
                    !  complete melting
                    Jcol_latent_T(kk) =     Jcol_latent_T(kk) + tmp1d3(kk)
                    deltaJ_sensible_S(1) = zero
                    Tsoil(1) = var(1)%Tfrz + (tmp1d1(kk)+tmp1d2(kk) -tmp1d4(kk))/ &
                         (rhow*cswat*(theta*dx(1)+h0(kk))+par(1)%rho*par(1)%css*dx(1))
 
                    var(1)%iice = 0
                    var(1)%thetai = zero
                    thetai(1) = var(1)%thetai
                    var(1)%thetal = theta
                 else
 
                    Jsoil = tmp1d1(kk)+tmp1d2(kk)! total energy in  soil layer
                    !check there is a zero
                    tmp1 = GTfrozen(Tsoil(1)-50._dp, Jsoil, dx(1), theta,par(1)%css, par(1)%rho, &
                         h0(kk), par(1)%thre, par(1)%the, par(1)%he, one/(par(1)%lambc*freezefac))
 
                    tmp2 = GTFrozen(Tfreezing(kk), Jsoil, dx(1), theta,par(1)%css, par(1)%rho, &
                         h0(kk), par(1)%thre, par(1)%the, par(1)%he, one/(par(1)%lambc*freezefac))
 
                    ! there is a zero in between
                    if ((tmp1*tmp2) < zero) then
                       tmp1d3(kk) = rtbis_Tfrozen(tmp1d2(kk), dx(1), theta,par(1)%css, par(1)%rho, &
                            h0(kk), par(1)%thre, par(1)%the, par(1)%he, one/(par(1)%lambc*freezefac), &
                            Tsoil(1)-50._dp, Tfreezing(kk), 0.0001_dp)
 
                       tmp1d4(kk) = thetalmax(tmp1d3(kk), S(1), par(1)%he, one/(par(1)%lambc*freezefac), &
                            par(1)%thre, par(1)%the) ! liquid content at new Tsoil
                    else
                       write(*,*) "Found no solution for Tfrozen 3. Stop.", kk, S(i), Tsoil(i), tmp1, tmp2
                       write(*,*) S(i), Tsoil(i), tmp1, tmp2
                       stop
                    endif
 
                    var(1)%thetal = max(tmp1d4(kk), zero)
                    var(1)%thetai = theta - tmp1d4(kk)
                    ! correct total energy stored in pond + soil
                    Jcol_latent_S(kk) = Jcol_latent_S(kk)- rhow*lambdaf*((hice(kk)-hice_tmp(kk)) + &
                         dx(1)*(var(1)%thetai-thetai(1)))
 
                    Jcol_sensible(kk) = Jcol_sensible(kk) - &
                         var(1)%csoil*dx(1)*(Tsoil(1)) - &
                         (h0_tmp(kk)-hice_tmp(kk))*cswat*rhow*(Tsoil(1)) - &
                         hice_tmp(kk)*rhow*(csice*(Tsoil(1))) + &
                         dx(1)*(tmp1d3(kk))*par(1)%rho*par(1)%css + &
                         (h0(kk)-hice(kk)+var(1)%thetal*dx(1))*cswat*rhow*(tmp1d3(kk)) + &
                         (hice_tmp(kk)+var(1)%thetai)*rhow*(csice*(tmp1d3(kk)))
 
                    thetai(1) = var(1)%thetai
                    Tsoil(1) = tmp1d3(kk)
 
                 endif ! incomplete melting
 
              endif ! iice=1
 
           endif ! extract new snow layer from soil ice
 
           vsnow(kk)%deltaJsensible(1) = vsnow(kk)%deltaJsensible(1) + &
                (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*csice*(vsnow(kk)%tsn(1)) + &
                vsnow(kk)%hliq(1)*rhow*cswat*(vsnow(kk)%tsn(1))
 
           vsnow(kk)%deltaJlatent(1) = vsnow(kk)%deltaJlatent(1) + &
                (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*(-lambdaf)
 
           vsnow(kk)%Qadv_transfer = vsnow(kk)%Qadv_transfer + & ! soil to snow
                (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*csice*(vsnow(kk)%tsn(1)) + &
                vsnow(kk)%hliq(1)*rhow*cswat*(vsnow(kk)%tsn(1)) + &
                (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*(-lambdaf)
 
           vsnow(kk)%Qtransfer = vsnow(kk)%Qtransfer + vsnow(kk)%hsnow(1)
           qtransfer(kk) = vsnow(kk)%hsnow(1) ! transfer of water from snow to soil
           vsnow(kk)%Jsensible(1) = (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*csice*(vsnow(kk)%Tsn(1))
           vsnow(kk)%Jlatent(1) = (vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*rhow*(-lambdaf)
           vsnow(kk)%J = sum(vsnow(kk)%Jsensible(1:vsnow(kk)%nsnow)+vsnow(kk)%Jlatent(1:vsnow(kk)%nsnow))
           vsnow(kk)%deltaJ = vsnow(kk)%J - J0snow(kk)
           vsnow(kk)%FluxDivergence = vsnow(kk)%Qadv_rain + vsnow(kk)%Qadv_snow + vsnow(kk)%Qadv_vap + &
                vsnow(kk)%Qadv_melt + vsnow(kk)%Qcond_net  +  vsnow(kk)%Qadv_transfer
 
        endif        ! snow layer initialisation
 
        do i=1, vsnow(kk)%nsnow
           vsnow(kk)%Jsensible(i) = (vsnow(kk)%hsnow(i)-vsnow(kk)%hliq(i))*rhow*csice*(vsnow(kk)%Tsn(i))
           vsnow(kk)%Jlatent(i) = (vsnow(kk)%hsnow(i)-vsnow(kk)%hliq(i))*rhow*(-lambdaf)
        enddo
 
        vsnow(kk)%J = sum(vsnow(kk)%Jsensible(1:vsnow(kk)%nsnow)+vsnow(kk)%Jlatent(1:vsnow(kk)%nsnow))
        vsnow(kk)%deltaJ = vsnow(kk)%J - J0snow(kk)
        vsnow(kk)%FluxDivergence = vsnow(kk)%Qadv_rain + vsnow(kk)%Qadv_snow + vsnow(kk)%Qadv_vap + &
             vsnow(kk)%Qadv_melt + vsnow(kk)%Qcond_net  +  vsnow(kk)%Qadv_transfer
 
        ! get snow melt from top layer
        if ((vsnow(kk)%hliq(1) - vsnow(kk)%hsnow(1)*vsnow(kk)%fsnowliq_max(1))>zero) then ! remove melt water
           qmelt(1) = max((vsnow(kk)%hliq(1) - vsnow(kk)%hsnow(1)*vsnow(kk)%fsnowliq_max(1)),zero)
           qmelt(1) = min(qmelt(1), max(0.9_dp*(vsnow(kk)%hsnow(1)-snmin*(vsnow(kk)%dens(1)/rhow)),zero))
           vsnow(kk)%melt(1) = vsnow(kk)%melt(1) + qmelt(1)
           vsnow(kk)%hliq(1) = vsnow(kk)%hliq(1) - qmelt(1)
           vsnow(kk)%hsnow(1) = vsnow(kk)%hsnow(1) - qmelt(1)
           ! adjust depth and density for snow melt removal
           !tmp1d3(kk) = vsnow(kk)%dens(1)*(one-vsnow(kk)%hliq(1)/vsnow(kk)%hsnow(1))
           !vsnow(kk)%depth(1) = vsnow(kk)%depth(1) - qmelt(1)*rhow/tmp1d3(kk)
           ! vsnow(kk)%dens(1) = vsnow(kk)%hsnow(1)*rhow/vsnow(kk)%depth(1)
           !vsnow(kk)%dens(1) = max(vsnow(kk)%dens(1) - rhow*qmelt(1)/vsnow(kk)%depth(1), 50.0_dp)
           do i=1, vsnow(kk)%nsnow
              vsnow(kk)%Jsensible(i) = (vsnow(kk)%hsnow(i)-vsnow(kk)%hliq(i))*rhow*csice*(vsnow(kk)%Tsn(i))
              vsnow(kk)%Jlatent(i) = (vsnow(kk)%hsnow(i)-vsnow(kk)%hliq(i))*rhow*(-lambdaf)
           enddo
        endif ! end remove snow melt from top layer
 
        ! add snow melt from above to layer below
        if (vsnow(kk)%nsnow>1) then
 
           ! simply add melt water from above if melt water already exists and total new amount doesn't exceed capacity
           if ((vsnow(kk)%hliq(vsnow(kk)%nsnow).gt.zero).and. &
                (vsnow(kk)%hliq(vsnow(kk)%nsnow) + qmelt(1)).le.(vsnow(kk)%hsnow(vsnow(kk)%nsnow) + &
                qmelt(1))*vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow)) then
 
              vsnow(kk)%hliq(vsnow(kk)%nsnow) = vsnow(kk)%hliq(vsnow(kk)%nsnow) + qmelt(1)
              vsnow(kk)%hsnow(vsnow(kk)%nsnow) = vsnow(kk)%hsnow(vsnow(kk)%nsnow) + qmelt(1)
              qmelt(vsnow(kk)%nsnow) = zero
              ! or convert excess to melt water
           elseif ((vsnow(kk)%hliq(vsnow(kk)%nsnow).gt.zero).and. &
                (vsnow(kk)%hliq(vsnow(kk)%nsnow) + qmelt(1)).gt.(vsnow(kk)%hsnow(vsnow(kk)%nsnow) + &
                qmelt(1))*vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow)) then
 
              tmp1d1(kk) = vsnow(kk)%hliq(vsnow(kk)%nsnow)
              tmp1d2(kk) = vsnow(kk)%hsnow(vsnow(kk)%nsnow)
 
 
              qmelt(vsnow(kk)%nsnow) =  tmp1d1(kk) + vsnow(kk)%melt(1) - &
                   vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow)*(tmp1d2(kk) - tmp1d1(kk))/ &
                   (1._dp-vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow))
 
              vsnow(kk)%hliq(vsnow(kk)%nsnow) = vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow)*(tmp1d2(kk)  - tmp1d1(kk) )/ &
                   (1._dp-vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow))
 
 
              vsnow(kk)%hsnow(vsnow(kk)%nsnow) = (tmp1d2(kk) - tmp1d1(kk)) + vsnow(kk)%hliq(vsnow(kk)%nsnow)
 
              ! or add melt water to completely frozen snowpack below
           else
              tmp1d1(kk) = (vsnow(kk)%hsnow(vsnow(kk)%nsnow)*(vsnow(kk)%tsn(vsnow(kk)%nsnow)*csice-lambdaf) &
                   /(vsnow(kk)%hsnow(vsnow(kk)%nsnow) + qmelt(1)) + lambdaf)/csice
 
              if (tmp1d1(kk).lt.zero) then ! snow pack remains completely frozen
                 vsnow(kk)%tsn(vsnow(kk)%nsnow) = tmp1d1(kk)
                 vsnow(kk)%hliq(vsnow(kk)%nsnow) = zero
                 vsnow(kk)%hsnow(vsnow(kk)%nsnow) = vsnow(kk)%hsnow(vsnow(kk)%nsnow) + qmelt(1)
                 qmelt(vsnow(kk)%nsnow) = zero
 
              else ! snowpack partially melted
                 vsnow(kk)%hliq(vsnow(kk)%nsnow) = vsnow(kk)%hsnow(vsnow(kk)%nsnow)/ &
                      lambdaf*(csice*vsnow(kk)%tsn(vsnow(kk)%nsnow)-lambdaf) + &
                      (vsnow(kk)%hsnow(vsnow(kk)%nsnow) + qmelt(1))
                 vsnow(kk)%tsn(vsnow(kk)%nsnow) = zero
                 vsnow(kk)%hsnow(vsnow(kk)%nsnow) = vsnow(kk)%hsnow(vsnow(kk)%nsnow) + qmelt(1)
                 qmelt(vsnow(kk)%nsnow) = zero
 
 
                 if (vsnow(kk)%hliq(vsnow(kk)%nsnow).gt. &
                      vsnow(kk)%hsnow(vsnow(kk)%nsnow)*vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow)) then
 
                    tmp1d1(kk) = vsnow(kk)%hliq(vsnow(kk)%nsnow)
                    tmp1d2(kk) = vsnow(kk)%hsnow(vsnow(kk)%nsnow)
 
                    qmelt(vsnow(kk)%nsnow) =  vsnow(kk)%hliq(vsnow(kk)%nsnow) - &
                         vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow)*(vsnow(kk)%hsnow(vsnow(kk)%nsnow) - &
                         vsnow(kk)%hliq(vsnow(kk)%nsnow))/(1._dp-vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow))
 
                    vsnow(kk)%hliq(vsnow(kk)%nsnow) = vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow)*(vsnow(kk)%hsnow(vsnow(kk)%nsnow) &
                         - vsnow(kk)%hliq(vsnow(kk)%nsnow))/ &
                         (1._dp-vsnow(kk)%fsnowliq_max(vsnow(kk)%nsnow))
 
                    vsnow(kk)%hsnow(vsnow(kk)%nsnow) = vsnow(kk)%hsnow(vsnow(kk)%nsnow) -  &
                         tmp1d1(kk) + vsnow(kk)%hliq(vsnow(kk)%nsnow)
                    tmp1d3(kk) = vsnow(kk)%dens(vsnow(kk)%nsnow)*(one-vsnow(kk)%hliq(vsnow(kk)%nsnow)) &
                         /vsnow(kk)%hsnow(vsnow(kk)%nsnow)
 
 
                 endif ! (vsnow(kk)%hliq(2).gt.vsnow(kk)%hsnow(2)*vsnow(kk)%fsnowliq_max(2))
 
              endif ! snowpack partially melted
              vsnow(kk)%melt(vsnow(kk)%nsnow) = vsnow(kk)%melt(vsnow(kk)%nsnow) + qmelt(vsnow(kk)%nsnow)
           endif ! (vsnow(kk)%hliq(2).gt.zero).and. &
           !(vsnow(kk)%hliq(2) + qmelt(1)).le.(vsnow(kk)%hsnow(2) + qmelt(1))*vsnow(kk)%fsnowliq_max(2))
 
        endif ! if (vsnow(kk)%nsnow>1)
 
        theta = S(1)*(par(1)%thre) + (par(1)%the - par(1)%thre)
        ! move snow melt from bottom snow layer to top soil + pond
        if (qmelt(vsnow(kk)%nsnow)>zero) then
           vsnow(kk)%Qmelt = vsnow(kk)%Qmelt + qmelt(vsnow(kk)%nsnow)
           if (melt_transfer) then
              h0_tmp(kk) = h0(kk)
              hice_tmp(kk) = hice(kk)
 
              ! total energy in melt-water is zero
 
              ! total energy in old top soil layer
              tmp1d2(kk) = JSoilLayer(Tsoil(1), &
                   dx(1), theta,par(1)%css, par(1)%rho, &
                   h0_tmp(kk), par(1)%thre, par(1)%the, &
                   par(1)%he, one/(par(1)%lambc*freezefac))
 
              !calculate new thetal, consistent with total energy and new pond height
              theta         = S(1)*(par(1)%thre) + (par(1)%the - par(1)%thre)
              theta_tmp = theta
              if (h0(kk)>zero) then
                 h0(kk) = h0(kk) +  qmelt(vsnow(kk)%nsnow)
              elseif ((theta+qmelt(vsnow(kk)%nsnow)/dx(1))<par(1)%the) then
                 h0(kk)=zero
                 theta = theta+qmelt(vsnow(kk)%nsnow)/dx(1)
                 S(1) = (theta - (par(1)%the - par(1)%thre) )/(par(1)%thre)
                 if (S(1).lt.one) then
                    var(1)%isat= 0
                 endif
              elseif ((theta+qmelt(vsnow(kk)%nsnow)/dx(1))>par(1)%the) then
                 h0(kk) = ((theta+qmelt(vsnow(kk)%nsnow)/dx(1))-par(1)%the)*dx(1)
 
                 S(1) = one
                 var(1)%isat = 1
                 ns(kk) = 0
                 theta = par(1)%the
              endif
              Tfreezing(kk) = Tfrz(S(1), par(1)%he, one/(par(1)%lambc*freezefac))
 
              ! calculate energy in new top soil layer
              if (var(1)%iice==0) then
                 var(1)%csoil = theta*rhow*cswat+par(1)%rho*par(1)%css
                 tmp1d3(kk) = (tmp1d2(kk))/((theta*rhow*cswat+par(1)%rho*par(1)%css)*dx(1)+ &
                      h0(kk)*rhow*cswat)
                 Jcol_sensible(kk) = Jcol_sensible(kk) - &
                      var(1)%csoil*dx(1)*(Tsoil(1)) - &
                      (h0_tmp(kk))*cswat*rhow*(Tsoil(1)) + &
                      (theta*rhow*cswat+par(1)%rho*par(1)%css)*dx(1)*(tmp1d3(kk)) + &
                      (h0_tmp(kk))*cswat*rhow*(tmp1d3(kk))
 
                 var(1)%csoil = theta*rhow*cswat+par(1)%rho*par(1)%css
                 Tsoil(1) = tmp1d3(kk)
 
              else
                 ! check if total energy in melt water and top soil (+pond) is enough for complete melting
                 tmp1d3(kk) = var(1)%thetai*dx(1)*rhow*lambdaf + &
                      var(1)%thetai*h0_tmp(kk)/par(1)%thre*rhow*lambdaf
                 tmp1d4(kk) = (Tfreezing(kk))*(rhow*cswat*(theta*dx(1)+h0(kk))+par(1)%rho*par(1)%css*dx(1))
                 if ((tmp1d2(kk)-tmp1d4(kk))>zero) then
                    !  complete melting
                    Jcol_latent_T(kk) =     Jcol_latent_T(kk) + tmp1d3(kk)
                    deltaJ_sensible_S(1) = zero
                    ! Tsoil(1) = var(1)%Tfrz + (tmp1d1(kk)+tmp1d2(kk) -tmp1d4(kk))/ &
                    !      (rhow*cswat*(theta*dx(1)+h0(kk))+par(1)%rho*par(1)%css*dx(1))
 
                    Tsoil(1) = var(1)%Tfrz + (tmp1d2(kk) -tmp1d4(kk))/ &
                         (rhow*cswat*(theta*dx(1)+h0(kk))+par(1)%rho*par(1)%css*dx(1))
 
                    var(1)%iice = 0
                    var(1)%thetai = zero
                    var(1)%thetal = theta
                 else
 
                    Jsoil = tmp1d2(kk)! total energy in  soil layer
                    !check there is a zero
                    tmp1 = GTfrozen(Tsoil(1)-50._dp, Jsoil, dx(1), theta,par(1)%css, par(1)%rho, &
                         h0(kk), par(1)%thre, par(1)%the, par(1)%he, one/(par(1)%lambc*freezefac))
 
                    tmp2 = GTFrozen(Tfreezing(kk), Jsoil, dx(1), theta,par(1)%css, par(1)%rho, &
                         h0(kk), par(1)%thre, par(1)%the, par(1)%he, one/(par(1)%lambc*freezefac))
 
                    ! there is a zero in between
                    if ((tmp1*tmp2) < zero) then
                       tmp1d3(kk) = rtbis_Tfrozen(tmp1d2(kk), dx(1), theta,par(1)%css, par(1)%rho, &
                            h0(kk), par(1)%thre, par(1)%the, par(1)%he, one/(par(1)%lambc*freezefac), &
                            Tsoil(1)-50._dp, Tfreezing(kk), 0.0001_dp)
 
                       tmp1d4(kk) = thetalmax(tmp1d3(kk), S(1), par(1)%he, one/(par(1)%lambc*freezefac), &
                            par(1)%thre, par(1)%the) ! liquid content at new Tsoil
                    else
                       write(*,*) "Found no solution for Tfrozen 4. Stop.", irec, qmelt(1), h0(kk)
                       stop
                    endif
 
                    var(1)%thetal = max(tmp1d4(kk), zero)
                    var(1)%thetai = theta - tmp1d4(kk)
                    hice_tmp(kk) = hice(kk)
                    hice = h0(kk)*var(1)%thetai/par(1)%thre
 
                    ! correct total energy stored in pond + soil
                    Jcol_latent_S(kk) = Jcol_latent_S(kk)- rhow*lambdaf*((hice(kk)-hice_tmp(kk)) + &
                         dx(1)*(var(1)%thetai-thetai(1)))
 
                    Jcol_sensible(kk) = Jcol_sensible(kk) - &
                         var(1)%csoil*dx(1)*(Tsoil(1)) - &
                         (h0_tmp(kk)-hice_tmp(kk))*cswat*rhow*(Tsoil(1)) - &
                         hice_tmp(kk)*rhow*(csice*(Tsoil(1))) + &
                         dx(1)*(tmp1d3(kk))*par(1)%rho*par(1)%css + &
                         (h0(kk)-hice(kk)+var(1)%thetal*dx(1))*cswat*rhow*(tmp1d3(kk)) + &
                         (hice_tmp(kk)+var(1)%thetai)*rhow*(csice*(tmp1d3(kk)))
 
                    thetai(1) = var(1)%thetai
                    Tsoil(1) = tmp1d3(kk)
                 endif ! incomplete melting
              endif ! iice=1
 
              do i=1, vsnow(kk)%nsnow
                 vsnow(kk)%Jsensible(i) = (vsnow(kk)%hsnow(i)-vsnow(kk)%hliq(i))*rhow*csice*(vsnow(kk)%Tsn(i))
                 vsnow(kk)%Jlatent(i) = (vsnow(kk)%hsnow(i)-vsnow(kk)%hliq(i))*rhow*(-lambdaf)
              enddo
              vsnow(kk)%J = sum(vsnow(kk)%Jsensible(1:vsnow(kk)%nsnow)+vsnow(kk)%Jlatent(1:vsnow(kk)%nsnow))
              vsnow(kk)%deltaJ = vsnow(kk)%J - J0snow(kk)
              vsnow(kk)%FluxDivergence = vsnow(kk)%Qadv_rain + vsnow(kk)%Qadv_snow + vsnow(kk)%Qadv_vap + &
                   vsnow(kk)%Qadv_melt + vsnow(kk)%Qcond_net  +  vsnow(kk)%Qadv_transfer
           endif
        endif ! remove  melt water
 
        do i=1, vsnow(kk)%nsnow
           vsnow(kk)%dens(i) = vsnow(kk)%hsnow(i)/vsnow(kk)%depth(i)*rhow
           if (vsnow(kk)%dens(i).lt.50._dp) then
              vsnow(kk)%dens(i) = 50.0_dp
              vsnow(kk)%depth(i) = vsnow(kk)%hsnow(i)*rhow/vsnow(kk)%dens(i)
           endif
           tmp1d3(kk) = (500._dp*(vsnow(kk)%hsnow(i)-vsnow(kk)%hliq(i)) + rhow*vsnow(kk)%hliq(i))/vsnow(kk)%hsnow(i)
           if (vsnow(kk)%dens(i).gt.tmp1d3(kk)) then
              vsnow(kk)%dens(i) = tmp1d3(kk)
              vsnow(kk)%depth(i) = vsnow(kk)%hsnow(i)*rhow/vsnow(kk)%dens(i)
           endif
 
        enddo
 
        vsnow(kk)%totdepth = sum(vsnow(kk)%depth(1:vsnow(kk)%nsnow))
 
        ! adjust number of snow layers if required
        if (nsnow_max>1.and.vsnow(kk)%totdepth > 0.03_dp) then
           tmp1d3(kk) = vsnow(kk)%depth(1)
 
           if (vsnow(kk)%totdepth.lt.0.06_dp) then
              vsnow(kk)%depth(1) = vsnow(kk)%totdepth/2._dp
           else
              vsnow(kk)%depth(1) = 0.03_dp
           endif
           vsnow(kk)%depth(nsnow_max) = vsnow(kk)%totdepth -  vsnow(kk)%depth(1)
 
           if (vsnow(kk)%nsnow == 1) then
              !put excess into new layer below (initialise 2nd snow layer if necessary)
              vsnow(kk)%tsn(vsnow(kk)%nsnow+1) = vsnow(kk)%tsn(1)
              vsnow(kk)%dens(vsnow(kk)%nsnow+1) = vsnow(kk)%dens(1)
              vsnow(kk)%hsnow(1) = vsnow(kk)%depth(1)*(vsnow(kk)%dens(1)/rhow)
              vsnow(kk)%hsnow(vsnow(kk)%nsnow+1) = vsnow(kk)%depth(vsnow(kk)%nsnow+1)*(vsnow(kk)%dens(vsnow(kk)%nsnow+1)/rhow)
              vsnow(kk)%hliq(vsnow(kk)%nsnow+1)  = vsnow(kk)%hliq(1)*vsnow(kk)%hsnow(vsnow(kk)%nsnow+1)/(vsnow(kk)%hsnow(1)+ &
                   vsnow(kk)%hsnow(vsnow(kk)%nsnow+1))
              vsnow(kk)%hliq(1) = vsnow(kk)%hliq(1) - vsnow(kk)%hliq(vsnow(kk)%nsnow+1)
              vsnow(kk)%nsnow = 2
           else
              ! recalculate tsn, dens, hsnow, hliq according to new layer depths
              ! total energy of combined snow pack
              tmp1d1(kk) = rhow*(vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*(csice*vsnow(kk)%tsn(1) - lambdaf) + &
                   rhow*(vsnow(kk)%hsnow(vsnow(kk)%nsnow)-vsnow(kk)%hliq(vsnow(kk)%nsnow))* &
                   (csice*vsnow(kk)%tsn(vsnow(kk)%nsnow) - lambdaf)
 
 
              if (vsnow(kk)%depth(1).le.tmp1d3(kk)) then
                 i= 1 ! if top layer decreases in depth, it retains old density
                 j=2
              else
                 i = 2 ! if bottom layer decreases in depth, it retains old density
                 j=1
              endif
              vsnow(kk)%hliq(i)= vsnow(kk)%hliq(i)*vsnow(kk)%depth(i)*(vsnow(kk)%dens(i)/rhow)/vsnow(kk)%hsnow(i)
              vsnow(kk)%hsnow(j)= (vsnow(kk)%hsnow(i)+vsnow(kk)%hsnow(j)) - vsnow(kk)%depth(i)*(vsnow(kk)%dens(i)/rhow)
              vsnow(kk)%hsnow(i)= vsnow(kk)%depth(i)*(vsnow(kk)%dens(i)/rhow)
 
              ! energy content of augmented layer
              tmp1d2(kk) = tmp1d1(kk) - &
                   rhow*(vsnow(kk)%hsnow(i)-vsnow(kk)%hliq(i))*(csice*vsnow(kk)%tsn(i)- lambdaf)
              if (tmp1d2(kk).lt. -vsnow(kk)%hsnow(j)*rhow*lambdaf) then
                 vsnow(kk)%hliq(j)= zero
                 vsnow(kk)%tsn(j)= (tmp1d2(kk)/(rhow*vsnow(kk)%hsnow(j))+lambdaf)/csice
              else
                 vsnow(kk)%tsn(j)= zero
                 vsnow(kk)%hliq(j)= vsnow(kk)%hsnow(j)+ tmp1d2(kk)/(rhow*lambdaf)
              endif
 
              ! density of augmented layer
              vsnow(kk)%dens(j)= rhow * vsnow(kk)%hsnow(j)/vsnow(kk)%depth(j)
 
           endif
 
        elseif (nsnow_max>1.and.vsnow(kk)%nsnow == 2.and.(vsnow(kk)%totdepth).le.0.03_dp) then
           ! combine top two snow layers into 1
 
           ! total energy of combined snow layer
           tmp1d1(kk) = rhow*(vsnow(kk)%hsnow(1)-vsnow(kk)%hliq(1))*(csice*vsnow(kk)%tsn(1) - lambdaf) + &
                rhow*(vsnow(kk)%hsnow(vsnow(kk)%nsnow)-vsnow(kk)%hliq(vsnow(kk)%nsnow))* &
                (csice*vsnow(kk)%tsn(vsnow(kk)%nsnow) - lambdaf)
           if (tmp1d1(kk).lt. -(vsnow(kk)%hsnow(1)+vsnow(kk)%hsnow(vsnow(kk)%nsnow))*rhow*lambdaf) then
              vsnow(kk)%hliq(1) = zero
              vsnow(kk)%tsn(1) = (tmp1d1(kk)/(rhow*(vsnow(kk)%hsnow(1)+vsnow(kk)%hsnow(vsnow(kk)%nsnow)))+lambdaf)/csice
           else
              vsnow(kk)%tsn(1) = zero
              vsnow(kk)%hliq(1) = (vsnow(kk)%hsnow(1)+vsnow(kk)%hsnow(vsnow(kk)%nsnow)) + tmp1d1(kk)/(rhow*lambdaf)
           endif
 
           vsnow(kk)%tsn(vsnow(kk)%nsnow) = zero
           vsnow(kk)%hliq(vsnow(kk)%nsnow) = zero
           vsnow(kk)%hsnow(1) = vsnow(kk)%hsnow(1) + vsnow(kk)%hsnow(vsnow(kk)%nsnow)
           vsnow(kk)%hsnow(vsnow(kk)%nsnow) = 0
           vsnow(kk)%depth(1) = vsnow(kk)%depth(1) + vsnow(kk)%depth(vsnow(kk)%nsnow)
           vsnow(kk)%depth(vsnow(kk)%nsnow) = 0
           vsnow(kk)%dens(1) = vsnow(kk)%hsnow(1)/vsnow(kk)%depth(1)*rhow
           vsnow(kk)%nsnow = 1
        endif
 
        if (vsnow(kk)%hsnow(1).lt.zero.or.vsnow(kk)%hsnow(nsnow_max).lt.zero) then
           write(*,*) irec, vsnow(kk)%hsnow(1), vsnow(kk)%hsnow(nsnow_max)
           stop
        endif
 
        do i=1, vsnow(kk)%nsnow
           vsnow(kk)%Dv(i) = Dva*((vsnow(kk)%tsn(i)+Tzero)/Tzero)**1.88_dp ! m2 s-1
           vsnow(kk)%sl(i) = slope_esat_ice(vsnow(kk)%tsn(i))*Mw/thousand/Rgas/(vsnow(kk)%tsn(i)+Tzero)
           vsnow(kk)%kE(i)     = vsnow(kk)%Dv(i)*vsnow(kk)%sl(i)*thousand*lambdaf
           vsnow(kk)%kH(i) = 3.2217e-6_dp * vsnow(kk)%dens(i)**2
           vsnow(kk)%kth(i) = vsnow(kk)%kE(i) + vsnow(kk)%kH(i)
           vsnow(kk)%cv(i) = esat_ice(vsnow(kk)%tsn(i))*Mw/thousand/Rgas/(vsnow(kk)%tsn(i)+Tzero) ! m3 m-3
           vsnow(kk)%depth(i) = vsnow(kk)%hsnow(i)/(vsnow(kk)%dens(i)/rhow)
        enddo
 
     endif ! dedicated snow layer
 
    END SUBROUTINE snow_adjust

    SUBROUTINE getfluxes_vp_1d(n, dx, vtop, vbot, parin, var, & 
        hint, phimin, q, qya, qyb, qTa, qTb, &
        ql, qlya, qlyb, qv, qvT, qvh, qvya, & 
        qvyb, &
        iflux, init, getq0, getqn, Tsoil, T0, nsat, nsatlast)
     !******************************************
     !     (1) Declaring variable
     !******************************************
        IMPLICIT NONE
    
        INTEGER(i4),                 INTENT(IN)    :: n
        REAL(dp),    DIMENSION(1:n), INTENT(IN)    :: dx
        TYPE(vars),                   INTENT(IN)    :: vtop
        TYPE(vars),                   INTENT(IN)    :: vbot
        TYPE(params), DIMENSION(1:n), INTENT(IN)    :: parin
        TYPE(vars),   DIMENSION(1:n), INTENT(INOUT)    :: var
        REAL(dp),    DIMENSION(1:n), INTENT(INOUT) :: hint
        REAL(dp),    DIMENSION(1:n), INTENT(INOUT) :: phimin
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT) :: q
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT) :: qya
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT) :: qyb
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT) :: qTa
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT) :: qTb
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT)   :: ql
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT)   :: qlya
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT)   :: qlyb
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT)   :: qv
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT)   :: qvT
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT)   :: qvh
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT)   :: qvya
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT)   :: qvyb
        INTEGER(i4),                 INTENT(IN)    :: iflux
        LOGICAL,                      INTENT(IN)    :: init
        LOGICAL,                      INTENT(IN)    :: getq0
        LOGICAL,                      INTENT(IN)    :: getqn
        REAL(dp),    DIMENSION(1:n), INTENT(IN)    :: Tsoil
        REAL(dp),                    INTENT(IN)    :: T0
        INTEGER(i4),                 INTENT(IN)    :: nsat
        INTEGER(i4),                 INTENT(IN)    :: nsatlast
    
        LOGICAL               :: flag, limit
        INTEGER(i4)          :: i, itmp, l
        REAL(dp)             :: dphii1, dhi, h1, h2, hi, Khi1, Khi2, phii1, q2, qya2, qyb2, y, y1, y2
        REAL(dp)             :: qTa2, qTb2
        TYPE(vars)            :: vi1, vi2
        REAL(dp), DIMENSION(1:n-1) :: dz
     !******************************************
     !     (2) Variable Explanation
     !******************************************
        ! Gets fluxes q and partial derivs qya, qyb wrt S (if unsat) or phi (if sat).
        ! Fluxes at top and bottom of profile, and fluxes due to plant extraction of
        ! water are included.
    
        ! Definitions of arguments:
        ! k     - land point
        ! n     - no. of soil layers.
        ! jt(1:n)   - layer soil type nos.
        ! dx(1:n)   - layer thicknesses.
        ! dz(1:n-1)   - distances between layer centres.
        ! vtop    - water vars at soil surface.
        ! vbot    - water vars at bottom of profile.
        ! var(1:n)   - water vars at layer centres.
        ! hint(1:n)   - values of h at interfaces are stored sequentially in hint.
        ! phimin(1:n) - similarly for phi at hmin in layers above interfaces.
        ! q(0:n)   - fluxes; q(i), i=1,...,n-1 is flux from layer i to layer i+1.
        !    q(0) is surface flux and q(n) is flux at bottom of profile.
        ! qya(0:n)   - partial deriv of q(i), i=0,...,n, wrt the variable to be solved
        !    for (S, phi or h) at upper end of flow path.
        ! qyb(0:n)   - ditto for var at lower end.
        ! iflux    - if iflux/=1, get only fluxes involving sat layers.
        ! init    - true if hint and phimin to be initialised.
        ! getq0    - true if q(0) required.
        ! getqn    - true if q(n) required.
    
     !******************************************
     !     (3) Calculate dz (distance of mid layers)
     !******************************************
        dz(:) = half*(dx(1:n-1)+dx(2:n))
 
     !******************************************
     !     (4) Set vars of soil to Zero
     !******************************************
        vi1 = zerovars()
        vi2 = zerovars()
    
     !******************************************
     !     (5) Calculate top_flux (if getq0 = TRUE )
     !      q(0), qTa(0),  qTb(0), qv(0), ql(0),  qvya(0)
     !******************************************
        if ((iflux==1) .or. (var(1)%isat /= 0)) then ! get top flux if required
            if (getq0) then
                call flux(parin(1), vtop, var(1), half*dx(1), q(0), qya(0), qyb(0), qTa(0), qTb(0))
        
                q(0)  = q(0)+(T0-Tsoil(1))*(var(1)%kE)/thousand/var(1)%lambdav/dx(1)*two
        
                qTa(0) = zero
                qTb(0) = -(var(1)%kE)/thousand/var(1)%lambdav/dx(1)*two
                qv(0)  = (vtop%phiv-var(1)%phiv)/dx(1)*two +(T0-Tsoil(1))*(var(1)%kE)/thousand/var(1)%lambdav/dx(1)*two
                ql(0)  = q(0) - qv(0)
        
                qvya(0) = zero
        
                if (vtop%isat==0) then
                    qvyb(0) = vtop%phivS/dx(1)*two
            else
                qvyb(0) = zero
            end if
    
            qlya(0) = qya(0) - qvya(0)
            qlyb(0) = qyb(0) - qvyb(0)
            end if
        end if

        ! otherwise undefined
        qvh(0) = zero
        qvT(0) = zero
    
     !******************************************
     !     (6) Loop do i=1, n-1 : Calculate layers fluxes
     !******************************************
        ! isat: 0 for unsaturated layers, 1 for saturated layers
        l = 0
        do i=1, n-1
        !----------------------------------
        !        6.1. Calculate flux
        !----------------------------------
            if (iflux==1 .or. var(i)%isat/=0 .or. var(i+1)%isat/=0 .or. nsat/=nsatlast) then ! get flux
             !........................................................
             !           a. Call flux: Similiar soil type (ishorizon) 
             !........................................................
                ! parin(i)    - parameters of soil
                ! var(i)      - variables of soil (At current/above layer)
                ! var(i+1)    - variables of soil (At below layer)
                ! dz(i)       - thickness of mid point of soil layer
        
                !----------UNKNOWNS----------------------------------------------
                ! q(i)        - soil moisture flux(qw in equation?) at current layer
                ! qya(i)      - derivative of q(i) wrt S(i) at above layer
                ! qyb(i)      - derivative of q(i) wrt S(i) at below layer
                ! qTa(i)      - derivative of q(i) wrt T(i) at above layer
                ! qTb(i)      - derivative of q(i) wrt T(i) at below layer
                if (parin(i)%ishorizon == parin(i+1)%ishorizon) then ! same soil type, no interface
                    if(var(i)%phi <= 1.e-20_dp) var(i)%phi = zero
                    if(var(i+1)%phi <= 1.e-20_dp) var(i+1)%phi = zero

                    call flux(parin(i), var(i), var(i+1), dz(i), q(i), & 
                                qya(i), qyb(i), qTa(i), qTb(i))
            
             !........................................................
             !           b. Call flux: Different soil type (ishorizon) 
             !........................................................
                else ! interface
                    l  = l+1 ! counter interface (l)
        
                    !,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    !              b1. Call hyofh: initialize hint (h at the interface)
                    !                    h & y for two layers
                    !,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    if (init) then ! initialise
                        call hyofh(hmin, parin(i)%lam, parin(i)%eta, parin(i)%Ke, &
                            parin(i)%he, vi1%K, Khi1, phimin(l)) ! get phi at hmin
                        h1 = var(i)%h
                        h2 = var(i+1)%h
                        y1 = var(i)%K*dx(i+1)
                        y2 = var(i+1)%K*dx(i)
        
                        ! equate fluxes (K constant) to get initial estimate of h at interface
                        hint(l) = (y1*h1 + y2*h2 + half*gf*(var(i)%K-var(i+1)%K)*dx(i)*dx(i+1)) / (y1+y2)
                    end if
                    
                    
                    !,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    !              b2. Loop do while (flag) : Newton Iteration
                    !                    to get hi at interface for equal fluxes
                    !,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    ! iterate to get hi at interface for equal fluxes ---> using Newton's method
                    ! get dphii1 at interface in upper layer, because of better linearity,
                    ! then convert to dhi
                    hi   = hint(l)
                    itmp = 0
        
                    flag = .true.
                    do while (flag)
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !                 b2.1. if itmp > 100
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        itmp = itmp+1
                        if (itmp>100) then
                           !  write(*,*) "getfluxes: too many iterations finding interface h"
                            !stop
                            if(var(i)%phi <= 1.e-20_dp) var(i)%phi = zero
                            if(var(i+1)%phi <= 1.e-20_dp) var(i+1)%phi = zero

                            call flux(parin(i), var(i), var(i+1), dz(i), q(i), qya(i), qyb(i), qTa(i), qTb(i))
                            goto 111
                        end if
                        
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !                 b2.2. Call hyofh: filling vi1 (vars interface 1)
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !   TYPE(vars)            :: vi1, vi2
                        !   vi1 - variables of soil for layer above(1)
                        !   vi2 - variables of soil for layer below(2)
        
                        ! Unsaturated
                        if (hi<parin(i)%he) then 
                            vi1%isat = 0
                            call hyofh(hi, parin(i)%lam, parin(i)%eta, parin(i)%Ke, parin(i)%he, &
                                        vi1%K, Khi1, phii1)
                            vi1%KS = Khi1/vi1%K ! use dK/dphi, not dK/dS
                        ! Saturated
                        else 
                            vi1%isat = 1
                            vi1%K    = var(i)%Ksat
                            phii1    = var(i)%phie+(hi-parin(i)%he)*var(i)%Ksat
                            vi1%KS   = zero
                        end if
        
        
                        vi1%h    = hi
                        vi1%phi  = phii1
                        vi1%phiS = one ! use dphi/dphi not dphi/dS
                        ! define phiT=0, KT=0 to be consistent with undefined version
                        vi1%phiT = zero
                        vi1%KT   = zero
                        ! macropore_factor was not defined but is used in flux(), set to factor of upper layer
                        vi1%macropore_factor = var(i)%macropore_factor
                        
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !                 b2.3. Call flux: for vi1
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if(var(i)%phi <= 1.e-20_dp) var(i)%phi = zero
                        if(vi1%phi <= 1.e-20_dp) vi1%phi = zero
                        call flux(parin(i), var(i), vi1, half*dx(i), q(i), qya(i), qyb(i), qTa(i), qTb(i))
        
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !                 b2.4. Call hyofh: filling vi2 (vars interface 2)
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !unsaturated
                        if (hi<parin(i+1)%he) then
                            vi2%isat = 0
                            call hyofh(hi, parin(i+1)%lam, parin(i+1)%eta, parin(i+1)%Ke, parin(i+1)%he, vi2%K, Khi2, vi2%phi)
                            vi2%KS = Khi2/vi2%K ! dK/dphi
                        
                        !saturated
                        else
                            vi2%isat = 1
                            vi2%K    = var(i+1)%Ksat
                            vi2%phi  = var(i+1)%phie+(hi-parin(i+1)%he)*var(i+1)%Ksat
                        end if
        
                        vi2%h    = hi
                        vi2%phiS = one ! dphi/dphi
                        ! define phiT=0, KT=0 to be consitent with undefined version
                        vi2%phiT = zero
                        vi2%KT   = zero
                        ! macropore_factor was not defined but is used in flux(), set to factor of lower layer
                        vi2%macropore_factor = var(i+1)%macropore_factor
        
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !                 b2.5. Call flux: for vi2
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if(var(i+1)%phi <= 1.e-20_dp) var(i+1)%phi = zero
                        if(vi2%phi <= 1.e-20_dp) vi2%phi = zero
                        call flux(parin(i+1), vi2, var(i+1), half*dx(i+1), q2, qya2, qyb2, qTa2, qTb2)
        
                        
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !                 b2.6. Re-calcu qya2 : partial deriv wrt phii1
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        qya2   = qya2*vi2%K/vi1%K 
                        
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !                 b2.7. Equal flux adjustment
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        dphii1 = -(q(i)-q2)/(qyb(i)-qya2)
                        limit  = .false.
        
                        if (phii1+dphii1<=phimin(l)) then ! out of range
                            limit  = .true.
                            dphii1 = -half*(phii1-phimin(l))
                        end if
        
                        phii1 = phii1+dphii1
                        dhi   = dphii1/(vi1%K+half*vi1%KS*dphii1) ! 2nd order Pade approx
                        if (-vi1%KS*dphii1 > 1.5_dp*vi1%K) then ! use 1st order approx for dhi
                            dhi = dphii1/vi1%K
                        end if
                        hi = hi+dhi
                        
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        !                 b2.8. flag = .false. IF convergene
                        !                       check for convergence - dphi/(mean phi)<=dpmaxr
                        !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if (.not.(limit .or. abs(dphii1/(phii1-half*dphii1))>dpmaxr)) then
                            flag = .false.
                        end if
                    end do ! while flag
                    
                    !,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    !              b3. Update q(i) and hint(l)
                    !,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    q(i)    = q(i) + qyb(i)*dphii1
                    hint(l) = hi
                    
                    !,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    !              b4. Adjust: y, qya(i), qyb(i)  Derivative
                    !,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                    y      = one/(qya2-qyb(i))
                    qya(i) = qya(i)*qya2*y
                    qyb(i) = -qyb2*qyb(i)*y
                end if
            end if
        
        !----------------------------------
        !        6.2. Calculate ql & qvT || Updating qTa,qTb
        !----------------------------------
            ! ql(i)       - flux of liquid water
            ! qvT(i)      - flux of vapor water in Temperature term
            ! qTa(i)      - derivative of q(i) wrt T(i) at above layer
            ! qTb(i)      - derivative of q(i) wrt T(i) at below layer
            111     ql(i)  = q(i)
                    qvT(i) = (Tsoil(i)-Tsoil(i+1))*(var(i)%kE+var(i+1)%kE)/thousand/var(1)%lambdav/two/dz(i)
    
                    ! updating after qvT is calculated
                    qTa(i) = qTa(i)+(var(i)%kE+var(i+1)%kE)/thousand/var(1)%lambdav/two/dz(i)
                    qTb(i) = qTb(i)-(var(i)%kE+var(i+1)%kE)/thousand/var(1)%lambdav/two/dz(i)
                    
        
        !----------------------------------
        !        6.3. Calculate qvh, qv, q (final)
        !----------------------------------
            ! qvh(i)      - flux of vapor water in pressure head term
            ! qv(i)       -  flux of vapor water (qvT + qvh)
            ! q(i)        - total flux of water or soil moisture (qv + ql)
        
            !MC The full description (next two lines) gave problems before ->  third line
            !   Try again original
            !VH Do both formulations come to the same? I found that I could not reproduce
            !   Barnes and Allison semi-analytic solution with original
            !MC This should be re-checked
            ! qvh(i) = ((((Tsoil(i)+Tzero)/Tzero)**1.88+((Tsoil(i+1)+Tzero)/Tzero)**1.88)/two) &
            !      * ((var(i)%cvsat+var(i+1)%cvsat)/two)*(var(i)%phiv-var(i+1)%phiv)/dz(i)
            qvh(i) = (half*(exp(1.88_dp*log((Tsoil(i)+Tzero)/Tzero))+exp(1.88_dp*log((Tsoil(i+1)+Tzero)/Tzero)))) &
                * (half*(var(i)%cvsat+var(i+1)%cvsat)) * (var(i)%phiv-var(i+1)%phiv)/dz(i)
            ! qvh(i) = ((var(i)%Dv+var(i+1)%Dv)/two)* ((var(i)%cvsat+var(i+1)%cvsat)/two)*(var(i)%rh-var(i+1)%rh)/dz(i)
            qv(i)  = qvh(i) + qvT(i) ! whole vapour flux has one part from humidity (qvh) and one part from temp diff (qvT)
            q(i)   = qv(i) + ql(i)
        
        
        !--------------------------------------------------
        !        6.4. Calculate qvya, qvyb: derivative qv wrt S
        !--------------------------------------------------
            ! Upper layer, Unsaturated
            if (var(i)%isat==0) then
            ! qvya(i) = var(i)%phivS/dz(i) *((((Tsoil(i)+Tzero)/Tzero)**1.88_dp+ &
            !      ((Tsoil(i+1)+Tzero)/Tzero)**1.88_dp)/two) &
            !      * ((var(i)%cvsat+var(i+1)%cvsat)/two)
            qvya(i) = var(i)%phivS/dz(i)*(half*(exp(1.88_dp*log((Tsoil(i)+Tzero)/Tzero)) + &
                    exp(1.88_dp*log((Tsoil(i+1)+Tzero)/Tzero)))) &
                    * (half*(var(i)%cvsat+var(i+1)%cvsat))
            ! Upper layer, Saturated
            else
            qvya(i) = zero
            end if
            
            ! Below layer, Unsaturated
            if (var(i)%isat==0) then
            ! qvyb(i) = -var(i+1)%phivS/dz(i) *((((Tsoil(i)+Tzero)/Tzero)**1.88_dp+ &
            !      ((Tsoil(i+1)+Tzero)/Tzero)**1.88_dp)/two) &
            !      * ((var(i)%cvsat+var(i+1)%cvsat)/two)
            qvyb(i) = -var(i+1)%phivS/dz(i) *(half*(exp(1.88_dp*log((Tsoil(i)+Tzero)/Tzero)) + &
                    exp(1.88_dp*log((Tsoil(i+1)+Tzero)/Tzero)))) &
                    * (half*(var(i)%cvsat+var(i+1)%cvsat))
            ! Below layer, Saturated
            else
            qvyb(i) = zero
            end if
        !--------------------------------------------------
        !        6.5. Calculate qly & qy: derivative ql & q wrt S
        !--------------------------------------------------
            qlya(i) = qya(i)
            qlyb(i) = qyb(i)
    
            qya(i)  = qlya(i) + qvya(i)
            qyb(i)  = qlyb(i) + qvyb(i)
        end do
 
     !******************************************
     !     (7) Calculate bot_flux (if getqn = TRUE )
     !           qvya, qvyb(n),  qlya(n), qlyb(n)
     !******************************************
        if (iflux==1 .or. var(n)%isat/=0) then ! get bottom flux if required
            if (getqn) then
               if(botbc == "groundwater") then
                  call flux(parin(n), var(n), vbot, half*dx(n)+abs(vbot%h), q(n), qya(n), qyb(n), qTa(n), qTb(n))
               else
                  call flux(parin(n), var(n), vbot, half*dx(n), q(n), qya(n), qyb(n), qTa(n), qTb(n))
               end if
                qvya(n) = zero
                qvyb(n) = zero
                qlya(n) = qya(n)
                qlyb(n) = zero
            else
                qvya(n) = zero
                qvyb(n) = zero
                qlya(n) = qya(n)
                qlyb(n) = zero
            end if

            !>> if BC is head or flux from aquifer below...cont ...
            
        else
            qvya(n) = zero
            qvyb(n) = zero
            qlya(n) = qya(n)
            qlyb(n) = zero
        end if

        ! otherwise undefined
        ql(n)  = q(n)
        qv(n)  = zero
        qvh(n) = zero
        qvT(n) = zero
    
     !******************************************
     !     (8) IF Dv=0, modify water flux (q)
     !              Dv -  diffusivity of water vapour in the bulk soil
     !******************************************
        do i=1, n-1
            if (var(i)%Dv == zero .or. var(i+1)%Dv == zero) then
                q(i)    = q(i) - qv(i)
                qya(i)  = qya(i) - qvya(i)
                qyb(i)  = qyb(i) - qvyb(i)
                qv(i)   = zero
                
                qvya(i) = zero
                qvyb(i) = zero
            endif
        enddo
 
    END SUBROUTINE getfluxes_vp_1d

    SUBROUTINE getfluxes_vp_2d(dx, vtop, vbot, parin, var, hint, phimin, i_q, i_qya, i_qyb, i_qTa, i_qTb, &
        i_ql, i_qlya, i_qlyb, i_qv, i_qvT, i_qvh, i_qvya, i_qvyb, iflux, init, getq0, getqn, Tsoil, T0, nsat, nsatlast)
 
     IMPLICIT NONE
 
     REAL(dp),    DIMENSION(:,:), INTENT(IN)    :: dx      ! 1:n
     TYPE(vars),   DIMENSION(:),   INTENT(IN)    :: vtop
     TYPE(vars),   DIMENSION(:),   INTENT(IN)    :: vbot
     TYPE(params), DIMENSION(:,:), INTENT(IN)    :: parin   ! 1:n
     TYPE(vars),   DIMENSION(:,:), INTENT(IN)    :: var     ! 1:n
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: hint    ! 1:n
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: phimin  ! 1:n
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_q       ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qya     ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qyb     ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qTa     ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qTb     ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(OUT)   :: i_ql      ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(OUT)   :: i_qlya    ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(OUT)   :: i_qlyb    ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(OUT)   :: i_qv      ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(OUT)   :: i_qvT     ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(OUT)   :: i_qvh     ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(OUT)   :: i_qvya    ! 0:n
     REAL(dp),    DIMENSION(:,:), INTENT(OUT)   :: i_qvyb    ! 0:n
     INTEGER(i4), DIMENSION(:),   INTENT(IN)    :: iflux
     LOGICAL,      DIMENSION(:),   INTENT(IN)    :: init
     LOGICAL,      DIMENSION(:),   INTENT(IN)    :: getq0
     LOGICAL,      DIMENSION(:),   INTENT(IN)    :: getqn
     REAL(dp),    DIMENSION(:,:), INTENT(IN)    :: Tsoil   ! 1:n
     REAL(dp),    DIMENSION(:),   INTENT(IN)    :: T0
     INTEGER(i4), DIMENSION(:),   INTENT(IN)    :: nsat
     INTEGER(i4), DIMENSION(:),   INTENT(IN)    :: nsatlast

     ! Gets fluxes q and partial derivs qya, qyb wrt S (if unsat) or phi (if sat).
     ! Fluxes at top and bottom of profile, and fluxes due to plant extraction of
     ! water are included.
     ! Definitions of arguments:
     ! k     - land point
     ! n     - no. of soil layers.
     ! jt(1:n)   - layer soil type nos.
     ! dx(1:n)   - layer thicknesses.
     ! dz(1:n-1)   - distances between layer centres.
     ! vtop    - water vars at soil surface.
     ! vbot    - water vars at bottom of profile.
     ! var(1:n)   - water vars at layer centres.
     ! hint(1:n)   - values of h at interfaces are stored sequentially in hint.
     ! phimin(1:n) - similarly for phi at hmin in layers above interfaces.
     ! q(0:n)   - fluxes; q(i), i=1,...,n-1 is flux from layer i to layer i+1.
     !    q(0) is surface flux and q(n) is flux at bottom of profile.
     ! qya(0:n)   - partial deriv of q(i), i=0,...,n, wrt the variable to be solved
     !    for (S, phi or h) at upper end of flow path.
     ! qyb(0:n)   - ditto for var at lower end.
     ! iflux    - if iflux/=1, get only fluxes involving sat layers.
     ! init    - true if hint and phimin to be initialised.
     ! getq0    - true if q(0) required.
     ! getqn    - true if q(n) required.
     
     LOGICAL,      DIMENSION(1:size(dx,1))                :: limit, l1, l2, l3
     REAL(dp),    DIMENSION(1:size(dx,1))                :: dphii1, dhi, h1, h2, hi, Khi1, Khi2, phii1
     REAL(dp),    DIMENSION(1:size(dx,1))                :: q2, qya2, qyb2, y, y1, y2
     REAL(dp),    DIMENSION(1:size(dx,1))                :: htmp
     REAL(dp),    DIMENSION(1:size(dx,1))                :: ztmp1, ztmp2, ztmp3, ztmp4, ztmp5
     TYPE(vars),   DIMENSION(1:size(dx,1))                :: vi1, vi2
     REAL(dp),    DIMENSION(1:size(dx,1),1:size(dx,2)-1) :: dz
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: q
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qya
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qyb
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qTa
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qTb
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: ql
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qlya
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qlyb
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qv
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qvT
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qvh
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qvya
     REAL(dp),    DIMENSION(1:size(dx,1),0:size(dx,2))   :: qvyb
     TYPE(vars)    :: vtmp
     INTEGER(i4)  :: i, n, mp, itmp
     
     print *, "This Subroutine 2D is used...."
     mp = size(dx,1)
     n  = size(dx,2)
     dz(:,1:n-1) = half*(dx(:,1:n-1)+dx(:,2:n))
     q(:,0:n)    = i_q(:,1:n+1)
     qya(:,0:n)  = i_qya(:,1:n+1)
     qyb(:,0:n)  = i_qyb(:,1:n+1)
     qTa(:,0:n)  = i_qTa(:,1:n+1)
     qTb(:,0:n)  = i_qTb(:,1:n+1)
     ql(:,0:n)   = zero
     qlya(:,0:n) = zero
     qlyb(:,0:n) = zero
     qv(:,0:n)   = zero
     qvT(:,0:n)  = zero
     qvh(:,0:n)  = zero
     qvya(:,0:n) = zero
     qvyb(:,0:n) = zero
 
     vtmp = zerovars()
     vi1  = spread(vtmp,1,mp)
     vi2  = spread(vtmp,1,mp)
     ztmp1(:) = zero
     ztmp2(:) = zero
     ztmp3(:) = zero
     ztmp4(:) = zero
     ztmp5(:) = zero
 
     l1(:)   = ((iflux(:)==1) .or. (var(:,1)%isat /= 0)) .and. getq0(:)
     if (any(l1(:))) &
          call flux(parin(:,1), vtop(:), var(:,1), half*dx(:,1), ztmp1(:), ztmp2(:), ztmp3(:), ztmp4(:), ztmp5(:))
     where (l1(:)) ! get top flux if required
        q(:,0)   = ztmp1(:)
        qya(:,0) = ztmp2(:)
        qyb(:,0) = ztmp3(:)
        qTa(:,0) = ztmp4(:)
        qTb(:,0) = ztmp5(:)
 
        q(:,0)    = q(:,0)+(T0(:)-Tsoil(:,1))*(var(:,1)%kE)/thousand/var(:,1)%lambdav/dx(:,1)*two
        qTa(:,0)  = zero
        qTb(:,0)  = -(var(:,1)%kE)/thousand/var(:,1)%lambdav/dx(:,1)*two
        qv(:,0)   = (vtop(:)%phiv-var(:,1)%phiv)/dx(:,1)*two +(T0(:)-Tsoil(:,1))* &
             (var(:,1)%kE)/thousand/var(:,1)%lambdav/dx(:,1)*two
        ql(:,0)   = q(:,0) - qv(:,0)
        qvya(:,0) = zero
        qvyb(:,0) = zero ! isat==0 below
        qlya(:,0) = qya(:,0) - qvya(:,0)
        qlyb(:,0) = qyb(:,0) - qvyb(:,0)
     endwhere
     where (l1(:) .and. (vtop(:)%isat==0))
        qvyb(:,0) = vtop(:)%phivS/dx(:,1)*two
        qlyb(:,0) = qyb(:,0) - qvyb(:,0)
     endwhere
     ! otherwise undefined
     qvh(:,0) = zero
     qvT(:,0) = zero
 
     ! get other fluxes
     do i=1, n-1
        l1(:) = (iflux(:)==1 .or. var(:,i)%isat/=0 .or. var(:,i+1)%isat/=0 .or. nsat(:)/=nsatlast(:))
        if (any(l1(:) .and. (parin(:,i)%ishorizon==parin(:,i+1)%ishorizon))) &
             call flux(parin(:,i), var(:,i), var(:,i+1), dz(:,i), ztmp1(:), ztmp2(:), ztmp3(:), ztmp4(:), ztmp5(:))
        where (l1(:) .and. (parin(:,i)%ishorizon==parin(:,i+1)%ishorizon)) ! same soil type, no interface
           q(:,i)   = ztmp1(:)
           qya(:,i) = ztmp2(:)
           qyb(:,i) = ztmp3(:)
           qTa(:,i) = ztmp4(:)
           qTb(:,i) = ztmp5(:)
        endwhere
        l2(:) = l1(:) .and. (parin(:,i)%ishorizon /= parin(:,i+1)%ishorizon) ! interface
        htmp(:) = hmin
        if (any(l2(:) .and. init(:))) &
             call hyofh(htmp(:), parin(:,i)%lam, parin(:,i)%eta, parin(:,i)%Ke, parin(:,i)%he, ztmp1(:), ztmp2(:), ztmp3(:))
        where (l2(:) .and. init(:)) ! interface & get phi at hmin
           vi1(:)%K    = ztmp1(:)
           Khi1(:)     = ztmp2(:)
           phimin(:,i) = ztmp3(:)
           h1(:)       = var(:,i)%h
           h2(:)       = var(:,i+1)%h
           y1(:)       = var(:,i)%K*dx(:,i+1)
           y2(:)       = var(:,i+1)%K*dx(:,i)
           ! equate fluxes (K constant) to get initial estimate of h at interface
           hint(:,i) = (y1(:)*h1(:) + y2(:)*h2(:) + half*gf*(var(:,i)%K-var(:,i+1)%K)*dx(:,i)*dx(:,i+1)) / (y1(:)+y2(:))
        endwhere
        !where ((.not. l2(:)) .and. init(:)) hint(:,i) = zero
        hi(:)   = hint(:,i)
        ! iterate to get hi at interface for equal fluxes using Newton's method
        ! get dphii1 at interface in upper layer, because of better linearity,
        ! then convert to dhi
        if (any(l2(:))) then
           l3(:)    = l2(:)
           limit(:) = .false.
           do itmp=1, 100
              if (any(l3(:) .and. (hi(:)<parin(:,i)%he))) &
                   call hyofh(hi(:), parin(:,i)%lam, parin(:,i)%eta, parin(:,i)%Ke, parin(:,i)%he, ztmp1(:), ztmp2(:), ztmp3(:))
              where (l3(:) .and. (hi(:)<parin(:,i)%he))
                 vi1(:)%isat = 0
                 vi1(:)%K    = ztmp1(:)
                 Khi1(:)     = ztmp2(:)
                 phii1(:)    = ztmp3(:)
                 vi1(:)%KS   = Khi1(:)/vi1(:)%K ! use dK/dphi, not dK/dS
              endwhere
              where (l3(:) .and. (hi(:)>=parin(:,i)%he))
                 vi1(:)%isat = 1
                 vi1(:)%K    = var(:,i)%Ksat
                 phii1(:)    = var(:,i)%phie+(hi(:)-parin(:,i)%he)*var(:,i)%Ksat
                 vi1(:)%KS   = zero
              endwhere
 
              where (l3(:))
                 vi1(:)%h    = hi(:)
                 vi1(:)%phi  = phii1(:)
                 vi1(:)%phiS = one ! use dphi/dphi not dphi/dS
                 !define phiT=0, KT=0 to be consitent with undefined version
                 vi1(:)%phiT = zero
                 vi1(:)%KT   = zero
                 ! macropore_factor was not defined but is used in flux(), set to factor of upper layer
                 vi1(:)%macropore_factor = var(:,i)%macropore_factor
              endwhere
 
              if (any(l3(:))) &
                   call flux(parin(:,i), var(:,i), vi1(:), half*dx(:,i), ztmp1(:), ztmp2(:), ztmp3(:), ztmp4(:), ztmp5(:))
              where (l3(:))
                 q(:,i)      = ztmp1(:)
                 qya(:,i)    = ztmp2(:)
                 qyb(:,i)    = ztmp3(:)
                 qTa(:,i)    = ztmp4(:)
                 qTb(:,i)    = ztmp5(:)
              endwhere
 
              if (any(l3(:) .and. (hi(:)<parin(:,i+1)%he))) &
                   call hyofh(hi(:), parin(:,i+1)%lam, parin(:,i+1)%eta, parin(:,i+1)%Ke, parin(:,i+1)%he, &
                   ztmp1(:), ztmp2(:), ztmp3(:))
              where (l3(:) .and. (hi(:)<parin(:,i+1)%he))
                 vi2(:)%K    = ztmp1(:)
                 Khi2(:)     = ztmp2(:)
                 vi2(:)%phi  = ztmp3(:)
                 vi2(:)%isat = 0
                 vi2(:)%KS   = Khi2(:)/vi2(:)%K ! dK/dphi
              endwhere
              where (l3(:) .and. (hi(:)>=parin(:,i+1)%he))
                 vi2(:)%isat = 1
                 vi2(:)%K    = var(:,i+1)%Ksat
                 vi2(:)%phi  = var(:,i+1)%phie+(hi(:)-parin(:,i+1)%he)*var(:,i+1)%Ksat
              endwhere
 
              where (l3(:))
                 vi2(:)%h    = hi(:)
                 vi2(:)%phiS = one ! dphi/dphi
                 ! define phiT=0, KT=0 to be consitent with undefined version
                 vi2(:)%phiT = zero
                 vi2(:)%KT   = zero
                 ! macropore_factor was not defined but is used in flux(), set to factor of lower layer
                 vi2(:)%macropore_factor = var(:,i+1)%macropore_factor
              endwhere
              if (any(l3(:))) &
                   call flux(parin(:,i+1), vi2(:), var(:,i+1), half*dx(:,i+1), ztmp1(:), ztmp2(:), ztmp3(:), ztmp4(:), ztmp5(:))
              where (l3(:))
                 q2(:)     = ztmp1(:)
                 qya2(:)   = ztmp2(:)
                 qyb2(:)   = ztmp3(:)
                 qya2(:)   = qya2(:)*vi2(:)%K/vi1(:)%K ! partial deriv wrt phii1
                 ! adjust for equal fluxes
                 dphii1(:) = -(q(:,i)-q2(:))/(qyb(:,i)-qya2(:))
                 limit(:)  = .false.
              endwhere
              where (l3(:) .and. (phii1(:)+dphii1(:)<=phimin(:,i))) ! out of range
                 limit(:)  = .true.
                 dphii1(:) = -half*(phii1(:)-phimin(:,i))
              endwhere
              where (l3(:))
                 phii1(:) = phii1(:)+dphii1(:)
                 dhi(:)   = dphii1(:)/(vi1(:)%K+half*vi1(:)%KS*dphii1(:)) ! 2nd order Pade approx
              endwhere
              where (l3(:) .and. (-vi1%KS*dphii1 > 1.5_dp*vi1%K)) ! use 1st order approx for dhi
                 dhi(:) = dphii1(:)/vi1(:)%K
              endwhere
              where (l3(:))
                 hi(:) = hi(:)+dhi(:)
              endwhere
 
              ! check for convergence - dphi/(mean phi)<=dpmaxr
              where (l3(:) .and. &
                   .not. (limit(:) .or. (abs(dphii1(:)/(phii1(:)-half*dphii1(:)))>dpmaxr))) l3(:) = .false.
              if (.not. any(l3(:))) exit
           end do ! do itmp=1, 100
           if (itmp>=100) then
              !write(*,*) "getfluxes: too many iterations finding interface h"
              !stop
              if (any(l2(:) .and. l3(:))) &
                   call flux(parin(:,i), var(:,i), var(:,i+1), dz(:,i), ztmp1(:), ztmp2(:), ztmp3(:), ztmp4(:), ztmp5(:))
              where (l2(:) .and. l3(:))
                 q(:,i)   = ztmp1(:)
                 qya(:,i) = ztmp2(:)
                 qyb(:,i) = ztmp3(:)
                 qTa(:,i) = ztmp4(:)
                 qTb(:,i) = ztmp5(:)
              endwhere
           else
              where (l2(:) .and. (.not. l3(:)))
                 q(:,i)    = q(:,i) + qyb(:,i)*dphii1(:)
                 hint(:,i) = hi(:)
                 ! adjust derivs
                 y(:)      = one/(qya2(:)-qyb(:,i))
                 qya(:,i) = qya(:,i)*qya2(:)*y(:)
                 qyb(:,i) = -qyb2(:)*qyb(:,i)*y(:)
              endwhere
           end if
        end if
 
        ql(:,i)  = q(:,i)
        qTa(:,i) = qTa(:,i)+(var(:,i)%kE+var(:,i+1)%kE)/thousand/var(:,1)%lambdav/two/dz(:,i)
        qTb(:,i) = qTb(:,i)-(var(:,i)%kE+var(:,i+1)%kE)/thousand/var(:,1)%lambdav/two/dz(:,i)
        qvT(:,i) = (Tsoil(:,i)-Tsoil(:,i+1))*(var(:,i)%kE+var(:,i+1)%kE)/thousand/var(:,1)%lambdav/two/dz(:,i)
        !MC The full description (next two lines) gave problems before ->  third line
        !   Try again original
        !VH Do both formulations come to the same? I found that I could not reproduce
        !   Barnes and Allison semi-analytic solution with original
        !MC This should be re-checked
        ! qvh(:,i) = ((((Tsoil(:,i)+Tzero)/Tzero)**1.88+((Tsoil(:,i+1)+Tzero)/Tzero)**1.88)/two) &
        !      * ((var(:,i)%cvsat+var(:,i+1)%cvsat)/two)*(var(:,i)%phiv-var(:,i+1)%phiv)/dz(:,i)
        qvh(:,i) = (half*(exp(1.88_dp*log((Tsoil(:,i)+Tzero)/Tzero))+exp(1.88_dp*log((Tsoil(:,i+1)+Tzero)/Tzero)))) &
             * (half*(var(:,i)%cvsat+var(:,i+1)%cvsat)) * (var(:,i)%phiv-var(:,i+1)%phiv)/dz(:,i)
        ! qvh(:,i) = ((var(:,i)%Dv+var(:,i+1)%Dv)/two) * &
        !             ((var(:,i)%cvsat+var(:,i+1)%cvsat)/two)*(var(:,i)%rh-var(:,i+1)%rh)/dz(:,i)
        qv(:,i)  = qvh(:,i) + qvT(:,i) ! whole vapour flux has one part from humidity (qvh) and one part from temp diff (qvT)
        q(:,i)   = qv(:,i) + ql(:,i)
 
        where (var(:,i)%isat==0)
           ! qvya(:,i) = var(:,i)%phivS/dz(:,i) *((((Tsoil(:,i)+Tzero)/Tzero)**1.88_dp+ &
           !      ((Tsoil(:,i+1)+Tzero)/Tzero)**1.88_dp)/two) &
           !      * ((var(:,i)%cvsat+var(:,i+1)%cvsat)/two)
           qvya(:,i) = var(:,i)%phivS/dz(:,i) *(half*(exp(1.88_dp*log((Tsoil(:,i)+Tzero)/Tzero)) + &
                exp(1.88_dp*log((Tsoil(:,i+1)+Tzero)/Tzero)))) &
                * (half*(var(:,i)%cvsat+var(:,i+1)%cvsat))
        elsewhere
           qvya(:,i) = zero
        endwhere
 
        where (var(:,i)%isat==0)
           ! qvyb(:,i) = -var(:,i+1)%phivS/dz(:,i) *((((Tsoil(:,i)+Tzero)/Tzero)**1.88_dp+ &
           !      ((Tsoil(:,i+1)+Tzero)/Tzero)**1.88_dp)/two) &
           !      * ((var(:,i)%cvsat+var(:,i+1)%cvsat)/two)
           qvyb(:,i) = -var(:,i+1)%phivS/dz(:,i) *(half*(exp(1.88_dp*log((Tsoil(:,i)+Tzero)/Tzero)) + &
                exp(1.88_dp*log((Tsoil(:,i+1)+Tzero)/Tzero)))) &
                * (half*(var(:,i)%cvsat+var(:,i+1)%cvsat))
        elsewhere
           qvyb(:,i) = zero
        endwhere
 
        qlya(:,i) = qya(:,i)
        qlyb(:,i) = qyb(:,i)
        qya(:,i)  = qya(:,i) + qvya(:,i)
        qyb(:,i)  = qyb(:,i) + qvyb(:,i)
     end do
 
     l1(:) = (iflux(:)==1) .or. (var(:,n)%isat/=0)
     if (any(l1(:) .and. getqn(:))) &  ! get bottom flux if required
          call flux(parin(:,n), var(:,n), vbot(:), half*dx(:,n), ztmp1(:), ztmp2(:), ztmp3(:), ztmp4(:), ztmp5(:))
     where (l1(:) .and. getqn(:))
        q(:,n)    = ztmp1(:)
        qya(:,n)  = ztmp2(:)
        qyb(:,n)  = ztmp3(:)
        qTa(:,n)  = ztmp4(:)
        qTb(:,n)  = ztmp5(:)
        qvya(:,n) = zero
        qvyb(:,n) = zero
        qlya(:,n) = qya(:,n)
        qlyb(:,n) = zero
     endwhere
 
     where (l1(:) .and. (.not. getqn(:)))
        qvya(:,n) = zero
        qvyb(:,n) = zero
        qlya(:,n) = qya(:,n)
        qlyb(:,n) = zero
     endwhere
     where (.not. l1(:))
        qvya(:,n) = zero
        qvyb(:,n) = zero
        qlya(:,n) = qya(:,n)
        qlyb(:,n) = zero
     endwhere
     ! otherwise undefined
     ql(:,n)  = q(:,n)
     qv(:,n)  = zero
     qvh(:,n) = zero
     qvT(:,n) = zero
 
     do i=1, n-1
        where (var(:,i)%Dv == zero .or. var(:,i+1)%Dv == zero)
           q(:,i)    = q(:,i) - qv(:,i)
           qya(:,i)  = qya(:,i) - qvya(:,i)
           qyb(:,i)  = qyb(:,i) - qvyb(:,i)
           qv(:,i)   = zero
           !qTa(:,i)  = zero
           !qTb(:,i)  = zero
           qvya(:,i) = zero
           qvyb(:,i) = zero
        endwhere
     enddo
 
     i_q(:,1:n+1)    = q(:,0:n)
     i_qya(:,1:n+1)  = qya(:,0:n)
     i_qyb(:,1:n+1)  = qyb(:,0:n)
     i_qTa(:,1:n+1)  = qTa(:,0:n)
     i_qTb(:,1:n+1)  = qTb(:,0:n)
     i_ql(:,1:n+1)   = ql(:,0:n)
     i_qlya(:,1:n+1) = qlya(:,0:n)
     i_qlyb(:,1:n+1) = qlyb(:,0:n)
     i_qv(:,1:n+1)   = qv(:,0:n)
     i_qvT(:,1:n+1)  = qvT(:,0:n)
     i_qvh(:,1:n+1)  = qvh(:,0:n)
     i_qvya(:,1:n+1) = qvya(:,0:n)
     i_qvyb(:,1:n+1) = qvyb(:,0:n)
 
    END SUBROUTINE getfluxes_vp_2d

    SUBROUTINE getheatfluxes_1d(n, dx, &
        qh, qhya, qhyb, qhTa, qhTb, &
        var, T, &
        q, qya, qyb, qTa, qTb, &
        qadv, qadvya, qadvyb, qadvTa, qadvTb, &
        advection)
     ! modified 25/05/10 to include contribution to heat flux from liquid water flux in the presence of ice
     IMPLICIT NONE
 
     INTEGER(i4),               INTENT(IN)    :: n
     REAL(dp),  DIMENSION(1:n), INTENT(IN)    :: dx
     REAL(dp),  DIMENSION(0:n), INTENT(INOUT) :: qh, q, qadv
     REAL(dp),  DIMENSION(0:n), INTENT(INOUT) :: qhya, qya, qadvya
     REAL(dp),  DIMENSION(0:n), INTENT(INOUT) :: qhyb, qyb, qadvyb
     REAL(dp),  DIMENSION(0:n), INTENT(INOUT) :: qhTa, qTa, qadvTa
     REAL(dp),  DIMENSION(0:n), INTENT(INOUT) :: qhTb, qTb, qadvTb
     TYPE(vars), DIMENSION(1:n), INTENT(IN)    :: var
     REAL(dp),  DIMENSION(1:n), INTENT(IN)    :: T
     INTEGER(i4),               INTENT(IN)    :: advection
     ! Gets heat fluxes qh and partial derivs qhya, qhyb wrt T and S (if unsat) or phi (if sat).
 
     INTEGER(i4)          :: i
     REAL(dp)             :: rdz, keff , w
     REAL(dp), DIMENSION(1:n-1) :: dz
     REAL(dp) :: dTqwdTa, dTqwdTb, Tqw
     
     !  print *, "getheatfluxes_1d is used"
     dz(:) = half*(dx(1:n-1)+dx(2:n))
 
     do i=1, n-1
        rdz = one/dz(i)
        keff = 2_dp*(var(i)%kth*var(i+1)%kth)/(var(i)%kth*dx(i)+var(i+1)%kth*dx(i+1))
 
        ! qh(i) = keff*(T(i)-T(i+1)) +(var(i)%phiv-var(i+1)%phiv)*var(i)%lambdav*thousand*rdz &
        !      * ((((T(i)+Tzero) /Tzero)**1.88_dp+((T(i+1)+Tzero) /Tzero)**1.88_dp)/two) &
        !      *((var(i)%cvsat+var(i+1)%cvsat)/two)
        qh(i) = keff*(T(i)-T(i+1)) + (var(i)%phiv-var(i+1)%phiv) * var(i)%lambdav*thousand*rdz &
             * (half*(exp(1.88_dp*log((T(i)+Tzero) /Tzero))+exp(1.88_dp*log((T(i+1)+Tzero) /Tzero)))) &
             *(half*(var(i)%cvsat+var(i+1)%cvsat))
 
 
        if (var(i)%isat==0) then
           ! qhya(i) = rdz*var(i)%lambdav*thousand*var(i)%phivS*((((T(i)+Tzero) /Tzero)**1.88_dp+ &
           !      ((T(i+1)+Tzero) /Tzero)**1.88_dp)/two) &
           !      * ((var(i)%cvsat+var(i+1)%cvsat)/two)
           qhya(i) = rdz*var(i)%lambdav*thousand*var(i)%phivS*(half*(exp(1.88_dp*log((T(i)+Tzero) /Tzero)) + &
                exp(1.88_dp*log((T(i+1)+Tzero) /Tzero)))) &
                * (half*(var(i)%cvsat+var(i+1)%cvsat))
        else
           qhya(i) = zero
        end if
        if (var(i+1)%isat==0) then
           ! qhyb(i) = -rdz*var(i)%lambdav*thousand*var(i+1)%phivS*((((T(i)+Tzero) /Tzero)**1.88_dp+ &
           !      ((T(i+1)+Tzero) /Tzero)**1.88_dp)/two) &
           !      * ((var(i)%cvsat+var(i+1)%cvsat)/two)
           qhyb(i) = -rdz*var(i)%lambdav*thousand*var(i+1)%phivS*(half*(exp(1.88_dp*log((T(i)+Tzero) /Tzero)) + &
                exp(1.88_dp*log((T(i+1)+Tzero) /Tzero)))) &
                * (half*(var(i)%cvsat+var(i+1)%cvsat))
        else
           qhyb(i) = zero
        end if
        qhTa(i) = keff
        qhTb(i) = -keff
 
        ! add advective terms
        if (advection==1) then
          !!$                   if (q(i) > zero) then
          !!$                       w = (var(i)%kth/dx(i))/(var(i)%kth/dx(i)+var(i+1)%kth/dx(i+1))
          !!$                    else
          !!$                       w = (var(i)%kth/dx(i))/(var(i)%kth/dx(i)+var(i+1)%kth/dx(i+1))
          !!$                    endif
          !!$                    qadv(i) = rhow*cswat*q(i)*(w*(T(i)+zero)+(one-w)*(T(i+1)+zero))
          !!$                    qadvya(i) =  rhow*cswat*qya(i)*(w*(T(i)+zero)+(one-w)*(T(i+1)+zero))
          !!$                    qadvyb(i) =  rhow*cswat*qyb(i)*(w*(T(i)+zero)+(one-w)*(T(i+1)+zero))
          !!$                    qadvTa(i) =  rhow*cswat*q(i)*w
          !!$                    qadvTb(i) =  rhow*cswat*q(i)*(one-w)
           Tqw  = merge(T(i), T(i+1), q(i)>zero) +zero
 
           dTqwdTa = merge(one, zero, (q(i)>zero))
 
           dTqwdTb = merge(zero,one, q(i)>zero)
           qadv(i) = rhow*cswat*q(i)*Tqw
           qadvya(i) =  rhow*cswat*qya(i)*Tqw
           qadvyb(i) =  rhow*cswat*qyb(i)*Tqw
 
           qadvTa(i) =  rhow*cswat*q(i)*dTqwdTa + rhow*cswat*Tqw*qTa(i)
           qadvTb(i) =  rhow*cswat*q(i)*dTqwdTb  +  rhow*cswat*Tqw*qTb(i)
 
           qh(i) = qh(i) + qadv(i)
           qhya(i) = qhya(i) + qadvya(i)
           qhyb(i) = qhyb(i) + qadvyb(i)
           qhTa(i) = qhTa(i) + qadvTa(i)
           qhTb(i) = qhTb(i) + qadvTb(i)
        endif
     enddo
 
     qh(n)   = zero
     qhya(n) = zero
     qhyb(n) = zero
     qhTa(n) = zero
     qhTb(n) = zero
 
    END SUBROUTINE getheatfluxes_1d

    SUBROUTINE getheatfluxes_2d(dx, i_qh, i_qhya, i_qhyb, i_qhTa, i_qhTb, var, T, &
        i_q,i_qya,i_qyb,i_qTa,i_qTb, &
        i_qadv,i_qadvya, i_qadvyb, i_qadvTa, i_qadvTb, advection)
     ! modified 25/05/10 to include contribution to heat flux from liquid water flux in the presence of ice
     IMPLICIT NONE
 
     REAL(dp),    DIMENSION(:,:), INTENT(IN)    :: dx      ! :,1:n
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qh    ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qhya  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qhyb  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qhTa  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qhTb  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_q    ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qya  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qyb  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qTa  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qTb  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qadv    ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qadvya  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qadvyb  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qadvTa  ! :,0:n => :,1:n+1
     REAL(dp),    DIMENSION(:,:), INTENT(INOUT) :: i_qadvTb  ! :,0:n => :,1:n+1
     TYPE(vars),   DIMENSION(:,:), INTENT(IN)    :: var
     REAL(dp),    DIMENSION(:,:), INTENT(IN)    :: T       ! :,1:n
     INTEGER(i4),   INTENT(IN)    :: advection
     ! Gets heat fluxes qh and partial derivs qhya, qhyb wrt T and S (if unsat) or phi (if sat).
 
     INTEGER(i4)          :: i, n
     REAL(dp),  DIMENSION(1:size(dx,1))                :: rdz
     REAL(dp),  DIMENSION(1:size(dx,1))                :: keff
     REAL(dp),  DIMENSION(1:size(dx,1),1:size(dx,2)-1) :: dz
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qh   ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qhya ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qhyb ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qhTa ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qhTb ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: q   ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qya ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qyb ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qTa ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qTb ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qadv   ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qadvya ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qadvyb ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qadvTa ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1),0:size(dx,2))   :: qadvTb ! :,0:n
     REAL(dp),  DIMENSION(1:size(dx,1)) :: dTqwdTa, dTqwdTb, Tqw
    
     print *, "getheatfluxes_2d is used"
     n = size(dx,2)
     dz(:,1:n-1) = half*(dx(:,1:n-1)+dx(:,2:n))
     qh(:,0:n)   = i_qh(:,1:n+1)
     qhya(:,0:n) = i_qhya(:,1:n+1)
     qhyb(:,0:n) = i_qhyb(:,1:n+1)
     qhTa(:,0:n) = i_qhTa(:,1:n+1)
     qhTb(:,0:n) = i_qhTb(:,1:n+1)
 
     q(:,0:n)   = i_q(:,1:n+1)
     qya(:,0:n) = i_qya(:,1:n+1)
     qyb(:,0:n) = i_qyb(:,1:n+1)
     qTa(:,0:n) = i_qTa(:,1:n+1)
     qTb(:,0:n) = i_qTb(:,1:n+1)
 
     qadv(:,0:n)   = i_qadv(:,1:n+1)
     qadvya(:,0:n) = i_qadvya(:,1:n+1)
     qadvyb(:,0:n) = i_qadvyb(:,1:n+1)
     qadvTa(:,0:n) = i_qadvTa(:,1:n+1)
     qadvTb(:,0:n) = i_qadvTb(:,1:n+1)
 
     do i=1, n-1
        rdz =  one/dz(:,i)
        keff = 2_dp*(var(:,i)%kth*var(:,i+1)%kth)/(var(:,i)%kth*dx(:,i)+var(:,i+1)%kth*dx(:,i+1))
        ! qh(:,i) = keff*(T(:,i)-T(:,i+1)) &
        !      + (var(:,i)%phiv-var(:,i+1)%phiv)*var(:,i)%lambdav*thousand*rdz(:) &
        !      * ((((T(:,i)+Tzero) /Tzero)**1.88_dp+((T(:,i+1)+Tzero) /Tzero)**1.88_dp)/two) &
        !      *((var(:,i)%cvsat+var(:,i+1)%cvsat)/two)
        qh(:,i) = keff*(T(:,i)-T(:,i+1)) &
             + (var(:,i)%phiv-var(:,i+1)%phiv)*var(:,i)%lambdav*thousand*rdz(:) &
             * (half*exp(1.88_dp*log(((T(:,i)+Tzero) /Tzero)) + exp(1.88_dp*log((T(:,i+1)+Tzero) /Tzero)))) &
             *(half*(var(:,i)%cvsat+var(:,i+1)%cvsat))
        where (var(:,i)%isat == 0)
           ! qhya(:,i) = rdz(:)*var(:,i)%lambdav*thousand*var(:,i)%phivS*((((T(:,i)+Tzero) /Tzero)**1.88_dp+ &
           !      ((T(:,i+1)+Tzero) /Tzero)**1.88_dp)/two) &
           !      * ((var(:,i)%cvsat+var(:,i+1)%cvsat)/two)
           qhya(:,i) = rdz(:)*var(:,i)%lambdav*thousand*var(:,i)%phivS*(half*(exp(1.88_dp*log((T(:,i)+Tzero) /Tzero)) + &
                exp(1.88_dp*log((T(:,i+1)+Tzero) /Tzero)))) &
                * (half*(var(:,i)%cvsat+var(:,i+1)%cvsat))
        elsewhere
           qhya(:,i) = zero
        endwhere
        where (var(:,i+1)%isat == 0)
           ! qhyb(:,i) = -rdz(:)*var(:,i)%lambdav*thousand*var(:,i+1)%phivS*((((T(:,i)+Tzero) /Tzero)**1.88_dp+ &
           !      ((T(:,i+1)+Tzero) /Tzero)**1.88_dp)/two) &
           !      * ((var(:,i)%cvsat+var(:,i+1)%cvsat)/two)
           qhyb(:,i) = -rdz(:)*var(:,i)%lambdav*thousand*var(:,i+1)%phivS*(half*exp(1.88_dp*log(((T(:,i)+Tzero) /Tzero)) + &
                exp(1.88_dp*log((T(:,i+1)+Tzero) /Tzero)))) &
                * (half*(var(:,i)%cvsat+var(:,i+1)%cvsat))
        elsewhere
           qhyb(:,i) = zero
        endwhere
        qhTa(:,i) = keff
        qhTb(:,i) = -keff
 
        ! add advective terms
        if (advection==1) then
 
           Tqw  = merge(T(:,i), T(:,i+1), q(:,i)>zero) +zero
           dTqwdTa = merge(one, zero, q(:,i)>zero)
           dTqwdTb = merge(zero,one, q(:,i)>zero)
 
           qadv(:,i) = rhow*cswat*q(:,i)*Tqw
           qadvya(:,i) =  rhow*cswat*qya(:,i)*Tqw
           qadvyb(:,i) =  rhow*cswat*qyb(:,i)*Tqw
 
           qadvTa(:,i) =  rhow*cswat*q(:,i)*dTqwdTa + rhow*cswat*Tqw*qTa(:,i)
           qadvTb(:,i) =  rhow*cswat*q(:,i)*dTqwdTb  +  rhow*cswat*Tqw*qTb(:,i)
 
           qh(:,i) = qh(:,i) + qadv(:,i)
           qhya(:,i) = qhya(:,i) + qadvya(:,i)
           qhyb(:,i) = qhyb(:,i) + qadvyb(:,i)
           qhTa(:,i) = qhTa(:,i) + qadvTa(:,i)
           qhTb(:,i) = qhTb(:,i) + qadvTb(:,i)
        endif
     enddo
 
     qh(:,n)   = zero
     qhya(:,n) = zero
     qhyb(:,n) = zero
     qhTa(:,n) = zero
     qhTb(:,n) = zero
 
     i_qh(:,1:n+1)   = qh(:,0:n)
     i_qhya(:,1:n+1) = qhya(:,0:n)
     i_qhyb(:,1:n+1) = qhyb(:,0:n)
     i_qhTa(:,1:n+1) = qhTa(:,0:n)
     i_qhTb(:,1:n+1) = qhTb(:,0:n)
 
     if (advection==1) then
        i_qadv(:,1:n+1)   = qadv(:,0:n)
        i_qadvya(:,1:n+1) = qadvya(:,0:n)
        i_qadvyb(:,1:n+1) = qadvyb(:,0:n)
        i_qadvTa(:,1:n+1) = qadvTa(:,0:n)
        i_qadvTb(:,1:n+1) = qadvTb(:,0:n)
     endif
 
    END SUBROUTINE getheatfluxes_2d

    SUBROUTINE tri_1d(ns, n, aa, bb, cc, dd, dy)
        ! Solves tridiag set of linear eqns. Coeff arrays aa and cc left intact.
        ! Definitions of arguments:
        ! ns      - start index for eqns.
        ! n       - end index.
        ! aa(0:n) - coeffs below diagonal; ns+1:n used.
        ! bb(0:n) - coeffs on diagonal; ns:n used.
        ! cc(0:n) - coeffs above diagonal; ns:n-1 used.
        ! dd(0:n) - rhs coeffs; ns:n used.
        ! ee(0:n) - work space.
        ! dy(0:n) - solution in ns:n.
        IMPLICIT NONE
    
        INTEGER(i4),                 INTENT(IN)    :: ns
        INTEGER(i4),                 INTENT(IN)    :: n
        REAL(dp),    DIMENSION(0:n), INTENT(IN)    :: aa
        REAL(dp),    DIMENSION(0:n), INTENT(IN)    :: cc
        REAL(dp),    DIMENSION(0:n), INTENT(IN)    :: dd
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT) :: bb
        REAL(dp),    DIMENSION(0:n), INTENT(INOUT) :: dy
        
        REAL(dp),   DIMENSION(0:n) :: ee
        INTEGER(i4)                :: i
      
        !>> constraint
        if (bb(0) == 0.0_dp) bb(0) = bb(1)
        if (bb(n) == 0.0_dp) bb(n) = bb(n-1)

        dy(ns) = dd(ns) ! decomposition and forward substitution
        do i=ns, n-1
           if(bb(i)==zero) bb(i) = bb(i+1)
           ee(i)   = cc(i)/bb(i)
           dy(i)   = dy(i)/bb(i)
           bb(i+1) = bb(i+1)-aa(i+1)*ee(i)
           dy(i+1) = dd(i+1)-aa(i+1)*dy(i)
        end do
        
         !>> constraint
        if (bb(n) == 0.0_dp) bb(n) = bb(n-1)
        if (bb(0) == 0.0_dp) bb(0) = bb(1)

        dy(n) = dy(n)/bb(n) ! back substitution
        do i=n-1, ns, -1
           dy(i) = dy(i)-ee(i)*dy(i+1)
        end do
    
    END SUBROUTINE tri_1d
    
    SUBROUTINE tri_2d(mp, ns, n, aa, bb, cc, dd, dy)
    
        IMPLICIT NONE
    
        INTEGER(i4),                      INTENT(IN)    :: mp
        INTEGER(i4),                      INTENT(IN)    :: ns
        INTEGER(i4),                      INTENT(IN)    :: n
        REAL(dp),    DIMENSION(1:mp,0:n), INTENT(IN)    :: aa
        REAL(dp),    DIMENSION(1:mp,0:n), INTENT(IN)    :: cc
        REAL(dp),    DIMENSION(1:mp,0:n), INTENT(IN)    :: dd
        REAL(dp),    DIMENSION(1:mp,0:n), INTENT(INOUT) :: bb
        REAL(dp),    DIMENSION(1:mp,0:n), INTENT(INOUT) :: dy
        ! Solves tridiag set of linear eqns. Coeff arrays aa and cc left intact.
        ! Definitions of arguments:
        ! mp      - number of land patches
        ! ns      - start index for eqns.
        ! n       - end index.
        ! aa(0:n) - coeffs below diagonal; ns+1:n used.
        ! bb(0:n) - coeffs on diagonal; ns:n used.
        ! cc(0:n) - coeffs above diagonal; ns:n-1 used.
        ! dd(0:n) - rhs coeffs; ns:n used.
        ! ee(0:n) - work space.
        ! dy(0:n) - solution in ns:n.
        REAL(dp),   DIMENSION(1:mp,0:n) :: ee
        INTEGER(i4)                     :: i
    
        dy(:,ns) = dd(:,ns) ! decomposition and forward substitution
        do i=ns, n-1
           ee(:,i)   = cc(:,i)/bb(:,i)
           dy(:,i)   = dy(:,i)/bb(:,i)
           bb(:,i+1) = bb(:,i+1)-aa(:,i+1)*ee(:,i)
           dy(:,i+1) = dd(:,i+1)-aa(:,i+1)*dy(:,i)
        end do
    
        dy(:,n) = dy(:,n)/bb(:,n) ! back substitution
        do i=n-1, ns, -1
           dy(:,i) = dy(:,i)-ee(:,i)*dy(:,i+1)
        end do
    
    END SUBROUTINE tri_2d

    SUBROUTINE massman_sparse_1d(aa, aah, bb, bbh, cc, cch, dd, ddh, ee, eeh, ff, ffh, gg, ggh, dy, dT, condition)
    
        IMPLICIT NONE
    
        ! in/out
        REAL(dp), DIMENSION(:), INTENT(IN)  :: aa, aah, bb, bbh, ee, eeh, ff, ffh
        REAL(dp), DIMENSION(:), INTENT(IN)  :: cc, cch, dd, ddh, gg, ggh
        REAL(dp), DIMENSION(:), INTENT(OUT) :: dy, dT
        INTEGER(i4),  OPTIONAL, INTENT(IN)  :: condition
        ! local
        INTEGER(i4)                         :: n, n2
        REAL(dp), DIMENSION(size(cc),2,2)   :: A, B, C
        REAL(dp), DIMENSION(size(cc),2)     :: d, x
        ! for conditioning
        REAL(dp), DIMENSION(2*size(cc))       :: lST, cST
        REAL(dp), DIMENSION(size(cc))         :: lS, lT, cS, cT
        REAL(dp), DIMENSION(2*size(cc)*2*size(cc)) :: allvec
        REAL(dp), DIMENSION(2*size(cc),2*size(cc)) :: allmat
        REAL(dp)    :: eps
        INTEGER(i4) :: docond ! 0: no conditioning, 1: columns, 2: lines, 3: both
        ! CHARACTER(LEN=20) :: form1
        ! integer :: i, nn
        !
        ! check input sizes
        if (.not. all((/size(aa)+1,size(bb)+1,size(dd),size(ee)+1,size(ff)+1,size(gg)/) == size(cc))) then
           write(*,*) 'massman_sparse_1d error1: unequal humidity coeffs.'
           stop 2
        end if
        if (.not. all((/size(aah)+1,size(bbh)+1,size(ddh),size(eeh)+1,size(ffh)+1,size(ggh)/) == size(cch))) then
           write(*,*) 'massman_sparse_1d error2: unequal temperature coeffs.'
           stop 2
        end if
        if (size(cc) /= size(cch)) then
           write(*,*) 'massman_sparse_1d error3: unequal temperature and humidity coeffs.'
           stop 2
        end if
        n = size(cc)
        if (present(condition)) then
           docond = condition
        else
           docond = 0
        endif
        !
        ! Overall matrix
        if (docond >= 1 .and. docond <= 3) then
           eps = epsilon(one)
           n2  = 2*n*2*n
           allvec(:) = zero
           allvec(1:n2:4*n+2)         = cc(1:n)
           allvec(2:n2:4*n+2)         = dd(1:n)
           allvec(3:n2-4*n:4*n+2)     = ee(1:n-1)
           allvec(4:n2-4*n:4*n+2)     = ff(1:n-1)
           allvec(2*n+1:n2:4*n+2)     = cch(1:n)
           allvec(2*n+2:n2:4*n+2)     = ddh(1:n)
           allvec(2*n+3:n2-4*n:4*n+2) = eeh(1:n-1)
           allvec(2*n+4:n2-4*n:4*n+2) = ffh(1:n-1)
           allvec(4*n+1:n2:4*n+2)     = aa(1:n-1)
           allvec(4*n+2:n2:4*n+2)     = bb(1:n-1)
           allvec(6*n+1:n2:4*n+2)     = aah(1:n-1)
           allvec(6*n+1:n2:4*n+2)     = bbh(1:n-1)
           allmat = reshape(allvec,shape=(/2*n,2*n/),order=(/2,1/))
        endif
        ! Get conditioning numbers
        select case (docond)
        case (1)
           cST = maxval(abs(allmat),1)
           where (cST < eps) cST = one
           cST     = one / cST
           cS(1:n) = cST(1:2*n-1:2)
           cT(1:n) = cST(2:2*n:2)
           lS(1:n) = one
           lT(1:n) = one
        case (2)
           lST = maxval(abs(allmat),2)
           where (lST < eps) lST = one
           lST     = one / lST
           lS(1:n) = lST(1:2*n-1:2)
           lT(1:n) = lST(2:2*n:2)
           cS(1:n) = one
           cT(1:n) = one
        case (3)
           lST = maxval(abs(allmat),2)
           where (lST < eps) lST = one
           lST     = one / lST
           lS(1:n) = lST(1:2*n-1:2)
           lT(1:n) = lST(2:2*n:2)
           allmat  = spread(lST,2,2*n)*allmat
           cST = maxval(abs(allmat),1)
           where (cST < eps) cST = one
           cST     = one / cST
           cS(1:n) = cST(1:2*n-1:2)
           cT(1:n) = cST(2:2*n:2)
        case default
           cS(1:n) = one
           cT(1:n) = one
           lS(1:n) = one
           lT(1:n) = one
        end select
        !
        ! fill matrices of generic thomas algorithm
        A(1,1:2,1:2) = zero
        A(2:n,1,1)   = aa(1:n-1)  * lS(2:n)   * cS(1:n-1)
        A(2:n,1,2)   = bb(1:n-1)  * lS(2:n)   * cT(1:n-1)
        A(2:n,2,1)   = aah(1:n-1) * lT(2:n)   * cS(1:n-1)
        A(2:n,2,2)   = bbh(1:n-1) * lT(2:n)   * cT(1:n-1)
        B(1:n,1,1)   = cc(1:n)    * lS(1:n)   * cS(1:n)
        B(1:n,1,2)   = dd(1:n)    * lS(1:n)   * cT(1:n)
        B(1:n,2,1)   = cch(1:n)   * lT(1:n)   * cS(1:n)
        B(1:n,2,2)   = ddh(1:n)   * lT(1:n)   * cT(1:n)
        C(1:n-1,1,1) = ee(1:n-1)  * lS(1:n-1) * cS(2:n)
        C(1:n-1,1,2) = ff(1:n-1)  * lS(1:n-1) * cT(2:n)
        C(1:n-1,2,1) = eeh(1:n-1) * lT(1:n-1) * cS(2:n)
        C(1:n-1,2,2) = ffh(1:n-1) * lT(1:n-1) * cT(2:n)
        C(n,1:2,1:2) = zero
        d(1:n,1)     = gg(1:n)    * lS(1:n)
        d(1:n,2)     = ggh(1:n)   * lT(1:n)
        !
        ! Call Generic Thomas algorithm
        call generic_thomas(n,A,B,C,d,x)
        
        dy(1:n) = x(1:n,1) * cS(1:n)
        dT(1:n) = x(1:n,2) * cT(1:n)
        !
    END SUBROUTINE massman_sparse_1d
    
    SUBROUTINE massman_sparse_2d(aa, aah, bb, bbh, cc, cch, dd, ddh, ee, eeh, ff, ffh, gg, ggh, dy, dT, condition)
    
        IMPLICIT NONE
    
        ! in/out
        REAL(dp), DIMENSION(:,:), INTENT(IN)  :: aa, aah, bb, bbh, ee, eeh, ff, ffh
        REAL(dp), DIMENSION(:,:), INTENT(IN)  :: cc, cch, dd, ddh, gg, ggh
        REAL(dp), DIMENSION(:,:), INTENT(OUT) :: dy, dT
        INTEGER(i4),    OPTIONAL, INTENT(IN)  :: condition
        ! local
        INTEGER(i4)                                        :: n, mp
        INTEGER(i4)                                        :: n2
        REAL(dp), DIMENSION(1:size(cc,1),1:size(cc,2),2,2) :: A, B, C
        REAL(dp), DIMENSION(1:size(cc,1),1:size(cc,2),2)   :: d, x
        ! for conditioning
        REAL(dp), DIMENSION(1:size(cc,1),2*size(cc,2))     :: lST, cST
        REAL(dp), DIMENSION(1:size(cc,1),size(cc,2))       :: lS, lT, cS, cT
        REAL(dp), DIMENSION(1:size(cc,1),2*size(cc,2)*2*size(cc,2)) :: allvec
        REAL(dp), DIMENSION(1:size(cc,1),2*size(cc,2),2*size(cc,2)) :: allmat
        REAL(dp)    :: eps
        INTEGER(i4) :: docond ! 0: no conditioning, 1: columns, 2: lines, 3: both
        ! CHARACTER(LEN=20) :: form1
        ! integer :: i, k, nn
        !
        ! check input sizes
        if (.not. all((/size(aa,1)+1,size(bb,1)+1,size(dd,1),size(ee,1)+1,size(ff,1)+1,size(gg,1)/) == size(cc,1))) then
           write(*,*) 'massman_sparse_2d error1: unequal humidity coeffs (1st dim).'
           stop 2
        end if
        if (.not. all((/size(aah,1)+1,size(bbh,1)+1,size(ddh,1),size(eeh,1)+1,size(ffh,1)+1,size(ggh,1)/) == size(cch,1))) then
           write(*,*) 'massman_sparse_2d error2: unequal temperature coeffs (1st dim).'
           stop 2
        end if
        if (size(cc,1) /= size(cch,1)) then
           write(*,*) 'massman_sparse_2d error3: unequal temperature and humidity coeffs (1st dim).'
           stop 2
        end if
        if (.not. all((/size(aa,2)+1,size(bb,2)+1,size(dd,2),size(ee,2)+1,size(ff,2)+1,size(gg,2)/) == size(cc,2))) then
           write(*,*) 'massman_sparse_2d error4: unequal humidity coeffs (2nd dim).'
           stop 2
        end if
        if (.not. all((/size(aah,2)+1,size(bbh,2)+1,size(ddh,2),size(eeh,2)+1,size(ffh,2)+1,size(ggh,2)/) == size(cch,2))) then
           write(*,*) 'massman_sparse_2d error5: unequal temperature coeffs (2nd dim).'
           stop 2
        end if
        if (size(cc,2) /= size(cch,2)) then
           write(*,*) 'massman_sparse_2d error6: unequal temperature and humidity coeffs (2nd dim).'
           stop 2
        end if
    
        mp   = size(cc,1)
        n    = size(cc,2)
        if (present(condition)) then
           docond = condition
        else
           docond = 0
        endif
        !
        ! Overall matrix
        if (docond >= 1 .and. docond <= 3) then
           eps = epsilon(one)
           n2  = 2*n*2*n
           allvec(1:mp,:) = zero
           allvec(1:mp,1:n2:4*n+2)         = cc(1:mp,1:n)
           allvec(1:mp,2:n2:4*n+2)         = dd(1:mp,1:n)
           allvec(1:mp,3:n2-4*n:4*n+2)     = ee(1:mp,1:n-1)
           allvec(1:mp,4:n2-4*n:4*n+2)     = ff(1:mp,1:n-1)
           allvec(1:mp,2*n+1:n2:4*n+2)     = cch(1:mp,1:n)
           allvec(1:mp,2*n+2:n2:4*n+2)     = ddh(1:mp,1:n)
           allvec(1:mp,2*n+3:n2-4*n:4*n+2) = eeh(1:mp,1:n-1)
           allvec(1:mp,2*n+4:n2-4*n:4*n+2) = ffh(1:mp,1:n-1)
           allvec(1:mp,4*n+1:n2:4*n+2)     = aa(1:mp,1:n-1)
           allvec(1:mp,4*n+2:n2:4*n+2)     = bb(1:mp,1:n-1)
           allvec(1:mp,6*n+1:n2:4*n+2)     = aah(1:mp,1:n-1)
           allvec(1:mp,6*n+1:n2:4*n+2)     = bbh(1:mp,1:n-1)
           allmat = reshape(allvec,shape=(/mp,2*n,2*n/),order=(/1,3,2/))
        endif
        ! Get conditioning numbers
        select case (docond)
        case (1)
           cST = maxval(abs(allmat),2)
           where (cST < eps) cST = one
           cST     = one / cST
           cS(1:mp,1:n) = cST(1:mp,1:2*n-1:2)
           cT(1:mp,1:n) = cST(1:mp,2:2*n:2)
           lS(1:mp,1:n) = one
           lT(1:mp,1:n) = one
        case (2)
           lST = maxval(abs(allmat),3)
           where (lST < eps) lST = one
           lST  = one / lST
           lS(1:mp,1:n) = lST(1:mp,1:2*n-1:2)
           lT(1:mp,1:n) = lST(1:mp,2:2*n:2)
           cS(1:mp,1:n) = one
           cT(1:mp,1:n) = one
        case (3)
           lST = maxval(abs(allmat),3)
           where (lST < eps) lST = one
           lST  = one / lST
           lS(1:mp,1:n) = lST(1:mp,1:2*n-1:2)
           lT(1:mp,1:n) = lST(1:mp,2:2*n:2)
           allmat  = spread(lST,2,2*n)*allmat
           cST = maxval(abs(allmat),2)
           where (cST < eps) cST = one
           cST = one / cST
           cS(1:mp,1:n) = cST(1:mp,1:2*n-1:2)
           cT(1:mp,1:n) = cST(1:mp,2:2*n:2)
        case default
           cS(1:mp,1:n) = one
           cT(1:mp,1:n) = one
           lS(1:mp,1:n) = one
           lT(1:mp,1:n) = one
        end select
        !
        ! fill matrices of generic thomas algorithm
        A(1:mp,1,1:2,1:2) = zero
        A(1:mp,2:n,1,1)   = aa(1:mp,1:n-1)  * lS(1:mp,2:n)   * cS(1:mp,1:n-1)
        A(1:mp,2:n,1,2)   = bb(1:mp,1:n-1)  * lS(1:mp,2:n)   * cT(1:mp,1:n-1)
        A(1:mp,2:n,2,1)   = aah(1:mp,1:n-1) * lT(1:mp,2:n)   * cS(1:mp,1:n-1)
        A(1:mp,2:n,2,2)   = bbh(1:mp,1:n-1) * lT(1:mp,2:n)   * cT(1:mp,1:n-1)
        B(1:mp,1:n,1,1)   = cc(1:mp,1:n)    * lS(1:mp,1:n)   * cS(1:mp,1:n)
        B(1:mp,1:n,1,2)   = dd(1:mp,1:n)    * lS(1:mp,1:n)   * cT(1:mp,1:n)
        B(1:mp,1:n,2,1)   = cch(1:mp,1:n)   * lT(1:mp,1:n)   * cS(1:mp,1:n)
        B(1:mp,1:n,2,2)   = ddh(1:mp,1:n)   * lT(1:mp,1:n)   * cT(1:mp,1:n)
        C(1:mp,1:n-1,1,1) = ee(1:mp,1:n-1)  * lS(1:mp,1:n-1) * cS(1:mp,2:n)
        C(1:mp,1:n-1,1,2) = ff(1:mp,1:n-1)  * lS(1:mp,1:n-1) * cT(1:mp,2:n)
        C(1:mp,1:n-1,2,1) = eeh(1:mp,1:n-1) * lT(1:mp,1:n-1) * cS(1:mp,2:n)
        C(1:mp,1:n-1,2,2) = ffh(1:mp,1:n-1) * lT(1:mp,1:n-1) * cT(1:mp,2:n)
        C(1:mp,n,1:2,1:2) = zero
        d(1:mp,1:n,1)     = gg(1:mp,1:n)    * lS(1:mp,1:n)
        d(1:mp,1:n,2)     = ggh(1:mp,1:n)   * lT(1:mp,1:n)
        !
        ! Call Generic Thomas algorithm
        call generic_thomas(mp,n,A,B,C,d,x)
        dy(1:mp,1:n) = x(1:mp,1:n,1) * cS(1:mp,1:n)
        dT(1:mp,1:n) = x(1:mp,1:n,2) * cT(1:mp,1:n)
        !
    END SUBROUTINE massman_sparse_2d

    SUBROUTINE generic_thomas_1d(n,A,B,C,r,u)

    
        IMPLICIT NONE
    
        ! in/out
        INTEGER(i4),                      INTENT(IN)  :: n
        REAL(dp), DIMENSION(1:n,1:2,1:2), INTENT(IN)  :: A, B, C
        REAL(dp), DIMENSION(1:n,1:2),     INTENT(IN)  :: r
        REAL(dp), DIMENSION(1:n,1:2),     INTENT(OUT) :: u
        ! local
        REAL(dp), DIMENSION(1:n,1:2,1:2) :: G
        REAL(dp), DIMENSION(1:2,1:2)     :: bet
        REAL(dp), DIMENSION(1:2)         :: d
        REAL(dp)                         :: detbet, detbet1
        INTEGER(i4)                      :: j
    
        ! j=1
        bet(1:2,1:2) = B(1,1:2,1:2)
        detbet       = bet(1,1)*bet(2,2) - bet(1,2)*bet(2,1)
        if (abs(detbet) < epsilon(detbet)) then
           write(*,*) 'generic_thomas_1d error1: det = 0'
           stop 'program terminated by generic_thomas_1d'
        endif
        detbet1 = one/detbet
        d(1:2)  = r(1,1:2)
        u(1,1)  = (bet(2,2)*d(1) - bet(1,2)*d(2))*detbet1
        u(1,2)  = (bet(1,1)*d(2) - bet(2,1)*d(1))*detbet1
        ! j=2, n
        do j=2, n
           d(1:2)       = C(j-1,1:2,1)
           G(j,1,1)     = (bet(2,2)*d(1) - bet(1,2)*d(2))*detbet1
           G(j,2,1)     = (bet(1,1)*d(2) - bet(2,1)*d(1))*detbet1
           d(1:2)       = C(j-1,1:2,2)
           G(j,1,2)     = (bet(2,2)*d(1) - bet(1,2)*d(2))*detbet1
           G(j,2,2)     = (bet(1,1)*d(2) - bet(2,1)*d(1))*detbet1
           bet(1:2,1:2) = B(j,1:2,1:2) - matmul(A(j,1:2,1:2),G(j,1:2,1:2))
           detbet       = bet(1,1)*bet(2,2) - bet(1,2)*bet(2,1)
           if (abs(detbet) < epsilon(detbet)) then
              write(*,*) 'generic_thomas_1d error2: det = 0 at j=', j
              stop 'program terminated by generic_thomas_1d'
           endif
           detbet1      = one/detbet
           d(1:2)       = r(j,1:2) - matmul(A(j,1:2,1:2),u(j-1,1:2))
           u(j,1)       = (bet(2,2)*d(1) - bet(1,2)*d(2))*detbet1
           u(j,2)       = (bet(1,1)*d(2) - bet(2,1)*d(1))*detbet1
        end do
        ! back substitution
        do j=n-1, 1, -1
           u(j,1:2) = u(j,1:2) - matmul(G(j+1,1:2,1:2),u(j+1,1:2))
        end do
        !
    END SUBROUTINE generic_thomas_1d
    
    SUBROUTINE generic_thomas_2d(mp, n,A,B,C,r,u)
        
        IMPLICIT NONE
    
        ! in/out
        INTEGER(i4),                           INTENT(IN)  :: mp
        INTEGER(i4),                           INTENT(IN)  :: n
        REAL(dp), DIMENSION(1:mp,1:n,1:2,1:2), INTENT(IN)  :: A, B, C
        REAL(dp), DIMENSION(1:mp,1:n,1:2),     INTENT(IN)  :: r
        REAL(dp), DIMENSION(1:mp,1:n,1:2),     INTENT(OUT) :: u
        ! local
        REAL(dp), DIMENSION(1:mp,1:n,1:2,1:2) :: G
        REAL(dp), DIMENSION(1:mp,1:2,1:2)     :: bet
        REAL(dp), DIMENSION(1:mp,1:2)         :: d
        REAL(dp), DIMENSION(1:mp)             :: detbet, detbet1
        INTEGER(i4)                           :: j
        REAL(dp), DIMENSION(1:mp,1:2)         :: tmp1d
        REAL(dp), DIMENSION(1:mp,1:2,1:2)     :: tmp2d
    
        ! j=1
        bet(1:mp,1:2,1:2) = B(1:mp,1,1:2,1:2)
        detbet(1:mp)      = bet(1:mp,1,1)*bet(1:mp,2,2) - bet(1:mp,1,2)*bet(1:mp,2,1)
        if (any(abs(detbet(1:mp)) < epsilon(detbet))) then
           write(*,*) 'generic_thomas_2d error1: det = 0'
           stop 'program terminated by generic_thomas_2d'
        endif
        detbet1(1:mp) = one/detbet(1:mp)
        d(1:mp,1:2)   = r(1:mp,1,1:2)
        u(1:mp,1,1)   = (bet(1:mp,2,2)*d(1:mp,1) - bet(1:mp,1,2)*d(1:mp,2))*detbet1(1:mp)
        u(1:mp,1,2)   = (bet(1:mp,1,1)*d(1:mp,2) - bet(1:mp,2,1)*d(1:mp,1))*detbet1(1:mp)
        ! j=2, n
        do j=2, n
           d(1:mp,1:2)       = C(1:mp,j-1,1:2,1)
           G(1:mp,j,1,1)     = (bet(1:mp,2,2)*d(1:mp,1) - bet(1:mp,1,2)*d(1:mp,2))*detbet1(1:mp)
           G(1:mp,j,2,1)     = (bet(1:mp,1,1)*d(1:mp,2) - bet(1:mp,2,1)*d(1:mp,1))*detbet1(1:mp)
           d(1:mp,1:2)       = C(1:mp,j-1,1:2,2)
           G(1:mp,j,1,2)     = (bet(1:mp,2,2)*d(1:mp,1) - bet(1:mp,1,2)*d(1:mp,2))*detbet1(1:mp)
           G(1:mp,j,2,2)     = (bet(1:mp,1,1)*d(1:mp,2) - bet(1:mp,2,1)*d(1:mp,1))*detbet1(1:mp)
           tmp2d(1:mp,1,1)   = A(1:mp,j,1,1)*G(1:mp,j,1,1) + A(1:mp,j,1,2)*G(1:mp,j,2,1)
           tmp2d(1:mp,1,2)   = A(1:mp,j,1,1)*G(1:mp,j,1,2) + A(1:mp,j,1,2)*G(1:mp,j,2,2)
           tmp2d(1:mp,2,1)   = A(1:mp,j,2,1)*G(1:mp,j,1,1) + A(1:mp,j,2,2)*G(1:mp,j,2,1)
           tmp2d(1:mp,2,2)   = A(1:mp,j,2,1)*G(1:mp,j,1,2) + A(1:mp,j,2,2)*G(1:mp,j,2,2)
           bet(1:mp,1:2,1:2) = B(1:mp,j,1:2,1:2) - tmp2d(1:mp,1:2,1:2)
           detbet(1:mp)      = bet(1:mp,1,1)*bet(1:mp,2,2) - bet(1:mp,1,2)*bet(1:mp,2,1)
           if (any(abs(detbet(1:mp)) < epsilon(detbet))) then
              write(*,*) 'generic_thomas_2d error2: det = 0 at j=', j
              stop 'program terminated by generic_thomas_2d'
           endif
           detbet1(1:mp) = one/detbet(1:mp)
           tmp1d(1:mp,1) = A(1:mp,j,1,1)*u(1:mp,j-1,1) + A(1:mp,j,1,2)*u(1:mp,j-1,2)
           tmp1d(1:mp,2) = A(1:mp,j,2,1)*u(1:mp,j-1,1) + A(1:mp,j,2,2)*u(1:mp,j-1,2)
           d(1:mp,1:2)   = r(1:mp,j,1:2) - tmp1d(1:mp,1:2)
           u(1:mp,j,1)   = (bet(1:mp,2,2)*d(1:mp,1) - bet(1:mp,1,2)*d(1:mp,2))*detbet1(1:mp)
           u(1:mp,j,2)   = (bet(1:mp,1,1)*d(1:mp,2) - bet(1:mp,2,1)*d(1:mp,1))*detbet1(1:mp)
        end do
        ! back substitution
        do j=n-1, 1, -1
           tmp1d(1:mp,1) = G(1:mp,j+1,1,1)*u(1:mp,j+1,1) + G(1:mp,j+1,1,2)*u(1:mp,j+1,2)
           tmp1d(1:mp,2) = G(1:mp,j+1,2,1)*u(1:mp,j+1,1) + G(1:mp,j+1,2,2)*u(1:mp,j+1,2)
           u(1:mp,j,1:2) = u(1:mp,j,1:2) - tmp1d(1:mp,1:2)
        end do
        !
    END SUBROUTINE generic_thomas_2d

    ELEMENTAL PURE SUBROUTINE hyofh(h, lam, eta, Ke, he, K, Kh, phi)
        ! Get soil water variables from h.
        ! Definitions of arguments:
        ! h   - matric head.
        ! K   - hydraulic conductivity.
        ! Kh  - derivative dK/dh.
        ! phi - matric flux potential (MFP).
        IMPLICIT NONE

        REAL(dp),    INTENT(IN)  :: h
        REAL(dp),    INTENT(IN)  :: lam
        REAL(dp),    INTENT(IN)  :: eta
        REAL(dp),    INTENT(IN)  :: Ke
        REAL(dp),    INTENT(IN)  :: he

        REAL(dp),    INTENT(OUT) :: K
        REAL(dp),    INTENT(OUT) :: Kh
        REAL(dp),    INTENT(OUT) :: phi
            
        REAL(dp) :: a

        a   =  -lam * eta
        K   =  Ke * exp(a*log(h/he))
        Kh  =  a * K / h
        phi =  K * h / (one+a)

    END SUBROUTINE hyofh

    ELEMENTAL PURE SUBROUTINE hyofS(S, Tsoil, parin, var)
        ! Get soil water variables from S.
        ! Definitions of arguments:
        ! S(1:ms)   - degree of saturation ("effective satn") of layers.
        ! ms        - no. of soil layers.
        ! jt(1:ms)  - layer soil type nos.
        ! var(1:ms) - other water vars of layers.
            IMPLICIT NONE

        !************************************************************
        !     (1) Declaring Variable
        !************************************************************
            TYPE(params), INTENT(INOUT)    :: parin
            TYPE(vars),   INTENT(INOUT)    :: var

            REAL(dp),    INTENT(INOUT)    :: S
            REAL(dp),    INTENT(IN)    :: Tsoil
            
            REAL(dp) :: lnS, theta, c, v3, v4
            REAL(dp) :: dhdS, lambda, crh, int
            REAL(dp) :: thetal_max
            REAL(dp) :: Sliq
            REAL(dp) :: A, B, D, C1
            INTEGER(i4) :: E
            REAL(dp) :: F1, F2, F

            ! REAL(dp) :: macropore_modifier
            REAL(dp) :: cdry, tmp_thetai
            REAL(dp), parameter :: tol = 1.e-6_dp

        !************************************************************
        !     (2) theta
        !************************************************************
            if(S<1e-3_dp) S=1e-3_dp
            theta  = S*(parin%thre) + (parin%the - parin%thre)

            if(parin%lambc < 0.01) parin%lambc = 0.01
        !************************************************************
        !     (3) var%lambdav, var%lambdaf
        !         var%lambdav - latent heat of vaporisation
        !         var%lambdaf  - latent heat of fusion
        !************************************************************
            var%lambdav   = 1.91846e6_dp*((Tsoil+Tzero)/((Tsoil+Tzero)-33.91_dp))**2  ! Henderson-Sellers, QJRMS, 1984
            var%lambdaf   = lambdaf  ! latent heat of fusion

        !************************************************************
        !     (4) var%Tfrz
        !         var%Tfrz - temperature freezing point
        !************************************************************
            var%Tfrz = Tfrz(S, parin%he, one/(parin%lambc*freezefac)) ! freezefac for test of steep freezing curve

        !************************************************************
        !     (5) IF ((Tsoil < var%Tfrz-tol) -- soil freezing
        !         Modify par & var
        !************************************************************
            if (Tsoil < var%Tfrz-tol) then ! ice
            
                !###############################################################
                !           5.1. par%lam & par%eta
                !###############################################################
                parin%lam     = parin%lambc * freezefac   ! freezefac>1 -> steeper freezing curve
                parin%eta     = two/parin%lam + two + one ! freezefac>1 -> steeper freezing curve

                !###############################################################
                !           5.2. var%dthetaldT
                ! thetal_max         - max soil moisture in liquid form
                ! var%dthetaldT      - derivative thetal_max wrt T
                !###############################################################
                thetal_max    = thetalmax(Tsoil,S,parin%he,one/parin%lam,parin%thre,parin%the)
                var%dthetaldT = dthetalmaxdT(Tsoil,S,parin%he,one/parin%lam,parin%thre,parin%the)
                
                !###############################################################
                !           5.3. var%iice
                ! var%iice      - flag, (1) ice (soil freezing), (0) no ice
                !###############################################################
                var%iice = 1

                !###############################################################
                !           5.4. var%thetai, var%thetal
                ! var%thetai      - soil moisture in ice form
                ! var%thetal      - soil moisture in liquid form
                !###############################################################
                var%thetai = max((theta - thetal_max),zero) ! volumetric ice content (m3(liq H2O)/m3 soil)
                tmp_thetai = max(min(theta, parin%thre) - thetal_max,zero)

                var%thetal = thetal_max

                !###############################################################
                !           5.5. var%Sliq
                ! var%Sliq      - relative saturation of liquid water content
                !###############################################################
                if ((parin%thre-tmp_thetai) .le. max(parin%thr,1.e-5_dp)) then
                    Sliq = max(parin%thr,1.e-5_dp)
                else
                    Sliq = min((var%thetal-(parin%the-parin%thre))/(parin%thre-tmp_thetai), one)
                endif

                var%Sliq = Sliq
                
                !###############################################################
                !           5.6. var%he, var%h
                ! var%he      - air entry potential, flux matric potential at saturation
                ! var%h       - matric potential value
                !###############################################################
                ! saturated liquid water content (< 1 for frozen soil)
                lnS        = log(Sliq)
                v3         = exp(-lnS/parin%lam)
                v4         = exp(parin%eta*lnS)
                dhdS   = zero

                var%he = parin%he
                var%h  = parin%he*v3  ! matric potential

                !###############################################################
                !           5.7. var%K, var%Ksat, var%KS, var%KT
                ! var%K      - Unsaturated hydraulic conductivity
                ! var%Ksat   - Saturated hydraulic conductivity
                ! var%KS     - Derivative K wrt S (saturation degree)
                ! var%KT     - Derivative K wrt T (Temperature)
                !###############################################################
                
                var%Ksat  = parin%Ke*v4
                var%K  = var%Ksat
                var%KS = zero
                var%KT = var%dthetaldT * parin%Ke * parin%eta * & 
                        exp(lnS*(parin%eta-one))/max(parin%thre-var%thetai,max(parin%thr,1e-5_dp))

                !###############################################################
                !           5.8. var%phie, var%phi, var%phiT, var%phiS
                ! var%phi    - Unsaturated MFP (matric flux potential -- phi = -K*dh/dz)
                ! var%phie   - Saturated MFP (matric flux potential -- phi = -K*dh/dz)
                ! var%phiS     - Derivative phi wrt S (saturation degree)
                ! var%phiT     - Derivative phi wrt T (Temperature)
                !###############################################################
                var%phie  = parin%phie*v3*v4
                if (S < one) var%phi = var%phie

                if (var%isat==0) then
                    var%phiT =  -( ( (-one +parin%eta*parin%lam) * parin%phie * (min(theta,parin%thre) - parin%thre) &
                        * exp( (-one + parin%eta - one/parin%lam) &
                        * log(var%thetal/(-min(theta,parin%thre) + var%thetal + parin%thre)) ) ) &
                        / (parin%lam*(-min(theta,parin%thre) + var%thetal + parin%thre)**2) ) * var%dthetaldT
                        
                    var%phiS =((-one + parin%eta*parin%lam)*parin%phie* &
                        exp((one + parin%eta - one/parin%lam)*log(var%thetal/(-min(theta,parin%thre) + var%thetal + parin%thre)))) / &
                        (parin%lam*var%thetal)*parin%thre
                else
                    var%phiS = zero
                    var%phiT = zero
                endif

                !###############################################################
                !           5.9. var%rh
                ! var%rh    - relative humidity (hr in the paper)
                !###############################################################
                var%rh   = max(exp(Mw*gravity*var%h/Rgas/(Tsoil+Tzero)), rhmin)
            
            !************************************************************
            !     (6) No Ice -- Soil not freezing
            !         Modify par & var as like above point (5)
            !************************************************************
            else
                parin%lam  = parin%lambc               ! freezefac>1 -> steeper freezing curve
                parin%eta  = two/parin%lam + two + one ! freezefac>1 -> steeper freezing curve
                
                var%dthetaldT = zero

                var%iice   = 0
                
                var%thetai = zero
                var%thetal = S*(parin%thre) + (parin%the - parin%thre)
                
                Sliq  = S        ! liquid water content, relative to saturation

                var%he     = parin%he

                var%Ksat   = parin%Ke

                var%phie   = parin%phie
                
                dhdS   = zero
                lnS     = log(Sliq)
                v3      = exp(-lnS/parin%lam)
                v4      = exp(parin%eta*lnS)
            endif
        
        !************************************************************
        !     (7) Soil completely frozen -- if (var%thetal < 1.e-12_dp)
        !         Modify var%dthetaldT, var%lambdav, var%KT 
        !************************************************************
            if (var%thetal < 1.e-12_dp) then ! completely frozen
                var%dthetaldT = zero
                var%lambdav   = lambdas ! latent heat of sublimation
                var%KT        = zero
            endif
            
        !************************************************************
        !     (8) Soil Unsaturated
        !         
        !************************************************************
            if ((Tsoil >= var%Tfrz-tol) .and. (S < one)) then ! unsaturated
                var%h    = parin%he*v3  ! matric potential
                if(var%h < hmin) var%h = hmin

                var%K    = parin%Ke*v4
                var%KS   = parin%eta*var%K/S
                var%KT   = zero

                var%phi  = parin%phie*v3*v4
                var%phiS = (parin%eta-one/parin%lam)*var%phi/S
                var%phiT = zero
                
                var%rh   = max(exp(Mw*gravity*var%h/Rgas/(Tsoil+Tzero)),rhmin)

                dhdS     = -parin%he/parin%lam*exp((-one/parin%lam-one)*log(S))
            endif
        
        !************************************************************
        !     (9) Soil Saturated
        !         
        !************************************************************
            if ((Tsoil >= var%Tfrz-tol) .and. (S >= one)) then ! saturated
                var%h    = parin%he
                
                var%K    = parin%Ke
                var%KS   = parin%eta*parin%Ke
                var%KT   = zero

                var%phi  = parin%phie
                var%phiS = (parin%eta-one/parin%lam)*parin%phie
                var%phiT = zero

                var%rh   = one

                dhdS     = zero
            endif
        
        !************************************************************
        !     (10) Recalculate theta
        !         
        !************************************************************
            !  variables required for vapour phase transfer
            theta  =  min(S,one)*(parin%thre) + (parin%the - parin%thre)
  
        !************************************************************
        !     (11) var%macropore_factor, var%sl
        ! var%sl       - litter degree of saturation    
        !************************************************************
            var%macropore_factor = one
            var%sl = slope_esat(Tsoil) * Mw/thousand/Rgas/(Tsoil+Tzero) ! m3 m-3 K-1
        
        !************************************************************
        !     (12) Recalculate var%rh
        ! var%sl       - litter degree of saturation    
        !************************************************************
            if (S < one) then
                var%rh = max(exp(Mw*gravity*var%h/Rgas/(Tsoil+Tzero)),rhmin)
            else
                var%rh = one
            endif
        
        !************************************************************
        !     (13) var%hS, var%rhS
        ! var%hS       - derivative h wrt S
        ! var%rhS      - hS * c * rh --> for calculating qvh (flux of vapor in h-term)
        !************************************************************
            c          = Mw*gravity/Rgas/(Tsoil+Tzero) ! c variable for calc. qvh
            crh        = c*var%rh !c*rh
            var%hS     = dhdS
            var%rhS    = crh*dhdS

        !************************************************************
        !     (14) var%cvsat, var%cv, var%cvS, var%cvsatT
        ! var%cvsat   - saturated concentration of water vapour in soil air spaces 
        ! var%cv      - concentration of water vapour in soil air spaces
        ! var%cvS     - derivative of cv wrt S ??? 
        ! var%cvsatT  - derivative of cvsat wrt T ???
        !************************************************************
            var%cvsat  = esat(Tsoil)*Mw/thousand/Rgas/(Tsoil+Tzero) ! m3 m-3
            if (S < one) then
                var%cv     = var%rh*var%cvsat
                var%cvS    = var%rhS *var%cvsat
                var%cvsatT = slope_esat(Tsoil)*Mw/thousand/Rgas/(Tsoil+Tzero) ! m3 m-3 K-1
            else
                var%cv = zero
                var%cvS    = zero
                var%cvsatT = zero
            endif

        !************************************************************
        !     (15) var%Dv, var%kv
        ! var%Dv       - diffusivity of water vapour in the bulk soil
        ! var%kv       - variable to calculate qv --> k_vh. k_vT
        !************************************************************
            ! Penman (1940): tortuosity*theta
            var%Dv    = Dva*parin%tortuosity*(parin%the-theta) * &
                        exp(1.88_dp*log((Tsoil+Tzero)/Tzero)) ! m2 s-1
            var%kv    = var%Dv * var%cvsat * c * var%rh
           
        !************************************************************
        !     (16) var%phiv, var%phivS
        ! var%phiv       - phi of vapor --> matrix flux potential of vapor
        ! var%phivS      - derivative of phi wrt S
        !************************************************************
            lambda = parin%lam
            int       = exp(lambda*log(-c*parin%he)) * igamma(one-lambda,-c*var%h)
            var%phiv  = Dva*parin%tortuosity * (parin%thre*exp(c*var%h) -parin%thre*int)
            var%phivS = dhdS * Dva*parin%tortuosity * &
                ( (parin%thre)*c*exp(c*var%h) - parin%thre*c*exp(-parin%lam*log(var%h/parin%he))*exp(c*var%h) )
                
        !************************************************************
        !     (17) var%kH - ithermalcond - Thermal conductivity of soil 
        ! 0: Campbell (1985)
        ! 1: Van de Griend and O'Neill (1986)
        ! DEFAULT = 0
        !************************************************************
            select case (ithermalcond)
            case (0)
                ! calculate v%kH as in Campbell (1985) p.32 eq. 4.20
                A  = 0.65_dp - 0.78_dp*parin%rho/thousand + 0.60_dp*(parin%rho/thousand)**2 ! (4.27)
                B  = 2.8_dp * (one-parin%thre) !*theta   ! (4.24)

                !MC if (parin%clay > 0.05) then
                if (parin%clay > 0.001_dp) then ! clay=0.001 -> C1=9.2
                    C1 = one + 2.6_dp/sqrt(parin%clay*100._dp) ! (4.28)
                else
                    C1 = one
                endif
                D  = 0.03_dp + 0.7_dp*(one-parin%thre)**2 ! (4.22)
                E  = 4
           
                F1 = 13.05_dp
                F2 = 1.06_dp
                if  (Tsoil < var%Tfrz-tol .and.  var%thetai.gt.zero ) then ! ice
                    ! F  = one + F1*var%thetai**F2
                    F  = one + F1*exp(F2*log(var%thetai))
                    if ((C1*(theta+F*var%thetai))**E > 100._dp) then
                        var%kH = A + B*(theta+F*var%thetai)
                    else
                        var%kH = A + B*(theta+F*var%thetai)-(A-D)*exp(-(C1*(theta+F*var%thetai))**E)
                    endif
                else
                    var%kH = A + B*(theta)-(A-D)*exp(-(C1*(theta))**E)
                endif

            case(1) ! van de Griend & O'Neill (1986)
                cdry = 2.0e6_dp * (one-parin%thre) ! Sispat Manual (2000), Eq. 2.21
                var%kH = one/(cdry + 4.18e6_dp*theta) * ( (parin%LambdaS + 2300._dp*theta - 1890._dp)/0.654_dp )**2
            end select
        
        !************************************************************
        !     (16) var%eta_th, var%kE, var%kth
        ! var%eta_th        - enhancement factor for transport of water vapour across a temperature gradient
        ! var%kE            - Thermal conductivity of soil in vapour phase
        ! var%kth           - Thermal conductivity of soil (includes contribution from vapour phase)
        !************************************************************
            var%eta_th = one
            var%kE     = var%eta_th * var%Dv * var%rh * var%sl*thousand * var%lambdav 
            var%kth    = var%kE + var%kH 
        
        !************************************************************
        !     (17) var%csoil, var%csoileff
        ! par%css      - soil heat capacity [J/Kg K] -- Gaylon Campbell 1985
        ! var%csoil    - volumetric heat capacity of soil
        ! var%csoileff - effective volumetric heat capacity of the soil
        !************************************************************
            var%csoil    = parin%css*parin%rho + rhow*cswat*var%thetal
            
            ! when soil contains ice, its effective heat capacity increases 
            ! due to the energy required for phase change (freezing/thawing).
            if (Tsoil < var%Tfrz) then ! increase effective heat capacity due to presence of ice
                var%csoileff = var%csoil
            else
                var%csoileff = var%csoil
            endif

    END SUBROUTINE hyofS
    
    ELEMENTAL PURE SUBROUTINE flux(parin, v1, v2, dz, q, qya, qyb, qTa, qTb)
     ! VH modified 25/05/10 to include t-dep component of liquid flux (in frozen soil)

     !*************************************************
     !     (1) Declaring variable
     !*************************************************
        ! Gets flux and partial derivs for specified flow path.
        ! Definitions of arguments:
        ! j   - soil type no.
        ! v1  - water vars at upper end of path.
        ! v2  - ditto at lower end.
        ! dz  - length of path.
        ! q   - flux.
        ! qya - partial deriv of flux wrt S (if unsat) or phi (if sat) at upper end.
        ! qyb - ditto at lower end.
        IMPLICIT NONE

        TYPE(params), INTENT(IN)  :: parin
        TYPE(vars),   INTENT(IN)  :: v1, v2
        REAL(dp),    INTENT(IN)  :: dz
        REAL(dp),    INTENT(OUT) :: q, qya, qyb, qTa, qTb
    
        REAL(dp) :: w, rdz
    
     !*************************************************
     !     (2) Calculate w (weighting) accroding to gf
     !        gf default = 1
     !        gf is gravity factor (0 to 1) assumed available in module
     !*************************************************
        ! parin    - parameters of soil
        ! v2%h     - pressure head of below soil (v2)
        ! v2%K*v2%macropore_factor   - K unsaturated of below soil (v2)
        !                            - macropore_factor = 1 (default)
        ! v2%phi   - matric flux potential, phi = K*dh
        ! dz       - thickness of mid point of soil layer
        if (gf < zero) then
            if ((v1%isat /= 0 .and. v2%isat /= 0) .or. v1%h-gf*(-dz) >= v1%he) then
                w = one
            else
                w = weight(parin, v1%h, v1%K*v1%macropore_factor, v1%phi, -dz)
                w = one-w
            end if
        else
            if ((v1%isat /= 0 .and. v2%isat /= 0) .or. v2%h-gf*dz >= v2%he) then ! saturated condition
                w = zero
            else !unsaturated
                w = weight(parin, v2%h, v2%K*v2%macropore_factor, v2%phi, dz)
            end if
        end if

     !*************************************************
     !     (3) Calculate qliq - flux of liquid water (liquid + vapor)
     !           qw(liq) = dphi/dz + K
     !           Eq. A51
     !*************************************************
        rdz = one/dz
        q   = (v1%phi-v2%phi)*rdz + gf*(w*v1%K*v1%macropore_factor + (one-w)*v2%K*v2%macropore_factor)
        
     !*************************************************
     !     (4) Calculate derivative q wrt S and T
     !           dqS/dS =  (v1%phiS-v2%phiS)*rdz --> v1%phiS*rdz
     !*************************************************
        ! Above Unsaturated
        if (v1%isat==0) then
            qya = v1%phiS*rdz + gf*w*(v1%KS)*v1%macropore_factor
            qTa = v1%phiT*rdz + gf*w*(v1%KT)*v1%macropore_factor
        ! Above Saturated
        else
            qya = rdz
            qTa = zero !v1%phiT = zero
        end if

        ! Below Unsaturated
        if (v2%isat==0) then
            qyb = -v2%phiS*rdz + gf*(one-w)*v2%KS*v2%macropore_factor
            qTb = -v2%phiT*rdz + gf*(one-w)*v2%KT*v2%macropore_factor
        ! Below Saturated
        else 
            qyb = -rdz
            qTb = zero
        end if

    END SUBROUTINE flux

    ELEMENTAL PURE SUBROUTINE potential_evap(Rn, rbh, rbw, Ta, rha, & 
        Tsoil, k, dz, lambdav, Epot_mhm, & 
        ! >> OUTPUT..........................................
        Ts, Epot, H, &
        G, dEpotdrha, dEpotdTs, dEpotdTsoil, dGdTa, dGdTsoil, iice)

        ! Pennman-Monteith equation, with additional account for heat flux into the surface
        IMPLICIT NONE

        REAL(dp), INTENT(IN)  :: Rn, rbh, rbw, Ta, rha, Tsoil, k, dz, lambdav
        REAL(dp), INTENT(OUT) :: Ts, Epot, H, G, dEpotdrha, dEpotdTs, dEpotdTsoil, dGdTa, dGdTsoil
        LOGICAL, INTENT(IN), OPTIONAL :: iice

        REAL(dp), INTENT(INOUT)  :: Epot_mhm

        !  LOCAL................
        REAL(dp) :: s, es, ea, dEpotdea, dEpotdesat, dTsdTa, dEpotdDa, Da
        REAL(dp):: rhocp, gamma != 67.0 ! psychrometric constant

        if (present(iice)) then
            if(iice) then
            es = esat_ice(Ta)
            s  = slope_esat_ice(Ta)
            else
            es = esat(Ta)
            s  = slope_esat(Ta)
            endif
        else
            es = esat(Ta)
            s  = slope_esat(Ta)
        endif

        rhocp = rmair*101325._dp/rgas/(Ta+Tzero)*cpa
        gamma = 101325._dp*cpa/lambdav/(Mw/rmair)
        ea = es * max(rha, 0.1_dp)
        Da = ea/max(rha, 0.1_dp) - ea

        !..................................................................................
        !>> PET CALCULATION
        ! Epot  = (rhocp*(Da*(k*rbh + dz*rhocp) + rbh*s*(dz*Rn + k*(-Ta + Tsoil)))) / &
        !     (gamma*rbw*(k*rbh + dz*rhocp) + dz*rbh*rhocp*s)
        
        Epot = Epot_mhm * (thousand*lambdav)
        !..................................................................................

        Ts = Ta + Epot*gamma*rbw/s/rhocp - Da/s
        H  = rhocp*(Ts - Ta)/rbh
        G  = k*(Ts-Tsoil)/dz

        dEpotdDa    = (-(k*rbh*rhocp) - dz*rhocp**2)/(gamma*k*rbh*rbw + dz*gamma*rbw*rhocp + dz*rbh*rhocp*s)
        dEpotdea    = -dEpotdDa
        dEpotdesat  = dEpotdea
        dEpotdrha   = dEpotdea *es
        !dEpotdTa    = (k*rbh*rhocp*s)/(gamma*k*rbh*rbw + dz*gamma*rbw*rhocp + dz*rbh*rhocp*s) + dEpotdesat *s
        dEpotdTsoil = -((k*rbh*rhocp*s)/(gamma*k*rbh*rbw + dz*gamma*rbw*rhocp + dz*rbh*rhocp*s))

        dTsdTa   = (-(dz*gamma*rbw*rhocp) - dz*rbh*rhocp*s)/(gamma*k*rbh*rbw + dz*gamma*rbw*rhocp + dz*rbh*rhocp*s)

        dGdTa    = k/dz * dTsdTa
        dGdTsoil = -k/dz !+k/dz*dEpotdTsoil*gamma*rbw/s/rhocp
        dEpotdTs = s*rhocp/gamma/rbw

    END SUBROUTINE potential_evap

    ELEMENTAL PURE SUBROUTINE aquifer_props(v_aquifer)
 
     IMPLICIT NONE
 
     TYPE(vars_aquifer), INTENT(INOUT) :: v_aquifer
 
     v_aquifer%zzero     = 53.43_dp  ! water table depth corresponding to Wa = zero
     v_aquifer%Sy        = 0.2_dp    ! specific yield of aquifer
 
     ! initialise water content of aquifer
     v_aquifer%Wa        = v_aquifer%Sy*(v_aquifer%zzero-max(v_aquifer%zdelta,v_aquifer%zsoil))
     
     v_aquifer%isat      = 0
     if (v_aquifer%zdelta <= v_aquifer%zsoil) v_aquifer%isat = 1
 
     v_aquifer%f         = 1.25_dp ! multiplier in exponent of Rs (m-1)
     v_aquifer%Rsmax     = 4.5e-7_dp  ! maximum discharge rate from aquifer (ms-1)
     v_aquifer%discharge = v_aquifer%Rsmax*exp(-v_aquifer%f*v_aquifer%zdelta)
 
    END SUBROUTINE aquifer_props

END MODULE mo_sli_utils