MODULE mo_sli_solve
    use mo_kind, only : i4, dp
    use mo_sli_numbers, only: params, vars_met, vars, vars_snow, vars_aquifer, & 
                            Tzero, Dva, emsoil, rhocp, &
                            freezefac, nsnow_max, &
                            zero, half, one, two, thousand, e5, e3, &
                            rlambda, lambdaf, hmin, hbot, botbc, &
                            cswat, rhow, csice, gf, dtmax, dsmax, dtlmax, h0min, dtmin, &
                            Smax, dSmax, dSmaxr, dSfac, dTsoilmax, lambdas, septs, &
                            hbot2, L1_gwdepth
                            
    use mo_sli_utils, only: setsol, sol, zerovars, hyofh, Sofh, hyofS, flux, &
                            SEB, getfluxes_vp, getheatfluxes, Tfrz, tri,&
                            massman_sparse, JSoilLayer, GTfrozen, thetalmax, rtbis_Tfrozen, &
                            aquifer_props

    use mo_common_variables, only : processMatrix
    use mo_soil_moisture, only : feddes_et_reduction, jarvis_et_reduction
    
    
    use  mo_common_mHM_mRM_variables, only: nTStepDay
    use mo_common_constants, only : eps_dp
    
    implicit none
    private

    public :: solve_sli

    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: nless, n_noconverge ! global counters

CONTAINS

    SUBROUTINE solve_sli(ts, tfin, irec, mp, n, dx, nsteps, &
            dosepts, docondition, doadvection, &
            vmet, &
            var, &
            par, &
            phi, &
            qprec, &
            S, &
            Tsoil, &
            thetai, &
            !............
            q, &
            qlsig, &
            qh, &
            drainage, &
            discharge, &
            !...........
            h0, &
            zdelta, &
            Epot_mhm)
  

     !************************************
     !    (1) Declaring Variable function
     !************************************
        IMPLICIT NONE

        REAL(dp),                             INTENT(IN)              :: ts, tfin
        INTEGER(i4),                          INTENT(IN)              :: irec, mp
        REAL(dp),      DIMENSION(1:mp),       INTENT(IN)              :: qprec
        INTEGER(i4),                          INTENT(IN)              :: n
        REAL(dp),      DIMENSION(1:mp,1:n),   INTENT(IN)              :: dx

        REAL(dp),      DIMENSION(1:mp),       INTENT(INOUT)           :: h0, zdelta
        REAL(dp),      DIMENSION(1:mp,1:n),   INTENT(INOUT)           :: S
        REAL(dp),      DIMENSION(1:mp,1:n),   INTENT(OUT)             :: thetai
        
        REAL(dp),      DIMENSION(1:mp,1:n),   INTENT(INOUT)           :: Tsoil
        

        REAL(dp),      DIMENSION(1:mp),       INTENT(OUT)             :: drainage, discharge
        REAL(dp),      DIMENSION(1:mp,-nsnow_max:n), INTENT(OUT)      :: qh

        INTEGER(i4),   DIMENSION(1:mp),       INTENT(OUT)             :: nsteps
        TYPE(vars_met), DIMENSION(1:mp),      INTENT(INOUT)           :: vmet
        TYPE(vars),     DIMENSION(1:mp,1:n),  INTENT(INOUT)           :: var

        REAL(dp),      DIMENSION(1:mp,1:n),   INTENT(INOUT)           :: phi


        TYPE(params),  DIMENSION(1:mp,1:n),   INTENT(INOUT)           :: par


        REAL(dp), DIMENSION(1:mp,-nsnow_max:n), INTENT(OUT),OPTIONAL  :: qlsig, q


        INTEGER(i4),  INTENT(IN),    OPTIONAL :: dosepts        ! 0: normal; 1: uncouple T & S
        INTEGER(i4),  INTENT(IN),    OPTIONAL :: docondition    ! 0: no cond., 1: columns, 2: lines, 3: both
        INTEGER(i4),  INTENT(IN),    OPTIONAL :: doadvection    ! 0: off; 1: onn

        REAL(dp),      DIMENSION(1:mp),       INTENT(INOUT)           :: Epot_mhm
        


     !************************************
     !    (2) Declaring Variable internal
     !************************************      
        !###################################################
        ! Previously called is mhm_interface_run
        REAL(dp), DIMENSION(1:mp,-nsnow_max:n)  :: qvsig   ! OUT (vapor flux total: Temp form + Pressure heaed form) at Sigma
        REAL(dp), DIMENSION(1:mp,-nsnow_max:n)  :: qvTsig  ! OUT (vapor flux in Temp form at Sigma)
        REAL(dp), DIMENSION(1:mp,-nsnow_max:n)  :: qvh     ! OUT (vapor flux in Pressure head form)
        REAL(dp), DIMENSION(1:mp)               :: evap    ! OUT (actual top soil column evapotranspiration by Pennmann)
        REAL(dp),      DIMENSION(1:mp,1:n)      :: Jsensible ! Sensible heat
        REAL(dp),      DIMENSION(1:mp)          :: T0, Tsurface !Surface soil,pond temperature --> used for call heatfluxes


        !###################################################
        
        TYPE(vars_aquifer), DIMENSION(1:mp) :: v_aquifer
        
        INTEGER(i4) :: advection, septs, condition  ! switches
        INTEGER(i4) :: kk ! loop
        

        
        REAL(dp), DIMENSION(1:mp,1:n) :: deltaJ_latent_S, deltaJ_latent_T, deltaJ_sensible_S, deltaJ_sensible_T
        REAL(dp), DIMENSION(1:mp,1:n) :: dTsoil, thetai_0, J0, thetal_max

        REAL(dp), DIMENSION(1:mp,0:n)   :: tmp2d1, tmp2d2

        REAL(dp), DIMENSION(1:mp,1:n-1) :: dz

        REAL(dp), DIMENSION(1:mp,-nsnow_max+1:n) :: LHS, RHS, LHS_h, RHS_h

        REAL(dp), DIMENSION(1:mp) :: Tfreezing, dT0
        REAL(dp), DIMENSION(1:mp) :: Kmin1, Khmin1, phimin1 
        REAL(dp), DIMENSION(1:mp) :: t
        REAL(dp), DIMENSION(1:mp) :: tmp1d1, tmp1d2, tmp1d3, tmp1d4        

        LOGICAL, DIMENSION(1:mp)      :: init

        !....Temporary Array..............
        REAL(dp), DIMENSION(1:N)           :: tmp_dx, tmp_S, tmp_thetai, tmp_Jsensible, &
                                              tmp_Tsoil, tmp_phi, tmp_hint, &
                                              tmp_phimin, tmp_deltaS, tmp_dTsoil, &
                                              tmp_deltaJ_latent_T, tmp_deltaJ_sensible_S, &
                                              tmp_deltaJ_sensible_T, tmp_deltaJ_latent_S, &
                                              tmp_thetai_0, tmp_J0

        REAL(dp), DIMENSION(0:N)           :: tmp_tmp2d1, tmp_tmp2d2

        REAL(dp), DIMENSION(-nsnow_max+1:n) :: tmp_LHS, tmp_RHS, tmp_LHS_h, tmp_RHS_h

        REAL(dp), DIMENSION(-nsnow_max:n) :: tmp_qh, tmp_qvh, tmp_qvsig, tmp_qlsig, tmp_qvTsig, tmp_q

        TYPE(vars), DIMENSION(1:n)          :: tmp_var
        TYPE(params), DIMENSION(1:n)        :: tmp_par


     !************************************
     !    (3) Set switches
     !************************************
        !.......Advection...............
        if (present(doadvection)) then
            advection = doadvection
         else
            advection = 0
         endif

         !.......Couple T & SM...............
         if (present(dosepts)) then
            septs = dosepts 
         else
            septs = 0
         endif
   
         if (septs > 1) then
            write(*,*) 'dosepts not in [0-1]: ', septs
            stop
         endif
        
        !.......Conditioning...............
        ! 0: no conditioning
        ! 1: condition columns
        ! 2: condition lines
        ! 3: condition first lines then columns
        if (present(docondition)) then
            condition = docondition
        else
            condition = 0
        endif
        if (condition < 0 .or. condition > 3) then
            write(*,*) 'docondition not in [0-3]: ', condition
            stop
        endif

   
 
     !************************************
     !    (5) Global counter & solve_type
     !************************************
        if (.not. allocated(nless)) allocate(nless(mp))
        nless(:) = 0

        if (.not. allocated(n_noconverge)) allocate(n_noconverge(mp))
        n_noconverge(:) = 0

        ! set solve_type for numerical derivatives
        if (.not. allocated(sol)) call setsol(mp)
     
     !************************************
     !    (6) Initialise cumulative variables
     !************************************
        
       !----------------------  Internal variables--------------------
        deltaJ_sensible_S(:,:)   = zero
        deltaJ_sensible_T(:,:)   = zero
        deltaJ_latent_S(:,:)     = zero
        deltaJ_latent_T(:,:)     = zero
        
        
        dTsoil(:,:)            = zero        
        thetai_0(:,:)          = zero
        J0(:,:)                = zero
        dT0(:)                 = zero
        thetal_max             = zero

        tmp1d1(:)  = zero

       !------------------- Input call variables -----------------------

        drainage(:)  = zero
        discharge(:) = zero
        evap(:)      = zero
       
        nsteps       = 0
        qvsig(:,:)   = zero
        qlsig(:,:)   = zero
        q(:,:)       = zero

       !-------------------- Calculate dz --------------------------------
        dz(:,:) = half*(dx(:,1:n-1)+dx(:,2:n)) ! flow paths

        Tsurface = zero

     !************************************
     !    (8) Initialise parameter/variable
     !************************************

       !>> Initialise t(:)
        t(:)       = ts
       
        !>> Initialise isat: initialise Unsaturated/Saturated regions
        var(:,:)%isat  = 0
        where (S(:,:) >= one)
           var(:,:)%K = par(:,:)%Ke
           var(:,:)%isat = 1
           var(:,:)%phi = phi
        end where
  
        !>> initialise aquifer
        if(botbc == "aquifer") then
           v_aquifer(:)%zsoil  = sum(dx(:,:),2)
           v_aquifer(:)%zdelta = zdelta(:)
           call aquifer_props(v_aquifer(:))
        end if

     
     !*************************************
     !    (9) Solve until tfin
     !*************************************
       !-------------------------------------------
       !       9.1. Set init(:) = .true.
       !-------------------------------------------
        init(:) = .true. ! flag to initialise h at soil interfaces
       
       !-------------------------------------------
       !       9.3. Looping do kk=1, mp
       !-------------------------------------------
        !$OMP parallel default(shared) & 
        !$OMP private(kk, tmp_dx, tmp_S, tmp_thetai, tmp_Jsensible, &
        !$OMP tmp_Tsoil, tmp_var, tmp_phi, tmp_par, tmp_dTsoil, tmp_tmp2d1, &
        !$OMP tmp_tmp2d2, tmp_LHS, tmp_RHS, tmp_LHS_h, tmp_RHS_h, &
        !$OMP tmp_deltaJ_latent_T, tmp_deltaJ_sensible_S, tmp_deltaJ_sensible_T, &
        !$OMP tmp_deltaJ_latent_S, tmp_thetai_0, tmp_J0, tmp_qh, tmp_qvh, &
        !$OMP tmp_qvsig, tmp_qlsig, tmp_qvTsig, tmp_q)
        !$OMP do SCHEDULE(STATIC)
        do kk=1, mp 
           ! Creting tmp......................................
            tmp_dx         = dx(kk,:)
            tmp_S          = S(kk,:)
            tmp_thetai     = thetai(kk,:)
            tmp_Jsensible  = Jsensible(kk,:)
            tmp_Tsoil      = Tsoil(kk,:)
            tmp_var        = var(kk,:)
            tmp_phi        = phi(kk,:)
            tmp_par        = par(kk,:)
            tmp_dTsoil     = dTsoil(kk,:)
            tmp_tmp2d1     = tmp2d1(kk,:)
            tmp_tmp2d2     = tmp2d2(kk,:)
            tmp_LHS        = LHS(kk,:)
            tmp_RHS        = RHS(kk,:)
            tmp_LHS_h      = LHS_h(kk,:)
            tmp_RHS_h      = RHS_h(kk,:)
            tmp_deltaJ_latent_T     = deltaJ_latent_T(kk,:)
            tmp_deltaJ_sensible_S   = deltaJ_sensible_S(kk,:)
            tmp_deltaJ_sensible_T   = deltaJ_sensible_T(kk,:)
            tmp_deltaJ_latent_S     = deltaJ_latent_S(kk,:)
            tmp_thetai_0            = thetai_0(kk,:)
            tmp_J0                  = J0(kk,:)

            tmp_qh                  = qh(kk,:)
            tmp_qvh                 = qvh(kk,:)
            tmp_qvsig               = qvsig(kk,:)
            tmp_qlsig               = qlsig(kk,:)
            tmp_qvTsig              = qvTsig(kk,:)
            tmp_q                   = q(kk,:)



           !>> Call timestep_loop.............................................
            CALL timestep_loop( &
                !---------------------------------------------
                ! FUNCTION VARIABLES
                !--------------------------------------------
                t, tfin, irec, mp, n, tmp_dx, nsteps, &
                septs, condition, advection, &
                vmet, &
                tmp_var, &
                tmp_par, &
                tmp_phi, &
                qprec, &
                tmp_S, &
                tmp_Tsoil, &
                tmp_thetai, &
                drainage, &
                discharge, &
                tmp_qlsig, &
                evap, &
                !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                tmp_qh, &
                tmp_qvsig, &
                qvTsig, &
                tmp_qvh, &
                tmp_Jsensible, &
                !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                h0, &
                zdelta, &
                T0, &
                Tsurface, &
                !----------------------------------------------
                ! INTERNAL VARIABLES
                !----------------------------------------------
                kk, &
                init, & ! FLAGS
                v_aquifer, &
                tmp1d1, tmp1d2, tmp1d3, tmp1d4, &
                !.................................................................................
                tmp_dTsoil, tmp_tmp2d1, tmp_tmp2d2, &
                tmp_LHS, tmp_RHS, tmp_LHS_h, tmp_RHS_h, &
                tmp_deltaJ_latent_T, tmp_deltaJ_sensible_S, &
                tmp_deltaJ_sensible_T, tmp_deltaJ_latent_S, &
                tmp_thetai_0, tmp_J0, tmp_q, &
                Epot_mhm)

           !>> Filling Original   
             S(kk,:)             = tmp_S
             thetai(kk,:)        = tmp_thetai
             Jsensible(kk,:)     = tmp_Jsensible
             Tsoil(kk,:)         = tmp_Tsoil
             var(kk,:)           = tmp_var
             phi(kk,:)           = tmp_phi
             par(kk,:)           = tmp_par
             dTsoil(kk,:)        = tmp_dTsoil
             tmp2d1(kk,:)        = tmp_tmp2d1
             tmp2d2(kk,:)        = tmp_tmp2d2
             LHS(kk,:)           = tmp_LHS
             RHS(kk,:)           = tmp_RHS
             LHS_h(kk,:)         = tmp_LHS_h
             RHS_h(kk,:)         = tmp_RHS_h
             deltaJ_latent_T(kk,:)     = tmp_deltaJ_latent_T
             deltaJ_sensible_S(kk,:)   = tmp_deltaJ_sensible_S
             deltaJ_sensible_T(kk,:)   = tmp_deltaJ_sensible_T
             deltaJ_latent_S(kk,:)     = tmp_deltaJ_latent_S
             thetai_0(kk,:)            = tmp_thetai_0
             J0(kk,:)                  = tmp_J0
             
             qh(kk,:)                  = tmp_qh
             qvh(kk,:)                 = tmp_qvh
             qvsig(kk,:)               = tmp_qvsig
             qlsig(kk,:)               = tmp_qlsig
             qvTsig(kk,:)              = tmp_qvTsig
             q(kk,:)                   = tmp_q

        end do
        !$OMP end do
        !$OMP end parallel

      if ( allocated(nless) ) deallocate(nless)
      if ( allocated(n_noconverge) ) deallocate(n_noconverge)
      if ( allocated(sol) ) deallocate(sol)

    END SUBROUTINE solve_sli

    SUBROUTINE timestep_loop( &
                !---------------------------------------------
                ! FUNCTION VARIABLES
                !--------------------------------------------
                t, tfin, irec, mp, n, dx, nsteps, &
                septs, condition, advection, &
                vmet, &
                var, &
                par, &
                phi, &
                qprec, &
                S, &
                Tsoil, &
                thetai, &
                drainage, &
                discharge, &
                qlsig, &
                evap, &
                !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                qh, &
                qvsig, &
                qvTsig, &
                qvh, &
                Jsensible, &
                !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                h0, &
                zdelta, &
                T0, &
                Tsurface, &
                !----------------------------------------------
                ! INTERNAL VARIABLES
                !----------------------------------------------
                kk, &
                init, & ! FLAGS
                v_aquifer, &
                tmp1d1, tmp1d2, tmp1d3, tmp1d4, &
                !.................................................................................
                dTsoil, tmp2d1, tmp2d2, &
                LHS, RHS, LHS_h, RHS_h, &
                deltaJ_latent_T, deltaJ_sensible_S, &
                deltaJ_sensible_T, deltaJ_latent_S, &
                thetai_0, J0, q, &
                Epot_mhm)
    
     !************************************
     !    (1) Declaring Variable function
     !************************************
        IMPLICIT NONE

        REAL(dp)                               :: tfin
        INTEGER(i4)                            :: irec, mp
        REAL(dp),     DIMENSION(1:mp)          :: qprec
        INTEGER(i4)                            :: n
        REAL(dp),      DIMENSION(1:n)          :: dx
        REAL(dp),      DIMENSION(1:mp)         :: h0
        REAL(dp),      DIMENSION(1:n)          :: S
        REAL(dp),      DIMENSION(1:n)          :: thetai
        REAL(dp),      DIMENSION(1:n)          :: Jsensible

        REAL(dp),      DIMENSION(1:n)         :: Tsoil
        REAL(dp),      DIMENSION(1:mp)        :: evap

        REAL(dp),      DIMENSION(1:mp)        :: drainage, discharge
        REAL(dp),      DIMENSION(-nsnow_max:n)      :: qh
        INTEGER(i4),   DIMENSION(1:mp)        :: nsteps
        TYPE(vars_met), DIMENSION(1:mp)       :: vmet
        TYPE(vars),     DIMENSION(1:n)        :: var
        REAL(dp),      DIMENSION(1:mp)        :: T0, Tsurface


        REAL(dp),      DIMENSION(1:n)         :: phi
        REAL(dp),      DIMENSION(1:mp)        :: zdelta

        TYPE(params),   DIMENSION(1:n)        :: par

        REAL(dp),      DIMENSION(-nsnow_max:n),   OPTIONAL :: qvsig, qlsig 
        
        REAL(dp),      DIMENSION(-nsnow_max:n),   OPTIONAL :: qvTsig, qvh

        LOGICAL,     DIMENSION(1:mp)       :: init


        REAL(dp),    DIMENSION(1:mp)       :: t
        REAL(dp),    DIMENSION(1:n)        :: dTsoil
        REAL(dp),    DIMENSION(0:n)        :: tmp2d1, tmp2d2
        TYPE(vars_aquifer), DIMENSION(1:mp) :: v_aquifer
        
        
        REAL(dp),    DIMENSION(1:n)        :: deltaJ_latent_S

        REAL(dp),    DIMENSION(1:n)        :: deltaJ_latent_T
        
        REAL(dp),    DIMENSION(1:n)        :: deltaJ_sensible_S, deltaJ_sensible_T
        REAL(dp),    DIMENSION(1:mp)       :: tmp1d1, tmp1d2, tmp1d3, tmp1d4
        REAL(dp),    DIMENSION(-nsnow_max+1:n) :: LHS, RHS, LHS_h, RHS_h

        INTEGER(i4) :: kk, condition, advection, septs

        REAL(dp)                  :: qw
       

        REAL(dp), DIMENSION(1:n)       :: thetai_0, J0
        REAL(dp), DIMENSION(1:mp)      :: Epot_mhm
        REAL(dp),    DIMENSION(-nsnow_max:n)   :: q

        !...................................................................
        ! INTERNAL VARIABLES
        !...................................................................
        INTEGER(i4)                        :: i, j
        INTEGER(i4), DIMENSION(1:mp)       :: iflux
        LOGICAL,     DIMENSION(1:mp)       :: again
        REAL(dp),    DIMENSION(1:mp)       :: dJcol_latent_S, dJcol_latent_T, dJcol_sensible
        REAL(dp),    DIMENSION(1:mp)       :: h0_0
        REAL(dp),    DIMENSION(1:mp)       :: deltah0
        REAL(dp),    DIMENSION(1:mp)       :: hice
        REAL(dp),    DIMENSION(1:mp)       :: hice_0
        REAL(dp),    DIMENSION(1:mp)       :: deltahice

     !**********************************************************
     !    (2) Declaring Variable Internal
     !*********************************************************
        REAL(dp),    DIMENSION(-nsnow_max+1:n) :: aa, bb, cc, dd, ee, ff, gg, dy
        REAL(dp),    DIMENSION(-nsnow_max+1:n) :: aah, bbh, cch, ddh, eeh, ffh, ggh, de

        REAL(dp),    DIMENSION(-nsnow_max:n)   :: qya, qyb, qTa, qTb,qhya, qhyb, qhTa, qhTb
        REAL(dp),    DIMENSION(-nsnow_max:n)   :: qadv, qadvya, qadvyb, qadvTa, qadvTb
        REAL(dp),    DIMENSION(-nsnow_max:n)   :: qsig, qhsig, qadvsig
        REAL(dp),    DIMENSION(-nsnow_max:n)   :: qliq, qv, qvT, qlya, qlyb, qvya, qvyb, qlTb, qvTa, qvTb

        
        INTEGER(i4), DIMENSION(1:n)               :: isave


        REAL(dp),    DIMENSION(1:n)               :: S0, Sliqice, Sliqice0, Sice0, Sliq0, cv0, Sliq
        REAL(dp),    DIMENSION(1:n)               :: deltacv, deltaSliqice, delthetai, Sice, deltasice, deltaSliq
        REAL(dp),    DIMENSION(1:n)               :: dthetaldT, thetal


     !**********************************************************
     !    (3) Initialize value
     !**********************************************************
        h0_0       = zero
        deltah0(:) = zero
        hice       = zero
        hice_0     = zero
        deltahice  = zero

        aa(:)     = zero
        aah(:)    = zero
        bb(:)     = zero
        bbh(:)    = zero
        cc(:)     = zero
        cch(:)    = zero
        dd(:)     = zero
        ddh(:)    = zero
        ee(:)     = zero
        eeh(:)    = zero
        ff(:)     = zero
        ffh(:)    = zero
        gg(:)     = zero
        ggh(:)    = zero
        dy(:)     = zero

        qya(:)    = zero
        qyb(:)    = zero
        qTa(:)    = zero
        qTb(:)    = zero
        qhya(:)   = zero
        qhyb(:)   = zero
        qhTa(:)   = zero
        qhTb(:)   = zero
        qadvyb(:) = zero
        qadvya(:) = zero
        de(:)     = zero

        qadvTa = zero
        qadvTb = zero
        qliq = zero
        qv = zero
        qvT = zero
        qlya = zero
        qlyb = zero
        qvya = zero
        qvyb = zero
        qlTb = zero
        qvTa = zero
        qvTb = zero

        do i = 1, n
            if (var(i)%thetai < 0.0001_dp) then
               var(i)%thetai = zero
            end if
        end do
     

     !******************************************************************
     !    (4) loop while t(kk) < tfin
     !       t = 0 at init , tfin = dt_ori = 86400 seconds for daily run
     !       kk = loop 1:mp (mp=1 for single grid)
     !******************************************************************
        do while (t(kk) < tfin) 
            !................................
            !      3.1. Setting iflux & again
            !................................
             iflux(kk)  = 1 ! flag for calculating flux
             again(kk)  = .true. ! flag for recalcn of fluxes (default=false)


            !................................
            !      3.3. Setting S (effective saturation)
            !................................
             S0(1:n) = S(1:n) !initial effective saturation of soil
             Sice0(1:n)    = var(1:n)%thetai / par(1:n)%thre !initial effective saturation of soil-ice
             Sliqice0(1:n) = (S(1:n) - var(1:n)%cv) / (one-var(1:n)%cv) ! cv: concentration of water vapour in soil air spaces (m3_LiqWater/m3_air)
             Sliq0(1:n)    = Sliqice0(1:n) - Sice0(1:n)

             ! calculate surface ice & liquid
             Sice0(1) = Sice0(1)  + hice_0(kk)/(dx(1)*par(1)%thre)
             Sliq0(1) = Sliq0(1) + (h0_0(kk)-hice_0(kk))/(dx(1)*par(1)%thre) ! add pond component to Sliq(1)
            
          
            !.............................................
            !      3.5. Call iflux_loop
            !............................................
             CALL iflux_loop(&
               tfin, irec, mp, qprec, n, dx(:), h0, S(:), thetai(:), &
               Jsensible(:), Tsoil(:), evap, drainage, discharge, &
               qh, nsteps, vmet, var(:), T0, Tsurface, &
               phi(:), zdelta, &
               par(:), &
               qvsig, qlsig, qvTsig, qvh, &
               again, init, &
               t, &
               aa(:), bb(:), cc(:), dd(:), ee(:), ff(:), gg(:), dy(:), &
               aah(:), bbh(:), cch(:), ddh(:), eeh(:), ffh(:), ggh(:), &
               de(:), q(:), qya(:), qyb(:), qTa(:), qTb(:),qhya(:), qhyb(:), qhTa(:), qhTb(:), qadv(:), qadvya(:), qadvyb(:), &
               qadvTa(:), qadvTb(:), qsig(:), qhsig(:), qadvsig(:), qliq(:), qv(:), qvT(:), qlya(:), qlyb(:), &
               qvya(:), qvyb(:), qlTb(:), qvTa(:), qvTb(:), dTsoil(:), tmp2d1(:), tmp2d2(:), &
               cv0(:), &
               dthetaldT(:), thetal(:), isave, v_aquifer, &
               dJcol_latent_S, dJcol_latent_T, dJcol_sensible, deltaJ_latent_S(:), &
               deltaJ_latent_T(:), deltaJ_sensible_S(:), deltaJ_sensible_T(:), &
               tmp1d1, tmp1d2, tmp1d3,  tmp1d4, deltah0, &
               LHS, RHS, LHS_h, RHS_h, &
               iflux, kk, condition, &
               advection, septs, &
               hice, h0_0, hice_0, &
               thetai_0(:), J0(:), &
               Epot_mhm)
            
    
            !...............................................
            !      3.7. Set cv=0 when soil saturated and at  surface soil, when pond exist
            ! cv: concentration of water vapour in soil air spaces (m3_LiqWater/m3_air)
            !..................................................
               
               where ((S0(:).ge.one).or.(S(:).ge.one))
                  var(1:n)%cv = zero
                  cv0(1:n) = zero
               endwhere

               if (h0(kk).gt.zero.or.h0_0(kk).gt.zero) then
                  var(1)%cv = zero
                  cv0(1) = zero
               endif
            
            !...............................
            !      3.9. Update "S" of liquid-ice
            !...............................
               Sliqice0(1:n) = (S0(1:n) - cv0(1:n))/(one-cv0(1:n))
               deltacv(1:n) = var(1:n)%cv - cv0(1:n)
               deltah0(kk) = h0(kk) - h0_0(kk)
               
               Sliqice(1:n) = (S(1:n) - var(1:n)%cv)/(one-var(1:n)%cv)
               deltaSliqice(1:n) = Sliqice(1:n) - Sliqice0(1:n)

            !..........................................................
            !      3.10. add pond component to Sliqice(1) and deltaSliqice(1)
            !..........................................................
               Sliqice(1) = Sliqice(1) + h0(kk)/(dx(1)*par(1)%thre) ! add pond component to Sliq(1)
               deltaSliqice(1) =  deltaSliqice(1) + deltah0(kk)/(dx(1)*par(1)%thre)


            !...............................................................
            !      3.11. Change in snow-pack (lumped with top layer)
            !...............................................................
               delthetai(1:n) = (var(1:n)%thetai-thetai_0(1:n))

               hice(kk) = h0(kk)*var(1)%thetai/par(1)%thre
               deltahice(kk) =  hice(kk) - hice_0(kk)
               deltahice(kk) =  hice(kk) - h0_0(kk)*thetai_0(1)/par(1)%thre

             
            
            !...............................................................
            !      3.12. Update "S" & "thetha" of ice & liquid
            !...............................................................
               Sice(1:n) = var(1:n)%thetai/par(1:n)%thre
               deltaSice(1:n) = delthetai(1:n)/par(1:n)%thre
               deltahice(kk) = hice(kk)-hice_0(kk)
               Sice(1) = Sice(1)  + hice(kk)/(dx(1)*par(1)%thre)
               deltaSice(1) = deltaSice(1) + deltahice(kk)/(dx(1)*par(1)%thre)

               Sliq(1:n) = Sliqice(1:n) - Sice(1:n)
               deltaSliq(1:n) = deltaSliqice(1:n) - deltaSice(1:n)


               thetai_0(1:n)    = var(1:n)%thetai
               thetai(1:n) = var(1:n)%thetai
               dthetaldt(1:n) = var(1:n)%dthetaldT

            
            !........................................................
            !      3.16. Calculate Jsensible (sensible heat) of soil & pond
            !                 h0 = 0 (default)
            !                 n: number of soil layers (ms)
            !........................................................
               Jsensible(1) = (var(1)%csoil* dx(1)+h0(kk)*cswat)*(Tsoil(1))
               Jsensible(2:n) = var(2:n)%csoil*(Tsoil(2:n))* dx(2:n)
            
            !........................................................
            !      3.17. Calculate change in heat stored in soil column
            !........................................................
               dJcol_latent_S(kk) = sum(deltaJ_latent_S(1:n))
               dJcol_latent_T(kk) = sum(deltaJ_latent_T(1:n))
               dJcol_sensible(kk) = sum(deltaJ_sensible_T(1:n)) + sum(deltaJ_sensible_S(1:n))


               init(kk)          = .false.
            
        end do ! while (t<tfin)


    END SUBROUTINE timestep_loop

    SUBROUTINE iflux_loop( &
               tfin, irec, mp, qprec, n, dx, h0, S, thetai, &
               Jsensible, Tsoil, evap, drainage, discharge, &
               qh, nsteps, vmet, var, T0, Tsurface, &
               phi, zdelta, &
               par, &
               qvsig, qlsig, qvTsig, qvh, &
               again, init, &
               t, &
               aa, bb, cc, dd, ee, ff, gg, dy, &
               aah, bbh, cch, ddh, eeh, ffh, ggh, &
               de, q, qya, qyb, qTa, qTb, qhya, qhyb, qhTa, qhTb, qadv, qadvya, qadvyb, &
               qadvTa, qadvTb, qsig, qhsig, qadvsig, qliq, qv, qvT, qlya, qlyb, &
               qvya, qvyb, qlTb, qvTa, qvTb, dTsoil, tmp2d1, tmp2d2, &
               cv0, &
               dthetaldT, thetal, isave, v_aquifer, &
               dJcol_latent_S, dJcol_latent_T, dJcol_sensible, deltaJ_latent_S, &
               deltaJ_latent_T, deltaJ_sensible_S, deltaJ_sensible_T, &
               tmp1d1, tmp1d2, tmp1d3,  tmp1d4, deltah0, &
               LHS, RHS, LHS_h, RHS_h, &
               iflux, kk, condition, &
               advection, septs, &
               hice, h0_0, hice_0, &
               thetai_0, J0, &
               Epot_mhm)

     !************************************
     !    (1) Declaring Variable function
     !************************************
      REAL(dp)                                              :: tfin
      INTEGER(i4)                                           :: irec, mp
      REAL(dp),      DIMENSION(1:mp)                        :: qprec
      INTEGER(i4)                                           :: n
      REAL(dp),      DIMENSION(1:n)                         :: dx
      REAL(dp),      DIMENSION(1:mp)                        :: h0
      REAL(dp),      DIMENSION(1:n)                         :: S
      REAL(dp),      DIMENSION(1:n)                         :: thetai
      REAL(dp),      DIMENSION(1:n)                         :: Jsensible
      REAL(dp),      DIMENSION(1:n)                         :: Tsoil
      REAL(dp),      DIMENSION(1:mp)                        :: evap
      REAL(dp),      DIMENSION(1:mp)                        :: drainage, discharge
      REAL(dp),      DIMENSION(-nsnow_max:n)                :: qh
      INTEGER(i4),   DIMENSION(1:mp)                        :: nsteps
      TYPE(vars_met), DIMENSION(1:mp)                        :: vmet
      TYPE(vars),     DIMENSION(1:n)                        :: var
      REAL(dp),      DIMENSION(1:mp)                        :: T0, Tsurface
      REAL(dp),      DIMENSION(1:n)                         :: phi
      REAL(dp),      DIMENSION(1:mp)                        :: zdelta
      TYPE(params),   DIMENSION(1:n)                        :: par
      REAL(dp),      DIMENSION(-nsnow_max:n)                :: qvsig, qlsig, qvTsig, qvh
      LOGICAL,      DIMENSION(1:mp)                         :: again, init
      REAL(dp),    DIMENSION(1:mp)                          :: t
      REAL(dp),    DIMENSION(-nsnow_max+1:n)                :: aa, bb, cc, dd, ee, ff, gg, dy
      REAL(dp),    DIMENSION(-nsnow_max+1:n)                :: aah, bbh, cch, ddh, eeh, ffh, ggh 
      REAL(dp),    DIMENSION(-nsnow_max+1:n)                :: de
      REAL(dp),    DIMENSION(-nsnow_max:n)                  :: q, qya, qyb, qTa, qTb, qhya, qhyb, qhTa, qhTb, qadv, qadvya, qadvyb
      REAL(dp),    DIMENSION(-nsnow_max:n)                  :: qadvTa, qadvTb
      REAL(dp),    DIMENSION(-nsnow_max:n)                  :: qsig, qhsig, qadvsig
      REAL(dp),    DIMENSION(-nsnow_max:n)                  :: qliq, qv, qvT, qlya, qlyb
      REAL(dp),    DIMENSION(-nsnow_max:n)                  :: qvya, qvyb, qlTb, qvTa, qvTb
      REAL(dp),    DIMENSION(1:n)                           :: dTsoil
      REAL(dp),    DIMENSION(0:n)                           :: tmp2d1, tmp2d2
      REAL(dp),    DIMENSION(1:n)                           :: cv0
      REAL(dp),    DIMENSION(1:n)                           :: dthetaldT, thetal
      INTEGER(i4), DIMENSION(1:n)                            :: isave
      TYPE(vars_aquifer), DIMENSION(1:mp)                    :: v_aquifer
      REAL(dp),          DIMENSION(1:mp)                    :: dJcol_latent_S, dJcol_latent_T, dJcol_sensible
      REAL(dp),          DIMENSION(1:n)                     :: deltaJ_latent_S
      REAL(dp),          DIMENSION(1:n)                     :: deltaJ_latent_T, deltaJ_sensible_S, deltaJ_sensible_T
      REAL(dp),          DIMENSION(1:mp)                    :: tmp1d1, tmp1d2, tmp1d3,  tmp1d4
      REAL(dp),          DIMENSION(1:mp)                    :: deltah0
      
      REAL(dp),          DIMENSION(-nsnow_max+1:n)          :: LHS, RHS, LHS_h, RHS_h
      INTEGER(i4),       DIMENSION(1:mp)                    :: iflux
      INTEGER(i4)                                           :: kk, condition
      INTEGER(i4)                                           :: advection, septs ! switches
      REAL(dp),          DIMENSION(1:mp)                    :: hice, h0_0, hice_0
      REAL(dp),          DIMENSION(1:n)                     :: thetai_0, J0
      REAL(dp),          DIMENSION(1:mp)                    :: Epot_mhm

     
     !------ Internal Variable --------------------------
      REAL(dp),          DIMENSION(nsnow_max)               :: hsnow
      INTEGER(i4)                                           :: i, j
      INTEGER(i4),       DIMENSION(1:mp)                    :: ih0
      REAL(dp),          DIMENSION(1:mp)                    :: cp, cpeff
      REAL(dp),          DIMENSION(1:mp)                    :: dt
      INTEGER(i4),       DIMENSION(1:mp)                    :: ns
      INTEGER(i4),       DIMENSION(1:mp)                    :: nsatlast
      INTEGER(i4),       DIMENSION(1:mp)                    :: nsat
      REAL(dp),          DIMENSION(1:mp)                    :: rsig, rsigdt, sig
      REAL(dp),          DIMENSION(1:mp)                    :: qevap
      REAL(dp)                                              :: theta
      REAL(dp),          DIMENSION(1:mp)                    :: phip
      REAL(dp),          DIMENSION(1:mp)                    :: Tfreezing

      cp(:)    = zero
      cpeff(:) = zero
      hsnow(:) = zero
      nsat(:)  = 0
      theta    = zero
      phip(:)   = zero
      ns(:) = 1
     !*********************************************************
     !    (2) Loop -- while (again(kk))
     !        again: true (default)
     !        again: false in SUBROUTINE get_fluxes_and_derivs
     !*********************************************************
      do while (again(kk)) ! sometimes need twice to adjust phi at satn

         !--------------------------------------------
         !      2.1. Calculate nsat
         !              for detecting saturated layer
         !--------------------------------------------
            nsatlast(kk) = nsat(kk) ! for detecting onset of profile saturation
            nsat(kk)     = sum(var(:)%isat,1) ! no. of sat layers..sum across 1st dimension

         !--------------------------------------------
         !      2.2. Calculate Sigma (time weighting)
         !--------------------------------------------
            sig(kk)      = half !sigma for unsaturated layer = 0.5
            if (nsat(kk) /= 0) sig(kk) = one !sigma for saturated layer = 1
            rsig(kk)     = one/sig(kk)

         !--------------------------------------------
         !      2.3. IF iflux=1, then...
         !              kk: 1:mp (multiple grid = 1)
         !              n : number of soil layers (ms)
         !--------------------------------------------
          if (iflux(kk)==1) then
            !.............................................................
            !          a. Call hyofS: filling var%X (variables of soil)
            !             Calc flux matric potentials (and derivatives) from S
            !.............................................................
             ! this set var-structure
             isave(:) = var(:)%isat

             call hyofS(S(1:n), Tsoil(1:n), par(1:n), var(1:n))
            
            !.............................................................
            !          b. Calculate cp(kk) & cpeff(kk)
            ! cp(kk)    - volumetric heat capacity of air at constant pressure (J m^-3 K^-1)
            ! cpeff(kk) - effective cp(kk) (J m^-3 K^-1)
            !.............................................................
             ! iice = flag, (1) ice (soil freezing), (0) no ice
             ! cswat = specific heat capacity for water (J/kg/K)
             !         it takes 4218 Joules to raise 1 kilogram of water by 1 degree Kelvin or Celsius
             ! csice = specific heat capacity for ice (J/kg/K)
             !         it takes 4218 Joules to raise 1 kilogram of water by 1 degree Kelvin or Celsius
             cp(kk) = real(1-var(1)%iice,dp)*cswat*rhow & 
                     + real(var(1)%iice,dp)*rhow * & 
                     ((one-var(1)%thetai/par(1)%thre)*cswat + (var(1)%thetai/par(1)%thre)*csice )
      
             cpeff(kk) = cp(kk) + rhow*lambdaf*var(1)%dthetaldT/par(1)%thre
            
            !.............................................................
            !          c. Translate theta_i, theta_l, isat, dthetaldT
            !                 h0 = 0 (default)
            !.............................................................          
             var(:)%isat  = isave(:)

             thetai(:)    = var(:)%thetai ! ice profile
             thetal(:)    = var(:)%thetal ! liq water profile
             thetai_0(:)  = thetai(:) ! initial ice profile
             dthetaldT(:) = var(:)%dthetaldT
            
            !.............................................................
            !          d. Calculate pond stuff
            !              phip(kk) - phi of pond
            !              phie -  MFP (matrix flux potential) at he (saturated)
            !              Pond heigh (h0)
            !              Ice height (hice)
            !...........................................................
             phip(kk) = (one+e5)*var(1)%phie !at onset of ponding
             h0_0(kk)   = h0(kk)

             hice(kk)   = h0(kk)*var(1)%thetai/par(1)%thre
             hice_0(kk) = hice(kk)
             
            !.............................................................
            !          e. Calculate Jsensible (sensible heat) of soil & pond
            !                 h0 = 0 (default)
            !                 n: number of soil layers (ms)
            !                 Jsensible is H in the paper
            !............................................................
             ! sensible heat stored in top layer + pond
             Jsensible(1)   = (var(1)%csoil*dx(1) + h0(kk)*cp(kk))*(Tsoil(1))
   
             ! sensible heat stored in soil column (2:n)
             Jsensible(2:n) = var(2:n)%csoil*(Tsoil(2:n))*dx(2:n)
            
            
            !...........................................................
             
          end if ! iflux==1

         !--------------------------------------------
         !      2.5. Call get_fluxes_and_derivs -- set again: FALSE
         !--------------------------------------------
          CALL get_fluxes_and_derivs( &
            irec, mp, qprec, n, dx(:), h0, &
            Tsoil(:), S(:), &
            qh(:), vmet, var, T0, Tsurface, &
            par, &
            qvh(:), &
            qevap, &
            again, init, ns, nsat, &
            nsatlast, &
            phip, &
            q(:), qya(:), qyb(:), qTa(:), qTb(:),qhya(:), qhyb(:), qhTa(:), qhTb(:), qadv(:), qadvya(:), qadvyb(:), &
            qadvTa(:), qadvTb(:), qliq(:), qv(:), qvT(:), qlya(:), qlyb(:), &
            qvya(:), qvyb(:), qlTb(:), qvTa(:), qvTb(:), &
            iflux, kk, &
            advection, hice, tmp1d1, &
            Epot_mhm)
         
         !--------------------------------------------
         !      2.6. Call estimate_timestep
         !--------------------------------------------
          CALL estimate_timestep( &
            irec, tfin, mp, n, dx(:), h0, &
            qh(:), qhTa(:), qadvTa(:), qhTb(:), qadvTb(:), &
            nsteps, var, &
            par, &
            again, nsat, &
            nsatlast, dt, &
            t, &
            q(:), qadv(:), &
            tmp2d1(:), tmp2d2(:), &
            tmp1d1, tmp1d3, &
            iflux, kk, &
            advection)
         
         !--------------------------------------------
         !      2.7. Calcu. rsigdt(kk)
         !--------------------------------------------
            !sig(kk)      - sigma (see 2.2 Calcu sigma )
            !               0.5 for unsaturated layer
            !               1   for saturated layer
            ! dt(kk))     - true time_Step calculated from above (2.6)
            rsigdt(kk) = one/(sig(kk)*dt(kk))
          

         !-------------------------------------------------------------------
         !      2.9. IF uncoupling T and S: Setting parameters 
         !-------------------------------------------------------------------
            ! qTa(:)  - deriv of q wrt T at upper layer
            ! qTb(:)  - deriv of q wrt T at lower layer

            ! qhya(:) - deriv of qh wrt S at upper layer
            ! qhyb(:) - deriv of qh wrt S at lower layer

           

            if (septs == 1) then ! uncoupling of T and S
               qTa(:)   = zero
               qTb(:)   = zero
               qhya(:)  = zero
               qhyb(:)  = zero
            endif
         
         !-------------------------------------------------------------------
         !      2.10. Call get_and_solve_eqn
         !-------------------------------------------------------------------
            CALL get_and_solve_eqn( &
                  irec, mp, qprec,  n, dx(:), h0, S(:), thetai(:), &
                  Tsoil(:), &
                  qh(:), nsteps, var, &
                  par, &
                  again, ns,  &
                  dt,   &
                  phip,  rsig, rsigdt, sig, t,      &
                  aa(:), bb(:), cc(:), dd(:), ee(:), ff(:), gg(:), dy(:), &
                  aah(:), bbh(:), cch(:), ddh(:), eeh(:), ffh(:), ggh(:), &
                  de(:), q(:), qya(:), qyb(:), qTa(:), qTb(:),qhya(:), qhyb(:), qhTa(:), qhTb(:), qadv(:), qadvya(:), qadvyb(:), &
                  qadvTa(:), qadvTb(:),  qsig(:), qhsig(:), qadvsig(:),      &
                  dTsoil(:), tmp2d1(:), &
                  dthetaldT(:), &
                  deltaJ_latent_T(:), deltaJ_sensible_S(:),&
                  tmp1d1, tmp1d2, tmp1d3,  tmp1d4,&
                  Tfreezing, LHS(:), RHS(:), LHS_h(:), RHS_h(:), &
                  iflux, kk, condition, &
                  advection, septs, theta, cp, &
                  cpeff, hice, hsnow(:), &
                  J0(:))

         !-------------------------------------------------------------------
         !      2.11. Call update_unknowns
         !-------------------------------------------------------------------
            CALL update_unknowns ( &
               irec, mp, qprec, n, dx(:), h0, S(:),  &
               Tsoil(:), evap, drainage, discharge, &
               qh(:), nsteps, vmet, var, &
               phi(:),  zdelta, &
               par,      &
               qvsig(:), qlsig(:), qvTsig(:),   &
               qevap,  &
               again, ih0, ns,  &
               dt,    &
               sig, &
               dy(:), &
               de(:), q(:), qya(:), qyb(:), qTa(:), qTb(:),qhya(:), qhyb(:), qhTa(:), qhTb(:),    &
               qsig(:), qhsig(:), qadvsig(:), qliq(:), qv(:), qvT(:),  qlyb(:), &
               qvya(:), qvyb(:), qlTb(:), qvTa(:), qvTb(:), dTsoil(:),    &
               v_aquifer,  &
               dJcol_latent_S, dJcol_latent_T, dJcol_sensible, deltaJ_latent_S(:), &
               deltaJ_latent_T(:), deltaJ_sensible_S(:), deltaJ_sensible_T(:), &
               tmp1d1, tmp1d2, tmp1d3,   deltah0,  &
               LHS_h(:),  &
               kk,    &
               advection, theta, cp, &
               hsnow(:), &
               J0(:))
         
         !-------------------------------------------------------------------
         !      2.12. call update_s_t
         !-------------------------------------------------------------------
            CALL update_s_t( &
               mp, n, dx(:), h0, S(:), thetai(:), &
               Tsoil(:),   &
               nsteps, var,     &
               par, &
               qlsig(:),    &
               again, ih0, ns,  &
               dt,      &
               dy(:),        &
               qhsig(:),       &
               dTsoil(:),    &
               deltaJ_latent_S(:), &
               deltaJ_latent_T(:), deltaJ_sensible_S(:), deltaJ_sensible_T(:),  &
               tmp1d1, tmp1d2, tmp1d3,  tmp1d4, deltah0,    &
               Tfreezing, LHS_h(:),    &
               kk,    &
               theta, &
               hice,   &
               J0(:))

         !-------------------------------------------------------------------
         !      2.13.  if (.not. again(kk)) : call hyofS (update variable of soil)
         !-------------------------------------------------------------------
            if (.not. again(kk)) then
               cv0(1:n)   = var(1:n)%cv  ! save cv before updating
               isave(1:n) = var(1:n)%iice ! save isat before updating

               ! update variables (particularly thetaice)
               call hyofS(S(1:n), Tsoil(1:n), par(1:n), var(1:n))

               var(1:n)%iice = isave(1:n)
            endif ! if .not.again
         
         !-------------------------------------------------------------------
         !      2.14. Update iflux = iflux(kk) + 1
         !-------------------------------------------------------------------
            iflux(kk) = iflux(kk) + 1

      end do ! again

    END SUBROUTINE iflux_loop
    
    SUBROUTINE get_fluxes_and_derivs( &
            irec, mp, qprec, n, dx, h0, &
            Tsoil, S, &
            qh, vmet, var, T0, Tsurface, &
            par, &
            qvh, &
            qevap, &
            again, init, ns, nsat, &
            nsatlast, &
            phip, &
            q, qya, qyb, qTa, qTb,qhya, qhyb, qhTa, qhTb, qadv, qadvya, qadvyb, &
            qadvTa, qadvTb, qliq, qv, qvT, qlya, qlyb, &
            qvya, qvyb, qlTb, qvTa, qvTb, &
            iflux, kk, &
            advection, hice, tmp1d1, &
            Epot_mhm)

     !***************************************************
     !     (1) Declaring Variables Functions
     !**************************************************
      IMPLICIT NONE

      INTEGER(i4)                                           :: irec, mp
      REAL(dp),      DIMENSION(1:mp)                        :: qprec
      INTEGER(i4)                                           :: n
      REAL(dp),      DIMENSION(1:n)                         :: dx
      REAL(dp),      DIMENSION(1:mp)                        :: h0
      REAL(dp),      DIMENSION(1:n)                         :: Tsoil, S
      REAL(dp),      DIMENSION(-nsnow_max:n)                :: qh
      TYPE(vars_met), DIMENSION(1:mp)                        :: vmet
      TYPE(vars),     DIMENSION(1:n)                         :: var
      REAL(dp),      DIMENSION(1:mp)                        :: T0, Tsurface
      TYPE(params),   DIMENSION(1:n)                         :: par
      REAL(dp),      DIMENSION(-nsnow_max:n)                :: qvh
      REAL(dp),    DIMENSION(1:mp)                          :: qevap
      LOGICAL,      DIMENSION(1:mp)                          :: again, init
      INTEGER(i4), DIMENSION(1:mp)                          :: ns, nsat, nsatlast
      REAL(dp),    DIMENSION(1:mp)                          :: phip
      REAL(dp),    DIMENSION(-nsnow_max:n)                  :: q, qya, qyb, qTa, qTb,qhya, qhyb, qhTa, qhTb
      REAL(dp),    DIMENSION(-nsnow_max:n)                  :: qadv, qadvya, qadvyb, qadvTa, qadvTb
      REAL(dp),    DIMENSION(-nsnow_max:n)                  :: qliq, qv, qvT, qlya, qlyb, qvya, qvyb, qlTb, qvTa, qvTb
      INTEGER(i4),       DIMENSION(1:mp)                    :: iflux
      INTEGER(i4)                                           :: j, kk
      INTEGER(i4)                                           :: advection ! switches
      REAL(dp),          DIMENSION(1:mp)                    :: hice
      REAL(dp),          DIMENSION(1:mp)                    :: tmp1d1
      REAL(dp),          DIMENSION(1:mp)                    :: Epot_mhm

      !....................................................
      ! INTERNAL VARIABLES
      !....................................................
      REAL(dp),          DIMENSION(1:mp)                    :: lE0, G0, Epot
      REAL(dp)                                              :: Tqw
      REAL(dp),          DIMENSION(1:mp, 1:n)               :: hint
      REAL(dp),          DIMENSION(1:n)                     :: hint_tmp
      REAL(dp),          DIMENSION(1:mp, 1:n)               :: phimin
      REAL(dp),          DIMENSION(1:n)                     :: phimin_tmp
      REAL(dp),          DIMENSION(1:n)                     :: Sbot, Tbot
      TYPE(vars),        DIMENSION(1:n)                     :: vcall
      TYPE(vars)                                             :: vtmp
      TYPE(vars),         DIMENSION(1:mp)                    :: vtop, vbot
      LOGICAL,           DIMENSION(1:mp)                    :: getq0, getqn

      Tqw          = zero
      hint(:,:)    = zero
      hint_tmp(:)  = zero
      phimin(:,:)  = zero
      phimin_tmp(:)  = zero



     !***************************************************
     !     (2) Set Boundary Conditions
     !**************************************************
      ! Set get surface/bottom flux: getq0, getqn ------------
      getq0(:) = .false.
      getqn(:) = .false.

      !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      !>> botbc=="seepage"
      !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
         if (botbc=="seepage") then
            vtmp = zerovars()
            vbot = spread(vtmp,1,mp)

            if (var(n)%h > -half*gf*dx(n)) then
               getqn(kk)        = .true. ! get bottom flux if required
               vbot(kk)%isat    = 1
               vbot(kk)%phi     = (zero-var(n)%he)*var(n)%Ksat + var(n)%phie ! (special for frozen soil)
               vbot(kk)%K       = var(n)%Ksat
               vbot(kk)%rh      = one
               vbot(kk)%lambdav = rlambda
               vbot(kk)%lambdaf = lambdaf

            else
               getqn(kk) = .false.
            endif
         end if

      !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      !>> botbc=="Constant Head"
      !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

         if (botbc == "constant head") then ! h at bottom bdry specified
            
            getqn(:)  = .true.
            tmp1d1(:)  = hbot
            
            !>> Bottom layer UNSATURATED
            ! for hbot < he
            Sbot(:) = spread(Sofh(tmp1d1(kk), par(n)), 1, n)
            Tbot(:) = spread(Tsoil(n), 1, n)
            call hyofS(Sbot(:), Tbot(:), par(:), vcall(:))

            !>> Bottom layer SATURATED
            ! for hbot >= he
            vtmp = zerovars()
            vtmp%isat    = 1
            vtmp%h       = hbot
            vtmp%rh      = one
            vtmp%lambdav = rlambda
            vtmp%lambdaf = lambdaf

            vbot = spread(vtmp,1,mp)
            vbot(kk)%phi = (hbot-par(n)%he)*par(n)%Ke + var(n)%phie
            vbot(kk)%K   = par(n)%Ke
            
            !>> Bottom layer UNSATURATED
            if (par(n)%he > hbot) then 
               vbot(kk)      = vcall(n)
               vbot(kk)%isat = 0
            end if
   
         end if

      !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      !>> botbc=="groundwater"
      !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
         if (botbc == "groundwater") then
            getqn(:)  = .true.
            if (.not. allocated(hbot2)) allocate(hbot2(mp))
            hbot2(kk) = L1_gwdepth(kk)*(-10._dp)
            ! print *, "hbot2 = ", hbot2(kk)
            ! tmp1d1(:)  = hbot
            if(hbot2(kk)>(sum(dx)*(-1))) hbot2(kk) = 0._dp

            if(hbot2(kk) < par(n)%he) then !unsaturated

               if(hbot2(kk)>=0) then
                  Sbot = 1._dp
               else
                  Sbot(:) = spread(Sofh(hbot2(kk), par(n)), 1, n)
               end if
   
               Tbot(:) = spread(Tsoil(n), 1, n)
               call hyofS(Sbot(:), Tbot(:), par(:), vcall(:))


               vtmp = zerovars()
               vtmp%h       = hbot2(kk) 
               vtmp%rh      = one
               vtmp%lambdav = rlambda
               vtmp%lambdaf = lambdaf
               vtmp%macropore_factor = one
               
               vbot = spread(vtmp,1,mp)
               vbot(kk) = vcall(n)
               vbot(kk)%isat = 0

            else ! saturated
                !>> set vtmp
               vtmp = zerovars()
               vtmp%isat = 1
               vtmp%h       = hbot2(kk) 
               vtmp%rh      = one
               vtmp%lambdav = rlambda
               vtmp%lambdaf = lambdaf
               vtmp%macropore_factor = one

               vtmp%K = par(n)%Ke
               vtmp%phi = par(n)%phie + (vtmp%h-par(n)%he)*par(n)%Ke

               vbot = spread(vtmp,1,mp)
            end if
         end if
         
      !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      !>> TOP BC pond is exist
      ! get surface condition
      !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
         ! ns==1 if no pond or full pond, i.e. do not solve for change in pond height
         ! ns==0 then change for pond height

         ! zero var-structure that contains all the hydrological variables
         vtmp = zerovars()
         vtmp%h       = one
         vtmp%lambdav = rlambda
         vtmp%lambdaf = lambdaf
         ! zero vars at the bottom and top of the soil column
         vtop = spread(vtmp,1,mp)

         ! no ponding
         if ((var(1)%phi <= phip(kk) .and. h0(kk) <= zero .and. nsat(kk) < n) .or. (var(1)%isat==0)) then 
            ns(kk)    = 1 ! start index for eqns
         ! ponding
         else 
            ! print *, "1st Layer is saturated! S = ",S(1)
            ns(kk)    = 0 ! start index for eqns
            
            var(1)%phi = (one+e5)*var(1)%phie+(h0(kk)-hice(kk))*var(1)%Ksat

            vtop(kk)%isat    = 1
            vtop(kk)%h       = h0(kk)
            vtop(kk)%phi     = max((var(1)%phie -var(1)%he*var(1)%Ksat), &
                  (one+e5)*var(1)%phie)+(h0(kk)-hice(kk))*var(1)%Ksat
            vtop(kk)%K       = var(1)%Ksat
            vtop(kk)%rh      = one
            vtop(kk)%lambdav = rlambda
            vtop(kk)%lambdaf = lambdaf
            
            ! calculates phi1,eff (pond + top soil layer)   !!vh!! does this get used???
            call flux(par(1), vtop(kk), var(1), half*dx(1), &
                      q(0), qya(0), qyb(0), qTa(0), qTb(0))
         endif

         !>> Erase ponding influence (at the moment)
         ! ns(kk) = 1 

     !***************************************************
     !     (3) Calling SEB (Surface Energy Balance)
     !***************************************************
      !>> no snow case     
      CALL SEB(n, par(:), vmet(kk), var(:), qprec(kk), dx(:), &
            h0(kk), Tsoil(:), irec, Epot_mhm(kk), &
            !>> Intent OUT ..................................................
            Tsurface(kk), & ! surface temperature (soil/pond/litter)
            G0(kk), & ! Belinda's stomatal model intercept
            lE0(kk), &  ! latent heat flux at current time step, assuming Tg of previous time-step  [W m-2]
            Epot(kk),  & ! potential evaporation
            q(0), &  ! qsurface   -  water flux into surface
            qevap(kk), & ! flux evaporative water flux from soil surface
            qliq(0), & ! liquid components of water flux from surface into soil
            qv(0), & ! vapor components of water flux from surface into soil
            qh(0), & ! components of heat flux into surface
            qadv(0), & ! advective components of heat flux into surface
            !>> Derivative
            qyb(0), & ! derivative of q(i) wrt SM(i) at below layer
            qTb(0), & ! derivative of q(i) wrt Temp(i) at below layer
            qlyb(0), & ! derivative of q_liquid(i) wrt SM(i) at below layer
            qvyb(0), & ! derivative of q_vapor(i) wrt SM(i) at below layer
            qlTb(0), & ! derivative of q_liquid(i) wrt Temp(i) at below layer
            qvTb(0), & ! derivative of q_vapor(i) wrt Temp(i) at below layer
            qhyb(0), & ! derivative of q_heat(i) wrt SM(i) at below layer
            qhTb(0), & ! derivative of q_heat(i) wrt TEMP(i) at below layer
            qadvyb(0), & ! derivative of q_heat_advective(i) wrt SM(i) at below layer
            qadvTb(0)) ! derivative of q_heat_advective(i) wrt Temp(i) at below layer
      
      !>> Set Derivative of surface fluxes upper layers (a) to zero
      qya(0)    = zero
      qTa(0)    = zero
      qlya(0)   = zero
      qvya(0)   = zero
      qvTa(0)   = zero
      qhya(0)   = zero
      qhTa(0)   = zero
      qadvya(0) = zero
      qadvTa(0) = zero
  

     !***************************************************
     !     (4) Call getfluxes_vp: 
     !        get moisture fluxes and derivatives (at time t=0, i.e. q0 etc.)
     !***************************************************
      call getfluxes_vp( & 
                        n, & !  number of soil layers (ms variable)
                        dx(1:n), & ! soil layer thickness (m)
                        vtop(kk), & ! var at soil surface.
                        vbot(kk), & ! var at bottom of profile.
                        par(1:n), & ! soil parameters
                        var(1:n), & ! var at middle point of soil layers
                        hint_tmp(1:n), & ! h_interface, values of h at interfaces are stored sequentially in hint.
                        phimin_tmp(1:n), & ! matric flux potential (MFP) at min value,  similarly for phi at hmin in layers above interfaces.
                        q(0:n), & ! flux of water (liquid + vapor)
                        qya(0:n), & ! deriv of q wrt S at upper layer
                        qyb(0:n), & ! deriv of q wrt S at lower layer
                        qTa(0:n), & ! deriv of q wrt T at upper layer
                        qTb(0:n), & ! deriv of q wrt T at lower layer
                        qliq(0:n), & ! flux of liquid-water
                        qlya(0:n), & ! deriv of qliq wrt S at upper layer
                        qlyb(0:n), & ! deriv of qliq wrt S at lower layer
                        qv(0:n), & ! flux of vapor-water
                        qvT(0:n), & ! flux qv in temperature term
                        qvh(0:n), & ! flux qv in pressure head term
                        qvya(0:n), & ! deriv of qv wrt S at upper layer
                        qvyb(0:n), & ! deriv of qv wrt S at lower layer
                        iflux(kk), & ! FLAG - if iflux/=1, get only fluxes involving sat layers.
                        init(kk), & ! FLAG - true if hint and phimin to be initialised.
                        getq0(kk), & ! FLAG - true if q(0) required
                        getqn(kk), & ! FLAG - true if q(n) required.
                        Tsoil(1:n), & ! Soil layers temperature
                        Tsurface(kk), & ! surface soil/pond/litter temperature
                        nsat(kk), & ! number of sat layers
                        nsatlast(kk)) ! old nsat, for detecting onset of profile saturation

      hint(kk,:)   = hint_tmp
      phimin(kk,:) = phimin_tmp

      !>> set bttom fluxes (n) to zero
      qTa(n) = zero ! deriv of q wrt T at upper layer
      qTb(n) = zero ! deriv of q wrt T at lower layer

      !>> Calculate qvTa, qvTb, qlTb for all layers
      qvTa(1:n) = qTa(1:n) ! deriv of qv wrt T at upper layer
      qvTb(1:n) = qTb(1:n) ! deriv of qv wrt T at lower layer
      qlTb(1:n) = zero     ! deriv of qliq wrt T at lower layer

     !***************************************************
     !     (5) call getheatfluxes: 
     !          get  fluxes heat and derivatives (at time t=0, i.e. q0 etc.)
     !***************************************************
      ! n        - number of soil layers (ms)
      ! dx(1:n)  - soil layer thickness (no layer 0)
      ! dxL(kk)  - unused
      ! qh(0:n)  - heat flux
      ! qhya(0:n) - derivative qh wrt S at upper layer
      ! qhyb(0:n)
      call getheatfluxes(n, dx(1:n), &
                        qh(0:n), qhya(0:n), qhyb(0:n), qhTa(0:n), qhTb(0:n), &
                        var(1:n), Tsoil(1:n), &
                        q(0:n), qya(0:n), qyb(0:n), qTa(0:n), qTb(0:n), &
                        qadv(0:n),qadvya(0:n), qadvyb(0:n), qadvTa(0:n), qadvTb(0:n), &
                        advection) ! heat fluxes
     
     !***************************************************
     !     (6) If Pond exist: pond included in top soil layer
     !***************************************************
      if (ns(kk)==0) then ! 
         ! change qya(1) from dq/dphi (returned by getfluxes) to dq/dh
         qya(1) = var(1)%Ksat*qya(1)
         if (advection==1) then
            qhya(1) = qhya(1) - qadvya(1)
            Tqw  = merge(Tsoil(1), Tsoil(2), q(1)>zero)
            qadvya(1) =  rhow*cswat*qya(1)*Tqw  ! apply corrected qya(1) to qadvya(1)
            qhya(1) = qhya(1) + qadvya(1)
         endif
      endif


     !***************************************************
     !     (7) Calculate flux (moisture) at bottom soil column
     !***************************************************
      ! specify mositure flux at bottom of soil column (heat flux set to zero)
      if (botbc /= "constant head") then
         
         select case (botbc)
         case ("zero flux")
            qliq(n) = zero
            qv(n)   = zero
            q(n)    = zero
            qya(n)  = zero
            qlya(n) = zero
            qvya(n) = zero
         
         case ("free drainage")
            q(n) = gf*var(n)%K

            if (var(n)%isat == 0) then
               qya(n) = gf*var(n)%KS
            else
               qya(n) = zero
            end if
         
         case ("seepage")
            if (var(n)%h <= -half*gf*dx(n)) then
               q(n)   = zero
               qya(n) = zero
            end if
         
         case default
            ! write(*,*) "solve: illegal bottom boundary condition."
            ! stop
            continue
         end select

      end if

     !***************************************************
     !     (8) Adjust lower heat flux for advection
     !***************************************************
      if (advection==1) then
         qadv(n) = rhow*cswat*(Tsoil(n))*q(n)
         qadvya(n) = rhow*cswat*(Tsoil(n))*qya(n)
         qadvTa(n)= rhow*cswat*q(n)
         qh(n) = qh(n) + qadv(n)
         qhya(n) = qhya(n) + qadvya(n)
         qhTa(n) = qhTa(n) + qadvTa(n)
      else
         qadv(:)   = zero
         qadvya(:) = zero
         qadvyb(:) = zero
         qadvTa(:) = zero
         qadvTb(:) = zero
      endif

 

     !***************************************************
     !     (9) Set again = False
     !***************************************************
      again(kk)  = .false. ! flag for recalcn of fluxes (default=false)
      !----- end get fluxes and derivs
    END SUBROUTINE get_fluxes_and_derivs

    SUBROUTINE estimate_timestep( &
      irec, tfin, mp, n, dx, h0, &
      qh, qhTa, qadvTa, qhTb, qadvTb, &
      nsteps, var, &
      par, &
      again, nsat, &
      nsatlast, dt, &
      t, &
      q, qadv, &
      tmp2d1, tmp2d2, &
      tmp1d1, tmp1d3, &
      iflux, kk, &
      advection)

      !******************************************************
      !     (1) Declaring Variable 
      !******************************************************
         IMPLICIT NONE
         REAL(dp)                                              :: tfin
         INTEGER(i4)                                           :: mp, irec
         INTEGER(i4)                                           :: n
         REAL(dp),      DIMENSION(1:n)                         :: dx
         REAL(dp),      DIMENSION(1:mp)                        :: h0
         REAL(dp),      DIMENSION(-nsnow_max:n) :: qh
         INTEGER(i4),   DIMENSION(1:mp)                        :: nsteps
         TYPE(vars),     DIMENSION(1:n) :: var
         TYPE(params),   DIMENSION(1:n) :: par
         LOGICAL,      DIMENSION(1:mp)                          :: again
         INTEGER(i4), DIMENSION(1:mp)                          :: nsat, nsatlast
         REAL(dp),    DIMENSION(1:mp)                          :: dt
         REAL(dp),    DIMENSION(1:mp)                          :: t
         REAL(dp),    DIMENSION(-nsnow_max:n) :: q, qhTa, qhTb
         REAL(dp),    DIMENSION(-nsnow_max:n) :: qadv, qadvTa, qadvTb
         REAL(dp),    DIMENSION(0:n) :: tmp2d1, tmp2d2,  deltaTmax
         REAL(dp),          DIMENSION(1:mp)                    :: tmp1d1, tmp1d3
         INTEGER(i4),       DIMENSION(1:mp)                    :: iflux
         INTEGER(i4)                                           :: kk
         INTEGER(i4)                                           :: advection ! switches

         !.......................................
         ! INTERNAL VARIABLES
         !......................................
         REAL(dp),    DIMENSION(1:mp)                          :: dmax

      !******************************************************
      !     (2) Initiate Zero: dmax(kk), tmp2d1(:),  tmp2d2(:) = zero
      !******************************************************
        
         !----- first estimate of time step dt before the calculation
         !      gets revised after the calculation
         dmax(kk)    = zero
         
         tmp2d1(:)   = zero
         tmp2d1(0)   = zero ! air compartment --> 0

         tmp2d2(:)   = zero !  temp storage
         tmp2d2(0)   = zero ! air compartment --> 0

      !******************************************************
      !     (3) Calcu. tmp2d1(1:n)
      !         estimate rate of change of moisture storage [m/s]
      !******************************************************
         where (var(1:n)%isat==0.and.var(1:n)%iice==0.) &
               tmp2d1(1:n) = abs(q(1:n)-q(0:n-1))/(par(1:n)%thre*dx(1:n))

         where (var(1:n)%iice==1) &
               tmp2d1(1:n) =  tmp2d1(1:n)/2._dp

      !******************************************************
      !     (4) Calcu. tmp2d2(1:n)
      !         estimate rate of change of heat storage [K/s]
      !******************************************************
         tmp2d2(1:n) = abs(qh(1:n)-qh(0:n-1))/(var(1:n)%csoileff*dx(1:n))
      
      !******************************************************
      !     (5) IF(advection==1) - Calcu.  tmp2d2(1:n), deltaTmax(1:n)
      !         estimate rate of change of heat storage [K/s]
      !******************************************************
         if (advection==1) then
            ! first order estimate of rate of temperature change
            tmp2d2(1:n) = abs((qh(1:n)-qadv(1:n))-(qh(0:n-1)-qadv(0:n-1)))/(var(1:n)%csoileff*dx(1:n))
            deltaTmax(1:n) = tmp2d2(1:n)*(tfin-t(kk)) ! maximum  T change
         end if
      
      !******************************************************
      !     (6) Calcu. tmp1d3(kk)
      !******************************************************
         
         tmp1d3(kk)  = dtmax
      
      !******************************************************
      !     (7) Calcu. dmax(kk)
      !         max derivative |dS/dt|
      !******************************************************
         dmax(kk) = maxval(tmp2d1(1:n),1) ! max derivative |dS/dt|

         if (abs(minval(tmp2d1(:),1)) > maxval(tmp2d1(:),1)) then
            dmax(kk) = abs(minval(tmp2d1(1:n),1))
            ! write(*,*) 'Should not be here (01)'
            ! stop
         endif

      !******************************************************
      !     (8) Calcu. tmp1d1(kk)
      !         max derivative |dTsoil/dt|
      !******************************************************
         tmp1d1(kk) = maxval(tmp2d2(1:n),1) ! max derivative |dTsoil/dt|
      
      !******************************************************
      !     (9) Calcu. dt_v1 - if (dmax(kk) > zero)
      !         transient
      !******************************************************
         if (dmax(kk) > zero) then
            ! constrained either by moisture or temp
            if(tmp1d1(kk)==zero) then
               dt(kk) = min(dSmax/dmax(kk),  86400._dp/nTstepDay, tmp1d3(kk))
            else
               dt(kk) = min(dSmax/dmax(kk), dTLmax/tmp1d1(kk), tmp1d3(kk)) 
            end if

         
      !******************************************************
      !     (10) Calcu. dt_v2 - steady state
      !         
      !******************************************************
         else ! steady state flow
            if (q(0)>=q(n)) then ! if saturated soil columnn and more precipitation then drainige -> finish
               dt(kk) = tfin-t(kk) ! step to finish
            else ! otherwise adjust dt because change of pond height
               dt(kk) = -(h0(kk)-half*h0min)/(q(0)-q(n))
            end if
            if(tmp1d1(kk)==zero) then
               dt(kk) = min(dt(kk), 86400._dp/nTstepDay, tmp1d3(kk)) ! constrained by  temp
            else
               dt(kk) = min(dt(kk), dTLmax/tmp1d1(kk), tmp1d3(kk)) ! constrained by  temp
            end if
         end if
      
      !******************************************************
      !     (12) Calcu. dt_v4 - dtmax consideration
      !         
      !******************************************************
         ! if (dt(kk)>dtmax) dt(kk) = dtmax ! user's limit
         if (dt(kk)>dtmax) dt(kk) = 86400._dp/nTstepDay ! user's limit
         if (dt(kk)<0) dt(kk) = dtmin ! user's limit
      !******************************************************
      !     (13) Calcu. dt_v5 - dtmin & saturation consideration
      !         
      !******************************************************
         ! if initial step, improve phi where S>=1
         ! might be that you get better derivatives, especially at sat/non-sat interfaces
         if (nsteps(kk)==0 .and. nsat(kk)>0 .and. iflux(kk)==1) then
            again(kk) = .true.
            dt(kk)    = dtmin
         end if

      !******************************************************
      !     (14) Calcu. dt_v6 - dtmin & saturation consideration
      !         
      !******************************************************
         ! if fully saturated but was not fully saturated before, adjust even within each time step iteration
         if (nsat(kk)==n .and. nsatlast(kk)<n .and. iflux(kk)==1) then
            again(kk) = .true. ! profile has just become saturated so adjust phi values
            dt(kk)    = dtmin
         end if
         
      !******************************************************
      !     (15) t(kk) Updating or Ending (t = tfin)
      !         
      !!******************************************************
         ! sprint to the end
         if (t(kk)+1.1_dp*dt(kk) > tfin) then ! step to finish
            dt(kk) = tfin-t(kk)
            t(kk)  = tfin
         else
            t(kk) = t(kk)+dt(kk) ! tentative update
            if (again(kk)) t(kk) = t(kk)-dt(kk)
         end if

         if (abs(minval(tmp2d1(:),1)) > maxval(tmp2d1(:),1)) then
            dt(kk) = tfin-t(kk) ! step to finish
            again(kk) = .false.
            t(kk)  = tfin
         end if

         !----- end estimate time step dt


    END SUBROUTINE estimate_timestep

    SUBROUTINE get_and_solve_eqn( &
         irec, mp, qprec,  n, dx, h0, S, thetai, &
         Tsoil, &
         qh, nsteps, var, &
         par, &
         again, ns,  &
         dt,   &
         phip,  rsig, rsigdt, sig, t,      &
         aa, bb, cc, dd, ee, ff, gg, dy, &
         aah, bbh, cch, ddh, eeh, ffh, ggh, &
         de, q, qya, qyb, qTa, qTb,qhya, qhyb, qhTa, qhTb, qadv, qadvya, qadvyb, &
         qadvTa, qadvTb,  qsig, qhsig, qadvsig,      &
         dTsoil, tmp2d1, &
         dthetaldT, &
         deltaJ_latent_T, deltaJ_sensible_S,&
         tmp1d1, tmp1d2, tmp1d3,  tmp1d4,&
         Tfreezing, LHS, RHS, LHS_h, RHS_h, &
         iflux, kk, condition, &
         advection, septs, theta, cp, &
         cpeff, hice, hsnow, &
         J0)
      
      !*********************************************
      !     (1) Declaring variables
      !*********************************************
         IMPLICIT NONE
         INTEGER(i4)                                           :: irec, mp
         REAL(dp),      DIMENSION(1:mp)                        :: qprec
         INTEGER(i4)                                           :: n
         REAL(dp),      DIMENSION(1:n)                         :: dx
         REAL(dp),      DIMENSION(1:mp)                        :: h0
         REAL(dp),      DIMENSION(1:n)                         :: S
         REAL(dp),      DIMENSION(1:n)                         :: thetai
         REAL(dp),      DIMENSION(1:n)                         :: Tsoil
         REAL(dp),      DIMENSION(-nsnow_max:n)                :: qh
         INTEGER(i4),   DIMENSION(1:mp)                        :: nsteps
         TYPE(vars),     DIMENSION(1:n)                        :: var
         TYPE(params),   DIMENSION(1:n)                        :: par
         LOGICAL,      DIMENSION(1:mp)                         :: again
         INTEGER(i4), DIMENSION(1:mp)                          :: ns
         REAL(dp),    DIMENSION(1:mp)                          :: dt, phip
         REAL(dp),    DIMENSION(1:mp)                          :: rsig, rsigdt, sig, t
         REAL(dp),    DIMENSION(-nsnow_max+1:n)                :: aa, bb, cc, dd, ee, ff, gg, dy
         REAL(dp),    DIMENSION(-nsnow_max+1:n)                :: aah, bbh, cch, ddh, eeh, ffh, ggh, de
         REAL(dp),    DIMENSION(-nsnow_max:n)                  :: q, qya, qyb, qTa, qTb,qhya, qhyb, qhTa, qhTb
         REAL(dp),    DIMENSION(-nsnow_max:n)                  :: qadv, qadvya, qadvyb, qadvTa, qadvTb
         REAL(dp),    DIMENSION(-nsnow_max:n)                  :: qsig, qhsig, qadvsig
         REAL(dp),    DIMENSION(1:n)                           :: dTsoil
         REAL(dp),    DIMENSION(0:n)                           :: tmp2d1
         REAL(dp),    DIMENSION(1:n)                           :: dthetaldT
         REAL(dp),          DIMENSION(1:n)                     :: deltaJ_latent_T, deltaJ_sensible_S
         REAL(dp),          DIMENSION(1:mp)                    :: tmp1d1, tmp1d2, tmp1d3,  tmp1d4
         REAL(dp),          DIMENSION(1:mp)                    :: G0
         REAL(dp),          DIMENSION(1:mp)                    :: Tfreezing
         REAL(dp),          DIMENSION(-nsnow_max+1:n)          :: LHS, RHS, LHS_h, RHS_h
         INTEGER(i4),       DIMENSION(1:mp)                    :: iflux
         INTEGER(i4)                                           :: kk, condition
         INTEGER(i4)                                           :: advection, septs ! switches
         REAL(dp)                                              :: theta
         REAL(dp),          DIMENSION(1:mp)                    :: cp, cpeff, hice
         REAL(dp),          DIMENSION(nsnow_max)               :: hsnow
         REAL(dp),      DIMENSION(1:n)                         :: J0
         
      !............................................................................
      ! INTERNAL VARIABLES
      !............................................................................
         INTEGER(i4),       DIMENSION(1:mp)                    :: nfac1, nfac2, nfac3, nfac4, nfac5, &
                                                                  nfac6, nfac7, nfac8, nfac9, nfac10, nfac11, nfac12
         INTEGER(i4)                                           :: i
         INTEGER(i4),       DIMENSION(1:mp)                    :: nns
         INTEGER(i4), DIMENSION(1:mp)                          :: iok
         INTEGER(i4), DIMENSION(1:mp)                          :: itmp
         REAL(dp)                                              :: c2
         REAL(dp),          DIMENSION(1:mp)                    :: h0_tmp
         REAL(dp),    DIMENSION(1:mp)                          :: accel
         REAL(dp),    DIMENSION(1:mp)                          :: fac
         
      !-----------------------------------------------------------------------------
      ! Initialise nfac
      !-----------------------------------------------------------------------------
         c2       = zero
         h0_tmp   = zero
         
         nfac1  = 0
         nfac2  = 0
         nfac3  = 0
         nfac4  = 0
         nfac5  = 0
         nfac6  = 0
         nfac7  = 0
         nfac8  = 0
         nfac9  = 0
         nfac10 = 0
         nfac11 = 0
         nfac12 = 0

      !*********************************************
      !     (2) Set flag and init: iok, itmp, h0_tmp
      !*********************************************
         iok(kk)            = 0 ! flag for looping -- time step test
         itmp(kk)           = 0 ! counter to abort if not getting solution
         h0_tmp(kk)         = h0(kk) !temporal pond height

      !*********************************************
      !     (3) Loop while (iok(kk)==0)
      !*********************************************
         do while (iok(kk)==0) ! keep reducing time step until all ok
            itmp(kk)  = itmp(kk) + 1

            !----------------------------------------------------------------
            !           3.1. CALCU. accel(kk): acceleration of timesteps (0.5 to 1)
            !----------------------------------------------------------------
               accel(kk) = one - 0.05_dp*real(min(10, max(0,itmp(kk)-4)), dp) ! acceleration [0.5,1], start with 1

            !----------------------------------------------------------------
            !           3.2. STOP SOLVER if (itmp(kk) > 1000)
            !----------------------------------------------------------------
               if (itmp(kk) > 1000) then
                  write(*,*) "solve: too many iterations of equation solution"
                  write(*,*) " irec, kk, S"
                  write(*,*) irec, kk, S(:)
                  write(*,*) " irec, kk, Tsoil"
                  write(*,*) irec, kk, Tsoil(:)
                  write(*,*) " irec, kk"
                  write(*,*) irec, kk
                  write(*,*) " irec, kk, h0"
                  write(*,*) irec, kk, h0(kk)
                  write(*,*) nfac1(kk), nfac2(kk), nfac3(kk), nfac4(kk), nfac5(kk), &
                     nfac6(kk), nfac7(kk), nfac8(kk), nfac9(kk), nfac10(kk), nfac11(kk), nfac12(kk)
                  stop
               end if
         
            !----------------------------------------------------------------
            !           3.4. Set Matrix Coefficient
            !----------------------------------------------------------------
               !...............................................................
               !                  a. aa bb ff gg: water & heat
               !...............................................................
               ! Water
               aa(1:n)   =  qya(0:n-1) ! dqw(j-1)/dS(j-1)
               ee(0:n-1) = -qyb(0:n-1) ! -dqw(j)/dS(j+1)
               bb(1:n)   =  qTa(0:n-1) ! dqw(j-1)/dS(T-1)
               ff(0:n-1) = -qTb(0:n-1) ! -dqw(j)/dT(j+1)

               gg(1:n) = -(q(0:n-1)-q(1:n))*rsig(kk) ! -( qw(j-1) - qw(j) )*rsig + qex(j)*dX(j)*rsig
               
               ! Heat
               aah(1:n)   =  qhya(0:n-1) ! dqh(j-1)/dS(j-1)
               eeh(0:n-1) = -qhyb(0:n-1) ! -dqh(j)/dS(j+1)
               bbh(1:n)   =  qhTa(0:n-1) ! dqh(j-1)/dS(T-1)
               ffh(0:n-1) = -qhTb(0:n-1) ! -dqh(j)/dT(j+1)
               ggh(1:n) =  -(qh(0:n-1)-qh(1:n))*rsig(kk) ! -( qh(j-1) - qh(j)) * rsig
               
               ! Surface air
               aa(0)  = qya(0)
               aah(0) = zero
               bbh(0) = zero

               !...............................................................
               !                  d. cc water: no/pond
               !...............................................................
               where (var(1:n)%isat==0) ! unsaturated layers
                  cc(1:n) = qyb(0:n-1) - qya(1:n) - & 
                           dx(1:n)*par(1:n)%thre * rsigdt(kk)
               elsewhere ! saturated layers
                  cc(1:n) = qyb(0:n-1) - qya(1:n)
               endwhere

               if (ns(kk)<1) then ! pond included in top soil layer, solving for change in pond height
                  cc(1) = -qya(1)-rsigdt(kk)
               endif

               !...............................................................
               !                  e. cch heat: no/pond, advection
               !...............................................................
               cch(1:n) = qhyb(0:n-1)-qhya(1:n) +   & ! + ice parameters if exist iice = 0 or 1
                     real(var(1:n)%iice,dp)*real(1-var(1:n)%isat,dp)*rhow*lambdaf*par(1:n)%thre*dx(1:n)*rsigdt(kk)

               if (ns(kk)==0) then ! change in pond height (top layer saturated)
                  cch(1) = cch(1) +  &
                        real(var(1)%iice,dp)*rhow*lambdaf*rsigdt(kk)*(var(1)%thetai/par(1)%thre)
               endif

               ! modification to cch for advection
               if (advection==1) then
                  if (ns(kk)==0) then  ! changing pond included in top soil layer
                     cch(1) = cch(1) -rhow*cswat*(Tsoil(1))*rsigdt(kk)*real(1-var(1)%iice,dp) &
                           -rhow*csice*(Tsoil(1))*rsigdt(kk)*real(var(1)%iice,dp) &
                           *(var(1)%thetai/par(1)%thre) &
                           -rhow*cswat*(Tsoil(1))*rsigdt(kk)*real(var(1)%iice,dp)* &
                           (one-(var(1)%thetai/par(1)%thre))
                  else ! no pond
                     cch(1) = cch(1) -rhow*(Tsoil(1))*par(1)%thre*dx(1)*rsigdt(kk)* &
                           real(1-var(1)%isat,dp)*(cswat*real(1-var(1)%iice,dp) &
                           +csice*real(var(1)%iice,dp))
                  endif

                  cch(2:n) = cch(2:n) -rhow*(Tsoil(2:n))*par(2:n)%thre*dx(2:n)*rsigdt(kk)* &
                        real(1-var(2:n)%isat,dp)*(cswat*real(1-var(2:n)%iice,dp) &
                        +csice*real(var(2:n)%iice,dp))
               endif

               !...............................................................
               !                  f. dd water
               !...............................................................
               dd(1:n)  = qTb(0:n-1)-qTa(1:n)
            
               !...............................................................
               !                  g. ddh heat: no/pond, advection
               !...............................................................
               ddh(1:n) = qhTb(0:n-1)-qhTa(1:n) - &
                     ! Only apply latent heat component of heat capacity to total deltaT if soil remains frozen
                     var(1:n)%csoileff*dx(1:n)*rsigdt(kk)- &
                     (cswat-csice)*dx(1:n)*var(1:n)%dthetaldt*rhow*(Tsoil(1:n))* &
                     rsigdt(kk)*real(var(1:n)%iice,dp)

               ! modification to ddh(1) for pond
               ddh(1) = ddh(1) - cpeff(kk)*h0_tmp(kk)*rsigdt(kk) - &
                     (cswat-csice)*h0(kk)/par(1)%thre*var(1)%dthetaldt*rhow*(Tsoil(1))* &
                     rsigdt(kk)*real(var(1)%iice,dp)

             

            !----------------------------------------------------------------
            !           3.5. Solve Matrix
            !----------------------------------------------------------------
               !...............................................................
               !                  a. Uncouple T and Water Content
               !............................................................... 
               if (septs == 1) then ! uncoupled of T and S
               
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     a1. Set nns(kk): start index for eqns
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  ! Solve two tridiagonal matrices instead of 1 block matrix
                  ! Could try two different time steps for moisture and temperature

                  if (ns(kk) == 1) then ! no pond , no litter
                     nns(kk) = 1 ! start index for eqns
                  else ! Contain pond or litter
                     nns(kk) = 0
                  endif


                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     a2. CALL tri Water: aa, cc, ee, gg, dy
                  !                         tridiag set of linear eqns.
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  call tri(nns(kk), n, aa(0:n), cc(0:n), ee(0:n), gg(0:n), dy(0:n))
                  if (nns(kk) == 1) dy(0) = zero

                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     a3. CALL tri Heat: bbh, ddh, ffh, gg, de
                  !                         tridiag set of linear eqns.
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  call tri(nns(kk), n, bbh(0:n), ddh(0:n), ffh(0:n), gg(0:n), de(0:n))

                  if (nns(kk) == 1) de(0) = zero
                  if (nns(kk)==0 .and. h0(kk)<e3) de(0) = zero
                  dTsoil(1:n) = de(1:n)
               !...............................................................
               !                  b. Couple T and Water Content
               !...............................................................
               else ! coupled of T and S
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     b1. Set nns(kk): start index for eqns
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  nns(kk) = 1  ! pond included in top soil layer
              

                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     b2. CALL massman_sparse: water & heat together
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  call massman_sparse(aa(nns(kk)+1:n), aah(nns(kk)+1:n), bb(nns(kk)+1:n), bbh(nns(kk)+1:n), &
                        cc(nns(kk):n), cch(nns(kk):n), dd(nns(kk):n), ddh(nns(kk):n), ee(nns(kk):n-1), &
                        eeh(nns(kk):n-1), &
                        ff(nns(kk):n-1), ffh(nns(kk):n-1), gg(nns(kk):n), ggh(nns(kk):n), &
                        dy(nns(kk):n), de(nns(kk):n), condition=condition)

                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     b3. CALCULATE dTsoil(1:n)
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  dTsoil(1:n) = de(1:n)

               
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     b5. CALCULATE fluxes at sigma
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  ! evaluate soil fluxes at sigma of time step

                  ! EQ. A.31 for water content.....................................
                  qsig(0)  = q(0)+sig(kk)*qyb(0)*dy(1) + sig(kk)*qya(0)*dy(0) + &
                        sig(kk)*qTb(0)*dTsoil(1) + sig(kk)*qTa(0)*de(0)

                  qsig(1:n-1) = q(1:n-1) + sig(kk)*(qya(1:n-1)*dy(1:n-1)+qyb(1:n-1)*dy(2:n) &
                        +qTa(1:n-1)*dTsoil(1:n-1)+qTb(1:n-1)*dTsoil(2:n))

                  qsig(n)     = q(n) + sig(kk)*(qya(n)*dy(n)+qTa(n)*dTsoil(n))

                  ! EQ. A.41 for heat......................................................
                  qhsig(0) = qh(0) + sig(kk)*qhyb(0)*dy(1) + sig(kk)*qhya(0)*dy(0) + &
                        sig(kk)*qhTb(0)*dTsoil(1) + sig(kk)*qhTa(0)*de(0)
                  
                  qhsig(1:n-1) = qh(1:n-1) + sig(kk)*(qhya(1:n-1)*dy(1:n-1)+qhyb(1:n-1)*dy(2:n) &
                        + qhTa(1:n-1)*dTsoil(1:n-1)+qhTb(1:n-1)*dTsoil(2:n))
                        
                  qhsig(n) = qh(n) + sig(kk)*(qhya(n)*dy(n)+qhTa(n)*dTsoil(n))
                  
                  ! for advection......................................................
                  qadvsig(0) = qadv(0) + sig(kk)*qadvyb(0)*dy(1) &
                        + sig(kk)*qadvya(0)*dy(0) + sig(kk)*qadvTb(0)*dTsoil(1) + sig(kk)*qadvTa(0)*de(0)

                  qadvsig(1:n-1) = qadv(1:n-1) + sig(kk)*(qadvya(1:n-1)*dy(1:n-1)+qadvyb(1:n-1)*dy(2:n) &
                        +qadvTa(1:n-1)*dTsoil(1:n-1)+qadvTb(1:n-1)*dTsoil(2:n))
                  
                  qadvsig(n) = qadv(n) + sig(kk)*(qadvya(n)*dy(n)+qadvTa(n)*dTsoil(n))

                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     b6. CALCULATE LHS_h & RHS_h
                  !                     Heat...Left Hand Side...Right Hand Side
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  LHS_h(1) = (dx(1)*var(1)%csoileff+h0_tmp(kk)*cpeff(kk))*dTsoil(1)/dt(kk) - &
                        dy(1)*par(1)%thre*dx(1)*rhow*lambdaf/dt(kk)*var(1)%iice* &
                        real(1-var(1)%isat,dp) - &
                        dy(1)*(var(1)%thetai/par(1)%thre)*rhow*lambdaf/dt(kk)*real(1-ns(kk),dp)* &
                        var(1)%iice  + &
                        var(1)%dthetaldT*(Tsoil(1))*rhow*(cswat-csice)*dx(1)*dTsoil(1)/dt(kk)* &
                        var(1)%iice + &
                        var(1)%dthetaldT*(Tsoil(1))*rhow*(cswat-csice)*h0(kk)/par(1)%thre* &
                        dTsoil(1)/dt(kk)*var(1)%iice

                  LHS_h(2:n) = (dx(2:n)*var(2:n)%csoileff)*dTsoil(2:n)/dt(kk)  - &
                        dy(2:n)*par(2:n)%thre*dx(2:n)*rhow*lambdaf/dt(kk)*var(2:n)%iice* &
                        real(1-var(2:n)%isat,dp) + &
                        var(2:n)%dthetaldT*(Tsoil(2:n))*rhow*(cswat-csice)*dx(2:n)* &
                        dTsoil(2:n)/dt(kk)*var(2:n)%iice

                  RHS_h(1:n) = qhsig(0:n-1) -qhsig(1:n)

                  if (advection==1) then
                     LHS_h(1:n) = LHS_h(1:n) &
                           + real(1-var(1:n)%iice,dp) * real(1-var(1:n)%isat,dp) * &
                           dx(1:n) * dy(1:n)/dt(kk) * par(1:n)%thre * (Tsoil(1:n)) * rhow * cswat &
                           + real(var(1:n)%iice,dp) * real(1-var(1:n)%isat,dp) * &
                           dx(1:n) * dy(1:n)/dt(kk) * par(1:n)%thre * rhow * csice * (Tsoil(1:n))

                     LHS_h(1) = LHS_h(1) &
                           + real(1-ns(kk),dp) * real(1-var(1)%iice,dp) * &
                           dy(1)/dt(kk) * rhow * cswat * (Tsoil(1)) &
                           + real(1-ns(kk),dp) * real(var(1)%iice,dp)* &
                           dy(1)/dt(kk) * rhow * csice * (Tsoil(1)) * (var(1)%thetai/par(1)%thre) &
                           + real(1-ns(kk),dp) * real(var(1)%iice,dp)  * &
                           dy(1)/dt(kk) * rhow * cswat * (Tsoil(1)) * (one-(var(1)%thetai/par(1)%thre))

                  endif

                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     b7. CALCULATE tmp2d1(1:n)
                  !                     rate of change of moisture storage
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  tmp2d1(1:n) = LHS_h(1:n)-RHS_h(1:n) !*dt(kk)
                  
               
               
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  !                     b8. CALCULATE LHS & RHS
                  !                     Water COntent...Left Hand Side...Right Hand Side
                  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                  ! check mass balance on top soil layer
                  if (ns(kk)==0) then  ! pond included in top soil layer
                     LHS(1) = dy(1)/dt(kk)
                  else
                     LHS(1) = dy(1)*par(1)%thre*dx(1)*real(-var(1)%isat+1)/dt(kk)
                  endif

                  RHS(1) = qsig(0) - qsig(1)
                  LHS(2:n) = dy(2:n)*par(2:n)%thre*dx(2:n)*real(-var(2:n)%isat+1)/dt(kk)
                  RHS(2:n) = qsig(1:n-1) - qsig(2:n)

                  

               endif ! septs==1
            !----------------------------------------------------------------
            !           3.6. CALCULATE tmp1d1
            !           max derivative |dTsoil/dt|
            !----------------------------------------------------------------
               tmp1d1 = nless
         
            !----------------------------------------------------------------
            !           3.7. SET iok(kk) = 1, fac(kk) = one
            !           iok == 1 to stop loop while
            !----------------------------------------------------------------
               ! dy contains dS or, for sat layers, dphi values
               iok(kk) = 1
               fac(kk) = one
         
            !----------------------------------------------------------------
            !           3.8. IF(.not. again(kk)) -- SET nfacX, fac, iok 
            !           
            !----------------------------------------------------------------
               if (.not. again(kk)) then

                  ! check if time step ok, if not then set fac to make it less
                  iok(kk) = 1
                 
                  !...............................................................
                  !                  b. Loop do i=1,n (soil layer)
                  !............................................................... 
                  do i=1, n
                  
                     if (var(i)%isat==0) then ! check change in S in initially unsaturated layers
                        
                     !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                     !                     b1. nfac1 - Check change in S in initially unsaturated layers
                     !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if (abs(dy(i)) > dSfac*dSmax) then
                           fac(kk) = max(half,accel(kk)*abs(dSmax/dy(i)))
                           nfac1(kk) = nfac1(kk)+1
                           iok(kk) = 0
                           exit
                        end if

                     !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                     !                     b2. nfac2 - Check relative moisture change
                     !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if (-dy(i) > dSmaxr*S(i)) then ! Check relative moisture change
                           fac(kk) = max(half,accel(kk)*dSmaxr*S(i)/(-dSfac*dy(i)))
                           nfac2(kk) = nfac2(kk)+1
                           iok(kk) = 0
                           exit
                        end if

                     !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                     !                     b3. nfac3 - Check for oversaturating
                     !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if (S(i) < one .and. S(i) + dy(i) > Smax) then ! Check for oversaturating,
                           fac(kk) = accel(kk)*(half*(one+Smax)-S(i))/dy(i)
                           nfac3(kk) = nfac3(kk)+1
                           !write(*,*) 'incrementing nfac3 due to layer', i
                           iok(kk) = 0
                           exit
                        end if

                     !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                     !                     b4. nfac4 - Check for limit at oversaturation
                     !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if (S(i) >= one .and. dy(i) > half*(Smax-one)) then ! Check for limit at oversaturation
                           fac(kk) = 0.25_dp*(Smax-one)/dy(i)
                           nfac4(kk) = nfac4(kk)+1
                           iok(kk) = 0
                           exit
                        end if

                     end if
                  end do

                  !...............................................................
                  !                  c. IF (iok(kk)==1), Check whether updated Tsoil exceeds freezing point
                  !............................................................... 
                  ! Check absolute soil temperature change in frozen soil layers,
                  ! where updated Tsoil exceeds freezing point
                  if (iok(kk)==1) then
                     do i=1, n
                        if(var(i)%iice==1) then
                           Tfreezing(kk) = Tfrz(S(i)+dy(i)*real(1-var(i)%isat), &
                              par(i)%he, one/(par(i)%lambc*freezefac))

                           if ((Tsoil(i)+dTsoil(i))>Tfreezing(kk)) then

                              theta = (S(i)+dy(i)*real(1-var(i)%isat))*(par(i)%thre) + &
                                 (par(i)%the - par(i)%thre)
                              deltaJ_latent_T(i) = thetai(i)*dx(i)*rhow*lambdaf
                              tmp1d1(kk) = Tsoil(i)
                              deltaJ_sensible_S(i) = (tmp1d1(kk))*(rhow*cswat*(theta*dx(i)) + &
                                 par(i)%rho*par(i)%css*dx(i)) - &
                                 (tmp1d1(kk))*(rhow*cswat*(var(i)%thetal*dx(i)) + &
                                 par(i)%rho*par(i)%css*dx(i)) - &
                                 (tmp1d1(kk))*(rhow*csice*(thetai(i)*dx(i)))

                              if ((i==1).and.(h0(kk)>zero)) then
                                 deltaJ_latent_T(i) = deltaJ_latent_T(i) + &
                                    thetai(i)*h0(kk)/par(i)%thre*rhow*lambdaf
                                 deltaJ_sensible_S(i) = deltaJ_sensible_S(i) + &
                                    (tmp1d1(kk))*(rhow*cswat*(h0(kk)+dy(1)*real(1-ns(kk)))) - &
                                    (tmp1d1(kk))*(rhow*cswat*(h0(kk)* &
                                    (one-thetai(1)/par(1)%thre))) - &
                                    (tmp1d1(kk))*(rhow*csice*(h0(kk)* &
                                    thetai(1)/par(1)%thre))

                                 tmp1d1(kk) = (LHS_h(i)*dt(kk) - (deltaJ_latent_T(i) + deltaJ_sensible_S(i)))/ &
                                    (rhow*cswat*(theta*dx(1)+(h0(kk)+dy(1) * &
                                    real(1-ns(kk))))+par(1)%rho*par(1)%css*dx(1))
                              else
                                 tmp1d1(kk) = (LHS_h(i)*dt(kk) - (deltaJ_latent_T(i) + deltaJ_sensible_S(i)))/ &
                                    (dx(i)*(cswat*theta*rhow + par(i)%css*par(i)%rho))
                              endif
                           
                           !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                           !                     c1. nfac11 - Check absolute soil temperature change
                           !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                              if (tmp1d1(kk) > dTsoilmax) then
                                 fac(kk) = min(fac(kk),max(half,accel(kk)*abs(dTsoilmax/tmp1d1(kk))))
                                 nfac11(kk) = nfac11(kk)+1
                                 iok(kk) = 0
                                 exit
                              end if

                           endif
                        endif
                     enddo
                  endif
                  !...............................................................
                  !                  d. nfac5 - Check absolute soil temperature change
                  !...............................................................
                  do i=1, n
                     if (abs(dTsoil(i)) > dTsoilmax.and.(var(i)%iice==0)) then ! Check absolute soil temperature change
                        fac(kk) = min(fac(kk),max(half,accel(kk)*abs(dTsoilmax/dTsoil(i))))
                        nfac5(kk) = nfac5(kk)+1

                        iok(kk) = 0
                        exit
                     end if
                  enddo

                  !...............................................................
                  !                  f. nfac9 - pond decreasing or runoff starts
                  !..............................................................
                  ! pond decreasing or runoff starts
                  if (iok(kk)==1 .and. ns(kk)<1 .and. h0(kk)+dy(1)<h0min) then ! pond going
                     fac(kk) = -(h0(kk)-half*h0min)/dy(1)
                     nfac9(kk) = nfac9(kk) +1
                     iok(kk) = 0
                  end if
               
                  !...............................................................
                  !                  g. Calculate surface J0(1)
                  !..............................................................
                  ! J0+deltaJ<J(thetai=theta) i.e. too much energy extracted
                  J0(1) = rhow*cswat*(Tsoil(1))*dx(1)*var(1)%thetal + &
                     rhow*dx(1)*thetai(1)*(csice*(Tsoil(1))-lambdaf) + &
                     par(1)%rho*par(1)%css*dx(1)*(Tsoil(1)) + &
                     (h0(kk)-hice(kk))*cswat*rhow*(Tsoil(1)) + &
                     hice(kk)*rhow*(csice*(Tsoil(1))-lambdaf)

                  !...............................................................
                  !                  h. Calculate tmp1d2,d3,d4
                  !..............................................................
                  tmp1d2(kk) = J0(1) + LHS_h(1)*dt(kk)  ! available energy
                  tmp1d3(kk) = Tsoil(1)+dTsoil(1)        ! projected temperature
                  tmp1d4(kk) = h0(kk) + dy(1)*real(one-ns(kk)) ! projected pond height

                  !...............................................................
                  !                  i. Calculate theta - projected moisture
                  !..............................................................
                  theta      = (S(1)+ dy(1)*real(one-var(1)%isat))*(par(1)%thre) + &
                              (par(1)%the - par(1)%thre) ! projected moisture

                  !...............................................................
                  !                  j. Calculate c2 - projected energy content
                  !..............................................................
                  c2 = rhow*((tmp1d3(kk))*csice-lambdaf)*(dx(1)*theta+tmp1d4(kk)*theta/par(1)%thre) + & ! projected energy content
                     rhow*(tmp1d3(kk))*cswat*tmp1d4(kk)*(one-theta/par(1)%thre) + &
                     dx(1)*(tmp1d3(kk))*par(1)%rho*par(1)%css
               
                  !...............................................................
                  !                  k. nfac10 - if((tmp1d2(kk)-c2)<zero)
                  !                     available energy
                  !..............................................................
                  if(((tmp1d2(kk))-c2)<zero) then
                     fac(kk) = 0.5_dp
                     iok(kk) = 0
                     nfac10(kk) = nfac10(kk)+1
                  endif
               
                  !...............................................................
                  !                  l. if (fac(kk) < one) -- nfacX true in reducing fac
                  !                     
                  !..............................................................
                  if (fac(kk) < one) then
                     var(:)%thetai = thetai(:)
                     var(:)%dthetaldT = dthetaldT(:)

                     do i=1,n
                        theta = S(i)*(par(i)%thre) + (par(i)%the - par(i)%thre)
                        var(i)%csoil = par(i)%css*par(i)%rho + rhow*cswat*(theta-var(i)%thetai) + &
                           rhow*csice*var(i)%thetai
                        if ((i==1) .and. (ns(kk)==0)) then
                           cp(kk) = real(1-var(1)%iice,dp)*cswat*rhow & ! heat capacity of pond
                              + real(var(1)%iice,dp)*rhow* &
                              ((one-var(1)%thetai/par(1)%thre)*cswat + (var(1)%thetai/par(1)%thre)*csice)
                        endif
                     enddo

                     var(:)%csoileff = var(:)%csoil + rhow*lambdaf*var(:)%dthetaldT*real(var(:)%iice,dp)
                     cpeff(kk)= cp(kk) + rhow*lambdaf*var(1)%dthetaldT/par(1)%thre*real(var(1)%iice,dp)
                     h0_tmp(kk) = h0(kk)

                  endif
               
                  !...............................................................
                  !                  m. if (iok(kk)==0) -- reduce timestep
                  !                     
                  !..............................................................
                  ! reduce time step
                  if (iok(kk)==0) then
                     t(kk)      = t(kk)-dt(kk)
                     dt(kk)     = fac(kk)*dt(kk)
                     t(kk)      = t(kk)+dt(kk)
                     rsigdt(kk) = one/(sig(kk)*dt(kk))
                     nless(kk)  = nless(kk) + 1 ! count step size reductions

                  
                  end if

                  if (var(1)%isat/=0 .and. iflux(kk)==1 .and. var(1)%phi<phip(kk) .and. &
                     var(1)%phi+dy(1)>phip(kk)) then
                     ! incipient (onset of) ponding - adjust state of saturated regions
                     t(kk)      = t(kk)-dt(kk)
                     dt(kk)     = dtmin
                     rsigdt(kk) = one/(sig(kk)*dt(kk))
                     again(kk)  = .true.
                     iok(kk)    = 0
                  end if

               end if  ! (.not. again(kk))

            !----------------------------------------------------------------
            !           3.9. Updating nsteps = nsteps + 1
            !           
            !----------------------------------------------------------------
               nsteps(kk) = nsteps(kk) + 1


            
         end do ! while (iok==0) ----- end get and solve eqns
      
    END SUBROUTINE get_and_solve_eqn

    SUBROUTINE update_unknowns( &
         irec, mp, qprec, n, dx, h0, S,  &
         Tsoil, evap, drainage, discharge, &
         qh, nsteps, vmet, var, &
         phi,  zdelta, &
         par,      &
         qvsig, qlsig, qvTsig,   &
         qevap,  &
         again, ih0, ns,  &
         dt,    &
         sig, &
         dy, &
         de, q, qya, qyb, qTa, qTb,qhya, qhyb, qhTa, qhTb,    &
         qsig, qhsig, qadvsig, qliq, qv, qvT,  qlyb, &
         qvya, qvyb, qlTb, qvTa, qvTb, dTsoil,    &
         v_aquifer,  &
         dJcol_latent_S, dJcol_latent_T, dJcol_sensible, deltaJ_latent_S, &
         deltaJ_latent_T, deltaJ_sensible_S, deltaJ_sensible_T, &
         tmp1d1, tmp1d2, tmp1d3,   deltah0,  &
         LHS_h,  &
         kk,    &
         advection, theta,  cp, &
         hsnow, J0)
     
   
      !************************************************************
      !     (1) Declaring variables
      !************************************************************
         IMPLICIT NONE
         INTEGER(i4)                                           :: irec, mp
         REAL(dp),      DIMENSION(1:mp)                        :: qprec
         INTEGER(i4)                                           :: n
         REAL(dp),      DIMENSION(1:n) :: dx
         REAL(dp),      DIMENSION(1:mp)                        :: h0
         REAL(dp),      DIMENSION(1:n) :: S
         REAL(dp),      DIMENSION(1:n) :: Tsoil
         REAL(dp),      DIMENSION(1:mp)                        :: evap
         REAL(dp),      DIMENSION(1:mp)                        :: drainage, discharge
         REAL(dp),      DIMENSION(-nsnow_max:n) :: qh
         INTEGER(i4),   DIMENSION(1:mp)                        :: nsteps
         TYPE(vars_met), DIMENSION(1:mp)                        :: vmet
         TYPE(vars),     DIMENSION(1:n) :: var
         REAL(dp),      DIMENSION(1:n) :: phi
         REAL(dp),      DIMENSION(1:mp)                        :: zdelta, SL, Tl
         TYPE(params),   DIMENSION(1:n) :: par
         REAL(dp),      DIMENSION(-nsnow_max:n) :: qvsig, qlsig, qvTsig
         REAL(dp),    DIMENSION(1:mp)                          :: qevap
         LOGICAL,      DIMENSION(1:mp)                          :: again
         INTEGER(i4), DIMENSION(1:mp)                          :: ih0, ns
         REAL(dp),    DIMENSION(1:mp)                          :: dt
         REAL(dp),    DIMENSION(1:mp)                          :: sig
         REAL(dp),    DIMENSION(-nsnow_max+1:n) :: dy
         REAL(dp),    DIMENSION(-nsnow_max+1:n) :: de
         REAL(dp),    DIMENSION(-nsnow_max:n) :: q, qya, qyb, qTa, qTb,qhya, qhyb, qhTa, qhTb
         REAL(dp),    DIMENSION(-nsnow_max:n) :: qsig, qhsig, qadvsig
         REAL(dp),    DIMENSION(-nsnow_max:n) :: qliq, qv, qvT, qlyb, qvya, qvyb, qlTb, qvTa, qvTb
         REAL(dp),    DIMENSION(1:n) :: dTsoil
         TYPE(vars_aquifer), DIMENSION(1:mp)                    :: v_aquifer
         
         REAL(dp),          DIMENSION(1:mp)                    :: dJcol_latent_S, dJcol_latent_T, dJcol_sensible
         REAL(dp),          DIMENSION(1:n) :: deltaJ_latent_S, deltaJ_latent_T, deltaJ_sensible_S, deltaJ_sensible_T

         REAL(dp),          DIMENSION(1:mp)                    :: tmp1d1, tmp1d2, tmp1d3
         REAL(dp),          DIMENSION(1:mp)                    :: deltah0
         REAL(dp),          DIMENSION(-nsnow_max+1:n) :: LHS_h
         INTEGER(i4)                                           :: kk
         INTEGER(i4)                                           :: advection ! switches
         REAL(dp)                                              :: theta
         REAL(dp),          DIMENSION(1:mp)                    :: cp
         REAL(dp),          DIMENSION(nsnow_max) :: hsnow
         REAL(dp),      DIMENSION(1:n) :: J0

         !.................................
         ! INTERNAL VARIABLES
         !.................................
         INTEGER(i4)                                           :: i, j
         REAL(dp),          DIMENSION(1:mp)                    :: qevapsig
         REAL(dp),          DIMENSION(1:mp)                    :: dwdrainage, dwdischarge

      !************************************************************
      !     (2) SET  ih0(kk) = 0
      !         flag of pond height
      !************************************************************
         ih0(kk) = 0
      
      !************************************************************
      !     (3) IF(.not. again(kk))
      !         
      !************************************************************
        if (.not. again(kk)) then
     
         !###########################################################
         !           3.1. CALCU, deltah0
         !###########################################################
            ! deltah0 - pond height lost
            deltah0(kk) = zero
           
      
         
         !###########################################################
         !           3.2. CALL JSoilLayer: calcu. J0
         !            J0 - absolute heat stored in soil column
         !###########################################################
            ! absolute heat stored in soil column before update
            do j=1, n
               theta  =  S(j)*(par(j)%thre) + (par(j)%the - par(j)%thre)

               J0(j) = JSoilLayer(Tsoil(j), &
                                 dx(j), theta,par(j)%css, par(j)%rho, &
                                 merge(h0(kk),zero,j.eq.1), par(j)%thre, par(j)%the, &
                                 par(j)%he, one/(par(j)%lambc*freezefac) &
                                 )
            end do
         
         !###########################################################
         !           3.3. CALCU per-layer change in Sensible & Latent Heat
         !###########################################################
            ! change in heat stored in soil column
            do j=1, n
               if (j==1) then ! pond included in top soil layer
                  deltaJ_latent_S(j) = -dx(j)*dy(j)*par(j)%thre*real(var(j)%iice,dp)* &
                     real(1-var(j)%isat,dp)*rhow*lambdaf - &
                     real(1-ns(kk),dp)*dy(1)*real(var(j)%iice,dp)*rhow*lambdaf*(var(1)%thetai/par(1)%thre)

                  deltaJ_latent_T(j) = var(j)%dthetaldT*dx(j)*dTsoil(j)*rhow*lambdaf* &
                     real(var(j)%iice,dp) + &
                     h0(kk)/par(1)%thre*var(j)%dthetaldT*dTsoil(j)*rhow*lambdaf*real(var(j)%iice,dp)

                  deltaJ_sensible_T(j) = var(j)%csoil*dx(j)*dTsoil(j) + &
                     cp(kk)*h0(kk)*dTsoil(j) + &
                     var(1)%iice*var(j)%dthetaldT*(Tsoil(j))*rhow*(cswat-csice)*dx(j)*dTsoil(j) + &
                     var(j)%iice*var(j)%dthetaldT*(Tsoil(j))*rhow*(cswat-csice)* &
                     h0(kk)/par(j)%thre*dTsoil(j)

                  if (advection==1) then
                     deltaJ_sensible_S(j) = rhow*cswat*(Tsoil(j))*dx(j)*par(j)%thre*dy(j)* &
                        real(1-var(j)%isat,dp)*real(1-var(j)%iice,dp)  + &
                        rhow*csice*(Tsoil(j))*dx(j)*par(j)%thre*dy(j)* &
                        real(1-var(j)%isat,dp)*real(var(j)%iice,dp) + &
                        real(1-ns(kk),dp)*real(1-var(1)%iice,dp)* &
                        dy(1)*rhow*cswat*(Tsoil(1))  + &
                        real(1-ns(kk),dp)*real(var(1)%iice,dp)* &
                        dy(1)*rhow*csice*(Tsoil(1))*(var(1)%thetai/par(1)%thre) + &
                        real(1-ns(kk),dp)*real(var(1)%iice,dp)* &
                        dy(1)*rhow*cswat*(Tsoil(1))*(one-(var(1)%thetai/par(1)%thre))
                  endif
               else ! (j>1) layer soil with no pond, since pond only exist in top layer
                  deltaJ_latent_S(j) = -dx(j)*dy(j)*par(j)%thre*real(var(j)%iice,dp)* &
                     real(1-var(j)%isat,dp)*rhow*lambdaf

                  deltaJ_sensible_T(j) = var(j)%csoil*dx(j)*dTsoil(j) + &
                     var(j)%iice*var(j)%dthetaldT*(Tsoil(j))*rhow*(cswat-csice)*dx(j)*dTsoil(j)

                  if (advection==1) then
                     deltaJ_sensible_S(j) = rhow*cswat*(Tsoil(j))*dx(j)*dy(j)*par(j)%thre* &
                        real(1-var(j)%isat,dp)*real(1-var(j)%iice,dp) &
                        + rhow*csice*(Tsoil(j))*dx(j)*dy(j)*par(j)%thre &
                        *real(1-var(j)%isat,dp)*real(var(j)%iice,dp)
                  endif

                  deltaJ_latent_T(j) = var(j)%dthetaldT*dx(j)*dTsoil(j)*rhow*lambdaf*real(var(j)%iice,dp)
               
               endif
            end do
         
         !###########################################################
         !           3.4. CALCU Columns change in Sensible & Latent Heat
         !###########################################################
            ! change in heat stored in soil column
            dJcol_latent_S(kk) = sum(deltaJ_latent_S(1:n))
            dJcol_latent_T(kk) = sum(deltaJ_latent_T(1:n))
            dJcol_sensible(kk) = sum(deltaJ_sensible_T(1:n)) + sum(deltaJ_sensible_S(1:n))


         !###########################################################
         !           3.5. CALCU tmp1d1(kk)
         !            tmp1d1 - max derivative |dTsoil/dt|
         !###########################################################            
            tmp1d1(kk) = dJcol_latent_S(kk) + dJcol_latent_T(kk) + dJcol_sensible(kk) - (qhsig(0)-qhsig(n))*dt(kk)

         !###########################################################
         !           3.6. CALCU change in pond height: h0, deltah0. ih0
         !###########################################################
            if (ns(kk)<1) then ! change in pond height
               h0(kk) = h0(kk) + dy(1)
               deltah0(kk) = dy(1)
               if (h0(kk)<zero .and. dy(1)<zero) then
                  ih0(kk)=1 ! pond gone
               endif
            endif

         !###########################################################
         !           3.7. CALCU Cumulative Evap fluxes from top of soil column
         !###########################################################
        
            evap(kk)     = evap(kk) + qevap(kk)*dt(kk) - sig(kk)*(qyb(0)*dy(1)+qTb(0)*dTsoil(1))*dt(kk)
            qevapsig(kk) = qevap(kk) - sig(kk)*(qyb(0)*dy(1)+qTb(0)*dTsoil(1))
           

         !###########################################################
         !           3.8. CALCU dwdrainage(kk)
         !            dwdrainage - drainage at the bottom layers
         !###########################################################
            dwdrainage(kk) = q(n)*dt(kk) + sig(kk)*dt(kk)*(qya(n)*dy(n) + qTa(n)*dTsoil(n))

         !###########################################################
         !           3.9. CALCU dwdischarge(kk), IF(botbc=="aquifer")
         ! 
         !###########################################################
            if (botbc=="aquifer" .and. v_aquifer(kk)%isat==0) then
               dwdischarge(kk) = v_aquifer(kk)%discharge*dt(kk)
            else
               dwdischarge(kk) = dwdrainage(kk)
            endif

         !###########################################################
         !           3.11. CALCU cumulative fluxes
         ! 
         !###########################################################
            
            discharge(kk) = discharge(kk) + dwdischarge(kk)

            if (botbc=="constant head") then 
                drainage(kk) = drainage(kk)+(q(n)+sig(kk)*qya(n)*dy(n))*dt(kk)
            else 
               drainage(kk)  = drainage(kk) + dwdrainage(kk)
            end if
         
         !###########################################################
         !           3.12. CALCU q at sigma: for use in isotope routine
         ! 
         !###########################################################
            ! evaluate soil fluxes at sigma of time step for use in isotope routine
           
            ! water content.........................
            qsig(0)  = q(0)  + sig(kk)*(qyb(0)*dy(1)  + qya(0)*dy(0)  + qTb(0)*de(1) &
                  + qTa(0)*de(0))
            qsig(1:n-1)   = q(1:n-1) + sig(kk)*(qya(1:n-1)*dy(1:n-1) + qyb(1:n-1)*dy(2:n) &
                  + qTa(1:n-1)*de(1:n-1) + qTb(1:n-1)*de(2:n))
            qsig(n)       = q(n) + sig(kk)*(qya(n)*dy(n) + qTa(n)*de(n))

            ! heat..........................
            qhsig(0) = qh(0) + sig(kk)*(qhyb(0)*dy(1) + qhya(0)*dy(0) + qhTb(0)*de(1) &
                  + qhTa(0)*de(0))
            qhsig(1:n-1)  = qh(1:n-1) + sig(kk)*(qhya(1:n-1)*dy(1:n-1) + qhyb(1:n-1)*dy(2:n) &
                  + qhTa(1:n-1)*de(1:n-1) + qhTb(1:n-1)*de(2:n))
            qhsig(n)      = qh(n) + sig(kk)*(qhya(n)*dy(n) + qhTa(n)*de(n))
            
            ! vapor..........................
            qvsig(0) = qv(0)+sig(kk)*qvyb(0)*dy(1)+sig(kk)*qvTb(0)*de(1)
            qvsig(1:n-1)  = qv(1:n-1) + sig(kk)*(qvya(1:n-1)*dy(1:n-1) + qvyb(1:n-1)*dy(2:n) &
                  + qvTa(1:n-1)*de(1:n-1) + qvTb(1:n-1)*de(2:n))
            qvsig(n)      = zero
            
            qvTsig(0)     = qvT(0) + sig(kk)*qvTb(0)*de(1)
            qvTsig(1:n-1) = qvT(1:n-1) + sig(kk)*(qvTa(1:n-1)*de(1:n-1) + qvTb(1:n-1)*de(2:n))
            qvTsig(n)     = zero

            qlsig(0) = qliq(0)+sig(kk)*qlyb(0)*dy(1)+sig(kk)*qlTb(0)*de(1)
            qlsig(1:n-1)  = qsig(1:n-1) - qvsig(1:n-1)
            qlsig(n)      = qsig(n)
         
         !###########################################################
         !           3.13. UPDATE v_aquifer%... if (botbc=="aquifer")
         !               v_aquifer - aquifer properties
         !###########################################################
            if (botbc=="aquifer") then ! update aquifer props
               if (v_aquifer(kk)%isat==0) then
                  if (v_aquifer(kk)%WA+(dwdrainage(kk)-dwdischarge(kk))*dt(kk) > &
                     (v_aquifer(kk)%zzero-v_aquifer(kk)%zsoil)*v_aquifer(kk)%Sy) then
                     v_aquifer(kk)%isat = 1  ! aquifer saturated
                     S(n) = S(n) + (-(v_aquifer(kk)%WA+(dwdrainage(kk)-dwdischarge(kk))*dt(kk)) &
                        +(v_aquifer(kk)%zzero-v_aquifer(kk)%zsoil)*v_aquifer(kk)%Sy)/(dx(n)*par(n)%thre)
                  endif

                  v_aquifer(kk)%WA        = min(v_aquifer(kk)%WA+(dwdrainage(kk)-dwdischarge(kk))*dt(kk), &
                     (v_aquifer(kk)%zzero-v_aquifer(kk)%zsoil)*v_aquifer(kk)%Sy)
                  v_aquifer(kk)%zdelta    = v_aquifer(kk)%zzero - v_aquifer(kk)%Wa/v_aquifer(kk)%Sy

                  ! new discharge rate
                  v_aquifer(kk)%discharge = v_aquifer(kk)%Rsmax*exp(-v_aquifer(kk)%f*v_aquifer(kk)%zdelta)

               elseif (v_aquifer(kk)%isat==1) then
                  ! check for desat of aquifer
                  if (dwdrainage(kk) < v_aquifer(kk)%Rsmax*exp(-v_aquifer(kk)%f*v_aquifer(kk)%zsoil)) then
                     v_aquifer(kk)%isat      = 0
                     v_aquifer(kk)%zdelta    = v_aquifer(kk)%zsoil
                     ! new discharge rate
                     v_aquifer(kk)%discharge = v_aquifer(kk)%Rsmax*exp(-v_aquifer(kk)%f*v_aquifer(kk)%zdelta)
                  endif
               endif
               zdelta(kk) = v_aquifer(kk)%zdelta
            endif       ! end update aquifer props
            
         
         !###########################################################
         !           3.17. UPDATE: phi(:), kth(:), csoil(:)
         !           
         !###########################################################
            ! csoil(:) = var(1:n)%csoil
            ! kth(:) = var(1:n)%kth
            phi(:) = var(1:n)%phi
         
        end if ! (.not. again(kk))


    END SUBROUTINE update_unknowns

    SUBROUTINE update_s_t(&
         mp, n, dx, h0, S, thetai, &
         Tsoil,   &
         nsteps, var,     &
         par, &
         qlsig,    &
         again, ih0, ns,  &
         dt,      &
         dy,        &
         qhsig,       &
         dTsoil,    &
         deltaJ_latent_S, &
         deltaJ_latent_T, deltaJ_sensible_S, deltaJ_sensible_T,  &
         tmp1d1, tmp1d2, tmp1d3,  tmp1d4, deltah0,    &
         Tfreezing, LHS_h,    &
         kk,    &
         theta, &
         hice,   &
         J0)
      
      ! update variables (S,T) to end of time step
      ! update variables to sig_component for use in isotope routine
   
      !**************************************************
      !     (1) Declaring Variable
      !**************************************************
         IMPLICIT NONE
         INTEGER(i4)                                           :: mp
         INTEGER(i4)                                           :: n
         REAL(dp),      DIMENSION(1:n) :: dx
         REAL(dp),      DIMENSION(1:mp)                        :: h0
         REAL(dp),      DIMENSION(1:n) :: S
         REAL(dp),      DIMENSION(1:n) :: thetai
         REAL(dp),      DIMENSION(1:n) :: Tsoil
         INTEGER(i4),   DIMENSION(1:mp)                        :: nsteps
         TYPE(vars),     DIMENSION(1:n) :: var
         TYPE(params),   DIMENSION(1:n) :: par
         REAL(dp),      DIMENSION(-nsnow_max:n) :: qlsig
         LOGICAL,      DIMENSION(1:mp)                          :: again
         INTEGER(i4), DIMENSION(1:mp)                          :: ih0, ns
         REAL(dp),    DIMENSION(1:mp)                          :: dt
         REAL(dp),    DIMENSION(-nsnow_max+1:n) :: dy
         REAL(dp),    DIMENSION(-nsnow_max:n) :: qhsig
         REAL(dp),    DIMENSION(1:n) :: dTsoil
         REAL(dp),          DIMENSION(1:mp)                    :: Jcol_sensible
         REAL(dp),          DIMENSION(1:n) :: deltaJ_latent_S, deltaJ_latent_T, deltaJ_sensible_S, deltaJ_sensible_T
         REAL(dp),          DIMENSION(1:mp)                    :: tmp1d1, tmp1d2, tmp1d3,  tmp1d4
         REAL(dp),          DIMENSION(1:mp)                    :: deltah0
         REAL(dp),          DIMENSION(1:mp)                    :: Tfreezing
         REAL(dp),          DIMENSION(-nsnow_max+1:n) :: LHS_h
         INTEGER(i4)                                           :: kk
         REAL(dp)                                              :: theta
         REAL(dp),          DIMENSION(1:mp)                    :: hice
         REAL(dp),      DIMENSION(1:n) :: J0
         

         
         

         !....................................................................................
         ! INTERNAL VARIABLES
         !....................................................................................
         INTEGER(i4)                                           :: i, j
         REAL(dp)                                              :: tmp1, tmp2
         REAL(dp),          DIMENSION(1:mp)                    :: hice_tmp
         REAL(dp),          DIMENSION(1:n)                     :: h_ex
         REAL(dp)                                              :: wpi
         REAL(dp),          DIMENSION(1:mp, 1:n)               :: deltaS


         hice_tmp           = zero
         !**************************************************
         !     (2) Loop do i=1, n -- update variables S,T
         !**************************************************
            do i=1, n
               !#############################################################
               !           2.1. if (.not.again(kk)) -  Update Tsoil(i)  = Tsoil(i) + dTsoil(i)
               !############################################################# 
               if (.not.again(kk)) Tsoil(i)  = Tsoil(i) + dTsoil(i)
               
               !#############################################################
               !           2.2. if unsaturated & .not.again(kk)
               !#############################################################
               if (var(i)%isat==0) then !unsaturated
                  if (.not.again(kk)) then
                  !----------------------------------------------------------
                  !                 a. Calcu deltaS(i), S(i)
                  !----------------------------------------------------------
                     deltaS(kk,i) = dy(i)
                     S(i)      = S(i)+dy(i)
                  
                  !----------------------------------------------------------
                  !                 b. if saturated: Calcu isat, K, phi
                  !---------------------------------------------------------
                     if (S(i)>one .and. dy(i)>zero) then ! onset of saturation of layer
                        var(i)%isat = 1
                        var(i)%K    = var(i)%Ksat
                        var(i)%phi  = var(i)%phie
                     endif

                  endif
               
               !#############################################################
               !           2.3. if saturated 
               !#############################################################
               else
                  !----------------------------------------------------------
                  !                 a. Set deltaS(i)  = zero
                  !---------------------------------------------------------
                  deltaS(kk,i)  = zero    !required for isotope subroutine

                  !----------------------------------------------------------
                  !                 b. Calcu phi
                  !---------------------------------------------------------
                  if (i==1) then ! pond included in top soil layer
                     var(i)%phi = var(i)%phi + dy(i)*real(ns(kk),dp) 
                  else
                     var(i)%phi = var(i)%phi + dy(i)
                  endif

                  ! var(i)%phi = zero from Ross
                  ! VH thinks it is o.k. because hyofS is called later again
                  ! MC think that we should probably get rid of -he*Ksat in phip etc.
                  !   because we merged the pond with the first layer.
                  
                  !----------------------------------------------------------
                  !                 c. If no pond - Set var(1)%phi = zero
                  !---------------------------------------------------------
                  if (i==1 .and. ih0(kk)/=0 .and. var(i)%phi>=var(i)%phie) then 
                     var(i)%phi = zero ! pond gone
                  end if
                  
                  !----------------------------------------------------------
                  !                 d. If var(i)%iice==0
                  !---------------------------------------------------------
                  if (var(i)%phi<var(i)%phie .and. var(i)%iice==0) then ! desaturation of layer
                     var(i)%isat = 0
                     var(i)%K    = var(i)%Ksat
                     var(i)%phi  = var(i)%phie
                     var(i)%KS   = par(i)%KSe
                     var(i)%phiS = par(i)%phiSe
                  
                  !----------------------------------------------------------
                  !                 e. If var(i)%iice==1
                  !---------------------------------------------------------
                  elseif (var(i)%phi<var(i)%phie .and. var(i)%iice==1) then
                     var(i)%isat = 0
                     var(i)%K    = var(i)%Ksat
                     var(i)%phi  = var(i)%phie
                     var(i)%KS   = var(i)%KS
                     var(i)%phiS = var(i)%phiS
                  endif
               end if

               !#############################################################
               !           2.4. if (.not. again(kk)) 
               !#############################################################
               if (.not. again(kk)) then
                  
                  !----------------------------------------------------------
                  !                 a. Calcu wpi -  water in profile initially
                  !---------------------------------------------------------
                  wpi = sum((par(:)%thr + (par(:)%the-par(:)%thr)*S(:))*dx(:))
                  
                  !----------------------------------------------------------
                  !                 b. if (h0(kk)<zero - negative pond correction
                  !---------------------------------------------------------
                  if (h0(kk)<zero .and. var(1)%isat==0 .and. (i==1)) then ! start negative pond correction
                     wpi = sum((par(:)%thr + (par(:)%the-par(:)%thr)*S(:))*dx(:))

                     do j=1,n
                        h_ex(j) = -h0(kk)* (par(j)%the-par(j)%thr)*S(j)* dx(j)/wpi
                     enddo

                     do j=1,n-1
                        qlsig(j) = qlsig(j) +sum(h_ex(j:n))
                     enddo

                     do j = 1,n
                        if (j<n .and. ( S(j)*(dx(j)*par(j)%thre) + h_ex(j)).lt.zero) then
                           h_ex(j+1) = h_ex(j+1) +(h_ex(j)- S(j)*(dx(j)*par(j)%thre))
                           h_ex(j) = S(j)*(dx(j)*par(j)%thre)
                        endif

                        deltaS(kk,j)= -min(h_ex(j)/(dx(j)*par(j)%thre),S(j))

                        if (var(j)%isat.eq.0) then
                           dy(j) = dy(j) + deltaS(kk,j)
                        else
                           dy(j) = deltaS(kk,j)
                           var(j)%isat = 0
                        endif

                        if (j==1) S(j)=S(j)+deltaS(kk,j)

                     enddo
                     
                     deltah0(kk) = -(h0(kk)-deltah0(kk)) ! whole pond lost (new change = - original pond height)
                     h0(kk) = zero  ! zero pond remaining

                  endif

                  !----------------------------------------------------------
                  !                 c. if S(i)<zero - negative soil moisture correction
                  !---------------------------------------------------------
                  if (S(i)<zero .and. i<n) then ! start correction for negative soil moisture in any layer
                     wpi = sum((par(i+1:n)%thr + (par(i+1:n)%the-par(i+1:n)%thr)*S(i+1:n))*dx(i+1:n))

                     do j=i+1,n
                        h_ex(j) = -deltaS(kk,i)*dx(i)*par(i)%thre * &
                              (par(j)%thr + (par(j)%the-par(j)%thr)*S(j))*dx(j) /wpi
                     enddo

                     do j=i+1,n
                        qlsig(j) = qlsig(j) +sum(h_ex(j:n))
                     enddo

                     do j = i+1,n
                        if (j<n .and. ( S(j)*(dx(j)*par(j)%thre) - h_ex(j)).lt.zero) then
                           h_ex(j+1) = h_ex(j+1) +(h_ex(j)- S(j)*(dx(j)*par(j)%thre))
                           h_ex(j) = S(j)*(dx(j)*par(j)%thre)
                        endif
                        deltaS(kk,j)= -min(h_ex(j)/(dx(j)*par(j)%thre),S(j))
                        if (var(j)%isat.eq.0) then
                           dy(j) = dy(j) + deltaS(kk,j)
                        else
                           dy(j) = deltaS(kk,j)
                           var(j)%isat = 0
                        endif
                        if (j==i) S(j)=S(j)+deltaS(kk,j)

                     enddo

                     S(i)=S(i)-deltaS(kk,i)
                     deltaS(kk,i) = zero
                     
                     
                  endif
                  
                  !----------------------------------------------------------
                  !                 d. Re-Calculate Tfreezing(kk) -- from new S(i)
                  !---------------------------------------------------------
                  ! corrections for onset of freezing and melting
                  ! calculate freezing point temperature at new S
                  if(septs==0) then
                     Tfreezing(kk) = Tfrz(S(i), par(i)%he, one/(par(i)%lambc*freezefac))
                  end if
                  ! check for onset of freezing and adjust (increase) temperature to account for latent heat
                  ! release by ice formation
                  
                  !----------------------------------------------------------
                  !                 f. if (var(i)%iice==1) - Correcting tmp1d1, tmp1d2, tmp1d3
                  !---------------------------------------------------------
                  ! tmp1d1 - max derivative |dTsoil/dt|
                  ! tmp1d2(:)     - available energy
                  ! tmp1d3(:)     - projected temperature
                  if (var(i)%iice==1) then
                     theta         = S(i)*(par(i)%thre) + (par(i)%the - par(i)%thre)
                     tmp1d1(kk) = Tsoil(i) - dTsoil(i)
                     ! energy for complete melting
                     if ((i==1).and.(h0(kk)>zero)) then
                        tmp1d3(kk) = thetai(i)*dx(i)*rhow*lambdaf + &
                              thetai(i)*(h0(kk)- deltah0(kk))/par(i)%thre*rhow*lambdaf

                        tmp1d2(kk) = (Tfreezing(kk))*(rhow*cswat*(theta*dx(1)+h0(kk))+ &
                              par(1)%rho*par(1)%css*dx(1)) - &
                              (tmp1d1(kk))*(rhow*cswat*(var(i)%thetal*dx(1)+(h0(kk)- deltah0(kk))* &
                              (one-thetai(1)/par(1)%thre))+par(1)%rho*par(1)%css*dx(1)) - &
                              (tmp1d1(kk))*(rhow*csice*(thetai(i)*dx(1)+(h0(kk)- deltah0(kk))* &
                              thetai(1)/par(1)%thre))
                     else
                        tmp1d3(kk) = thetai(i)*dx(i)*rhow*lambdaf

                        tmp1d2(kk) = (Tfreezing(kk))*(rhow*cswat*(theta*dx(i))+par(i)%rho*par(i)%css*dx(i)) - &
                              (tmp1d1(kk))*(rhow*cswat*(var(i)%thetal*dx(i))+par(i)%rho*par(i)%css*dx(i)) - &
                              (tmp1d1(kk))*(rhow*csice*(thetai(i)*dx(i)))
                     endif
                  endif
                  
                  !----------------------------------------------------------
                  !                 g. Check for thawing -- Correcting Tsoil
                  !---------------------------------------------------------
                  ! check for onset of thawing, and 
                  ! decrease temperature to account for latent heat required to melting ice
                  if ((var(i)%iice==1).and.((J0(i) + LHS_h(i)*dt(kk)).ge.JSoilLayer(Tfreezing(kk), &
                        dx(i), theta,par(i)%css, par(i)%rho, &
                        merge(h0(kk),zero,i==1), par(i)%thre, par(i)%the, &
                        par(i)%he, one/(par(i)%lambc*freezefac)))) then
                     ! start correction for onset of thawing
                     ! Correct for "overthawing". This extra energy is used to heat the soil even more
                     ! The correction comes from equating the energy balances before and after correction.
                     tmp1d1(kk) = Tsoil(i) - dTsoil(i)

                     if ((i==1) .and. (h0(kk)>zero)) then
                        deltaJ_latent_T(i)=      tmp1d3(kk)
                        deltaJ_latent_S(i) = zero
                        h0(kk) = h0(kk) - deltah0(kk)

                        deltaJ_sensible_S(i) = (tmp1d1(kk))*(rhow*cswat*(theta*dx(1)+(h0(kk)+deltah0(kk)))+ &
                              par(1)%rho*par(1)%css*dx(1)) - &
                              (tmp1d1(kk))*(rhow*cswat*(var(i)%thetal*dx(1)+(h0(kk))* &
                              (one-thetai(1)/par(1)%thre))+par(1)%rho*par(1)%css*dx(1)) - &
                              (tmp1d1(kk))*(rhow*csice*(thetai(i)*dx(1)+(h0(kk))* &
                              thetai(1)/par(1)%thre))

                        dTsoil(i) = (LHS_h(i)*dt(kk) - (deltaJ_latent_T(i) + deltaJ_sensible_S(i)))/ &
                              (rhow*cswat*(theta*dx(1)+(h0(kk)+deltah0(kk)))+par(1)%rho*par(1)%css*dx(1))

                        deltaJ_sensible_T(i) = dTsoil(i)* &
                              (rhow*cswat*(theta*dx(1)+(h0(kk)+deltah0(kk)))+par(1)%rho*par(1)%css*dx(1))

                        h0(kk) = h0(kk) + deltah0(kk)
                     else
                        deltaJ_latent_T(i) = thetai(i)*dx(i)*rhow*lambdaf
                        deltaJ_latent_S(i) = zero

                        theta         = S(i)*(par(i)%thre) + (par(i)%the - par(i)%thre)
                        var(i)%csoil = cswat*theta*rhow + par(i)%css*par(i)%rho

                        deltaJ_sensible_S(i) = (tmp1d1(kk))*(rhow*cswat*(theta*dx(i))+ &
                              par(i)%rho*par(i)%css*dx(i)) - &
                              (tmp1d1(kk))*(rhow*cswat*(var(i)%thetal*dx(i))+par(i)%rho*par(i)%css*dx(i)) - &
                              (tmp1d1(kk))*(rhow*csice*(thetai(i)*dx(i)))
                        dTsoil(i) = (LHS_h(i)*dt(kk) - (deltaJ_latent_T(i) + deltaJ_sensible_S(i)))/ &
                              (dx(i)*var(i)%csoil)
                        deltaJ_sensible_T(i) = var(i)%csoil*dTsoil(i)*dx(i)
                     endif
                     Tsoil(i) = tmp1d1(kk) + dTsoil(i)
                     
                     ! correct thetai and T in frozen soil
                  elseif ((var(i)%iice==1).and.((J0(i) + LHS_h(i)*dt(kk))<JSoilLayer(Tfreezing(kk), &
                        dx(i), theta,par(i)%css, par(i)%rho, &
                        merge(h0(kk),zero,i==1), par(i)%thre, par(i)%the, &
                        par(i)%he, one/(par(i)%lambc*freezefac)))) then

                     tmp1d2(kk) = J0(i) + LHS_h(i)*dt(kk) ! total energy in  soil layer
                     if (Tsoil(i) .lt. -40.0_dp) then
                        ! assume no lquid below min temp threshhold
                        var(i)%thetal = 0.0_dp
                        var(i)%thetai = theta
                        if (i.eq.1) hice(kk) = h0(kk)
                        tmp1d3(kk) = (tmp1d2(kk) + rhow*lambdaf*(theta*dx(i) +  merge(h0(kk),zero,i==1)))/ &
                              (dx(i)*par(i)%css*par(i)%rho + rhow*csice*(theta*dx(i) + &
                              merge(h0(kk),zero,i==1)))

                     else
                        !check there is a zero
                        tmp1 = GTfrozen(Tsoil(i)-dTsoil(i)-50._dp, tmp1d2(kk), dx(i), theta,par(i)%css, par(i)%rho, &
                              merge(h0(kk),zero,i==1), par(i)%thre, par(i)%the, par(i)%he, one/(par(i)%lambc*freezefac))

                        tmp2 = GTFrozen(Tfreezing(kk), tmp1d2(kk), dx(i), theta,par(i)%css, par(i)%rho, &
                              merge(h0(kk),zero,i==1), par(i)%thre, par(i)%the, par(i)%he, one/(par(i)%lambc*freezefac))
                        
                        ! there is a zero in between
                        if ((tmp1*tmp2) < zero) then
                           tmp1d3(kk) = rtbis_Tfrozen(tmp1d2(kk), dx(i), theta,par(i)%css, par(i)%rho, &
                                 merge(h0(kk),zero,i==1), par(i)%thre, par(i)%the, &
                                 par(i)%he, one/(par(i)%lambc*freezefac), &
                                 Tsoil(i)-dTsoil(i)-50._dp, Tfreezing(kk), 0.0001_dp)
                           tmp1d4(kk) = thetalmax(tmp1d3(kk), S(i), par(i)%he, one/(par(i)%lambc*freezefac), &
                                 par(i)%thre, par(i)%the) ! liquid content at solution for Tsoil
                        else
                           write(*,*) "Found no solution for Tfrozen 1. ", kk, i
                           write(*,*) "Assume soil is totally frozen"
                           var(i)%thetal = 0.0_dp
                           var(i)%thetai = theta
                           if (i.eq.1) hice(kk) = h0(kk)

                           tmp1d3(kk) = (tmp1d2(kk) + rhow*lambdaf*(theta*dx(i) +  merge(h0(kk),zero,i==1)))/ &
                                 (dx(i)*par(i)%css*par(i)%rho + rhow*csice*(theta*dx(i) + &
                                 merge(h0(kk),zero,i==1)))
                           write(*,*) "frozen soil temperature: ", tmp1d3(kk)
                           !write(*,*) "soil moisture: ", S(kk)

                           write(*,*) nsteps(kk), S(i), Tsoil(i), dTsoil(i), h0(kk), tmp1, tmp2, tmp1d2(kk), theta, &
                                 JSoilLayer(Tfreezing(kk), &
                                 dx(i), theta,par(i)%css, par(i)%rho, &
                                 merge(h0(kk),zero,i==1), par(i)%thre, par(i)%the, &
                                 par(i)%he, one/(par(i)%lambc*freezefac)), J0(i) + LHS_h(i)*dt(kk), Tfreezing(kk)
                           ! stop

                        endif
                        var(i)%thetal = tmp1d4(kk)
                        var(i)%thetai = theta - tmp1d4(kk)
                     end if ! Tsoil <-40

                     if (i==1) then
                        hice_tmp(kk) = hice(kk)
                        hice(kk) = h0(kk)*var(1)%thetai/par(1)%thre

                        ! correct total energy stored in pond + soil
                        deltaJ_latent_S(i) = - rhow*lambdaf*((hice(kk)-hice_tmp(kk)) + &
                              dx(1)*(var(1)%thetai-thetai(1)))
                     else
                        ! correct total energy stored in  soil
                        deltaJ_latent_S(i) = - rhow*lambdaf*( + &
                              dx(i)*(var(i)%thetai-thetai(i)))


                     endif

                     deltaJ_latent_T(i) = zero

                     deltaJ_sensible_T(i) = JSoilLayer(tmp1d3(kk), &
                           dx(i), theta,par(i)%css, par(i)%rho, &
                           merge(h0(kk),zero,i==1), par(i)%thre, par(i)%the, &
                           par(i)%he, one/(par(i)%lambc*freezefac)) - &
                           J0(i)- &
                           deltaJ_latent_S(i)

                     deltaJ_sensible_S(i) = 0._dp

                     thetai(i) = var(i)%thetai
                     Tsoil(i) = tmp1d3(kk)

                  endif ! soil remains frozen

               endif ! if .not.again

            end do ! i=1, n => update variables S,T
         
        
    END SUBROUTINE update_s_t

END MODULE mo_sli_solve