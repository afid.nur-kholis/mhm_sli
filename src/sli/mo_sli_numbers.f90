MODULE mo_sli_numbers
    use mo_kind, only : i4, dp

    implicit none

    real(dp), public, dimension(:, :), allocatable :: dx, x, z

    TYPE params !soil parameters
     REAL(dp) :: the, thre, he, lam, Ke, eta, thr
     REAL(dp) :: KSe, phie, phiSe, rho, thw, thfc, kd, css, clay, sand, tortuosity
     INTEGER(i4) :: ishorizon ! horizon number with different soil properties
     REAL(dp) :: zeta
     REAL(dp) :: fsatmax
     REAL(dp) :: lambc        ! original lam for storage
     REAL(dp) :: LambdaS      ! thermal inertia of saturation for van de Griend & O'Neill (1986) thermal conductivity
    END TYPE params

    TYPE vars_met !variable of meteoroogy
     REAL(dp) :: Ta, &      ! air temperature
                 rha, &     ! relative humidity
                 Da, &      ! water vap pressure deficit at ref height (Pa) -- m3 H2O (liq)/ m3 (air)
                 cva, &     ! concentration of atmospheric water vapour (m3 H2O(l)/ m3 (air))
                 rbw, &     ! resistance to water vapour transfer beween surface and lowest atmospheric layer
                 rbh, &     ! resistance to heat transfer beween surface and lowest atmospheric layer
                 rrc, &     ! combined convective and radiative resistance to heat transfer
                 Rn, & 
                 civa, &    ! atmospheric isotope concentration
                 phiva, & 
                 Rnsw
    END TYPE vars_met

    TYPE vars ! variable of soils
        INTEGER(i4) :: isat
        REAL(dp)    :: h, phi, phiS, K, KS, Dv, cvsat, rh, phiv, phivS, kH
        REAL(dp)    :: kE, kth, csoil, eta_th, hS, rhS, sl, &
                       cv, & ! Concentration of water vapour in soil air spaces (m3_LiqWater/m3_air)
                       cvsatT, cvS, kv
        INTEGER(i4) :: iice ! flag, (1) ice (soil freezing), (0) no ice
        REAL(dp)    :: thetai, thetal, phiT, KT, lambdav, lambdaf, Sliq
        REAL(dp)    :: he, phie, Ksat ! air-entry potential values (different to core sp params for frozen soil)
        REAL(dp)    :: dthetaldT
        REAL(dp)    :: Tfrz ! freezing point (deg C) depends on csol and S
        REAL(dp)    :: csoileff
        REAL(dp)    :: zsat ! height of saturated/unsaturated boundary (relative to bottom of layer)
        REAL(dp)    :: macropore_factor
    END TYPE vars

    INTEGER(i4), PARAMETER :: nsnow_max = 1  ! maximum number of dedicated snow layers (1 or 2)
    TYPE vars_snow
        REAL(dp), DIMENSION(nsnow_max):: depth, & 
                                         hsnow, & 
                                         hliq, &
                                         dens, & 
                                         tsn, &
                                         kH, &
                                         kE, &
                                         kth, &
                                         Dv, &
                                         cv, & ! concentration of water vapour in soil air spaces (m3_LiqWater/m3_air)
                                         sl, &
                                         melt, &
                                         Jsensible, &
                                         Jlatent, &
                                         deltaJlatent, &
                                         deltaJsensible, &
                                         fsnowliq_max
        REAL(dp) :: wcol, Qadv_snow, Qadv_rain, totdepth, J, &
                    Qadv_melt, Qadv_vap, Qcond_net, &
                    Qadv_transfer, Qmelt, Qtransfer,FluxDivergence, deltaJ, &
                    Qvap, MoistureFluxDivergence, Qprec, Qevap, deltawcol
        INTEGER(i4) :: nsnow, nsnow_last
    END TYPE vars_snow

    TYPE solve_type ! for energy and moisture balance in rh0_sol, etc.
        INTEGER(i4) :: k
        REAL(dp)    :: T1, Ta, cva, Rnet, hr1, hra, Dv, gv, gh, Dh, & 
                       dz, phie, he, K1, eta,lambda, Ks, lambdav
    END TYPE solve_type

    TYPE vars_aquifer
        INTEGER(i4) :: isat
        REAL(dp)    :: zdelta, zsoil, zzero, K, Wa, discharge, f, Rsmax, Sy
    END TYPE vars_aquifer

    ! Constants for Saturated vapour pressure calculation
    REAL(dp), PARAMETER :: esata = 6.106_dp*100.0_dp ! constants for saturated vapour pressure calculation
    REAL(dp), PARAMETER :: esatb = 17.27_dp         ! %
    REAL(dp), PARAMETER :: esatc = 237.3_dp         ! %

    REAL(dp), PARAMETER :: esata_ice = 611.2_dp   ! constants for saturated vapour pressure calculation over ice (WMO, 2008)
    REAL(dp), PARAMETER :: esatb_ice = 22.46_dp   ! %
    REAL(dp), PARAMETER :: esatc_ice = 272.62_dp  ! %

    ! constant for unit transform
    REAL(dp), PARAMETER :: Tzero = 273.16_dp ! Celcius -> Kelvin

    ! heat parameters contants
    REAL(dp), PARAMETER :: Dva = 2.17e-5_dp ! vapour diffusivity of water in air at 0 degC [m2/s]
    REAL(dp), PARAMETER :: cpa       = 1004.64_dp  ! specific heat capacity of dry air at 0-40 degC [J/kgK]
    REAL(dp), PARAMETER :: rhoa      = 1.184_dp    ! denisty of dry air at std (25 degC) [kg/m3]
    REAL(dp), PARAMETER :: rhocp     = 1189.8_dp   ! cpa*rhoa at std (25 degC) [J/m3K]

    REAL(dp), PARAMETER :: lambdaf   = 335000._dp  ! latent heat of fusion (J kg-1)
    REAL(dp), PARAMETER :: rlambda   = 2.442e6_dp  ! latent heat of condensation at 25 degC [J/kg]

    REAL(dp), PARAMETER :: cswat     = 4.218e3_dp ! specific heat capacity for water (J/kg/K)
    REAL(dp), PARAMETER :: csice     = 2.100e3_dp ! specific heat capacity for ice
    REAL(dp), PARAMETER :: lambdas   = 2835000._dp ! latent heat of sublimation (J kg-1)

    ! Aerodynamic parameters, diffusivities, water density:
    REAL(dp), PARAMETER :: emsoil = 0.95_dp ! soil emissivity
    REAL(dp), PARAMETER :: Mw = 0.018016_dp! weight of 1 mol of water [kg]
    REAL(dp), PARAMETER :: gf = 1.0_dp    ! gravity factor for flow direction (usually 1 for straight down).
    REAL(dp), PARAMETER :: rmair     = 0.02897_dp         ! molecular wt: dry air (kg/mol

    ! Ice/Freezing constants
    REAL(dp), PARAMETER :: freezefac = 1.0_dp ! Steeper freezing curve factor: 1=normal, >1=steeper (e.g. 1.5-2.0)
    
    ! minium/maximum values
    REAL(dp), PARAMETER :: hmin      = -1.0e6_dp ! minimum matric head h (used by MF).
    REAL(dp), PARAMETER :: rhmin     = 0.05_dp   ! minimum relative humidity in soil and litter
    REAL(dp), PARAMETER :: snmin        = 0.005_dp ! depth of snowpack (m) without dedicated snow layer(s)
    REAL(dp), PARAMETER :: dpmaxr       = 0.5_dp
    REAL(dp), PARAMETER :: dtmax     = 3600._dp
    REAL(dp), PARAMETER :: dtmin     = 0.01_dp
    REAL(dp), PARAMETER :: dTLmax    = 30.0_dp
    REAL(dp), PARAMETER :: h0min        = -2.e-3_dp
    REAL(dp), PARAMETER :: h0max     = 0.005_dp
    REAL(dp), PARAMETER :: Smax      = 1.05_dp
    REAL(dp), PARAMETER :: dSmax     = 0.1_dp
    REAL(dp), PARAMETER :: dSmaxr    = 0.1_dp
    REAL(dp), PARAMETER :: dSfac        = 1.25_dp
    REAL(dp), PARAMETER :: dTsoilmax = 30.0_dp

    ! General constant
    REAL(dp), PARAMETER :: gravity   = 9.8086_dp ! gravitation constant [m/s2]
    REAL(dp), PARAMETER :: csol      = 0.0_dp    ! solute concentration (mol kg-1 soil water)
    REAL(dp), PARAMETER :: rgas      = 8.3143_dp  ! universal gas const  (J/mol/K)
    REAL(dp), PARAMETER :: rhow      = 1000.0_dp   ! denisty of water [kg/m3]

    ! numbers
    REAL(dp), PARAMETER :: zero      = 0.0_dp
    REAL(dp), PARAMETER :: half      = 0.5_dp
    REAL(dp), PARAMETER :: one       = 1.0_dp
    REAL(dp), PARAMETER :: two       = 2.0_dp
    REAL(dp), PARAMETER :: four      = 4.0_dp
    REAL(dp), PARAMETER :: thousand  = 1000._dp
    REAL(dp), PARAMETER :: e1        = 1.e-1_dp
    REAL(dp), PARAMETER :: e2        = 1.e-2_dp
    REAL(dp), PARAMETER :: e3        = 1.e-3_dp
    REAL(dp), PARAMETER :: e4        = 1.e-4_dp
    REAL(dp), PARAMETER :: e5        = 1.e-5_dp
    REAL(dp), PARAMETER :: e6        = 1.e-6_dp
    REAL(dp), PARAMETER :: e7        = 1.e-7_dp

    !###########################################################################
    ! CHANGE in input .nml file
    !###########################################################################
    
    !>> Boundary Conditions
    REAL(dp)             :: hbot
    real(dp), public, dimension(:), allocatable :: hbot2
    CHARACTER(LEN=20)    :: botbc
    ! CHARACTER(LEN=20)    :: botbc = "zero flux"
    ! CHARACTER(LEN=20)    :: botbc = "aquifer"
    ! CHARACTER(LEN=20)    :: botbc = "constant head"
    ! CHARACTER(LEN=20)    :: botbc = "seepage"

    !>> GW_Depth and Baseflow from Groundwater Model
    ! real(dp), public, dimension(:), allocatable :: L0_gwdepth
    ! real(dp), public, dimension(:), allocatable :: L0_gwbaseflow
    real(dp), public, dimension(:), allocatable :: L1_gwdepth
    real(dp), public, dimension(:), allocatable :: L1_gwbaseflow

    !>> Thermal conductivity of soil
    ! 0: Campbell (1985)
    ! 1: Van de Griend and O'Neill (1986)
    INTEGER(i4) :: ithermalcond

    ! 0: coupled calc
    ! 1: uncoupled energy (T) and moisture (S)
    INTEGER(i4) :: septs

    ! 0: no conditioning
    ! 1: condition columns
    ! 2: condition lines
    ! 3: condition first lines then columns
    INTEGER(i4) :: condition = 3

    ! heat advection by water
    ! 0: No Advection
    ! 1: Advection 
    INTEGER(i4) :: advection

END MODULE mo_sli_numbers