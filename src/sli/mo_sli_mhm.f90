MODULE mo_sli_mhm
    use mo_kind, only : i4, dp
    use mo_sli_numbers, only: zero, half, one, two, thousand, e5, e3, & 
                             L1_gwbaseflow, botbc
    use mo_common_mHM_mRM_variables, only: nTStepDay
    use mo_common_constants, only : eps_dp
    
    use mo_julian, only : dec2date
    use mo_canopy_interc, only : canopy_interc
    use mo_snow_accum_melt, only : snow_accum_melt

    implicit none
    private

    public :: upper_fluxes, soil_fluxes


CONTAINS

    SUBROUTINE upper_fluxes(& !>> Canopy Interception.........
                            mp, pet_calc, interc_max, prec_calc, & 
                            interc, throughfall, aet_canopy, &
                            !
                            !>> Snow melt or accumulation............
                            deg_day_incr, deg_day_max, &
                            c2TSTu, deg_day_noprec, temp_calc, temp_thresh, &
                            snowpack, deg_day, melt, prec_effect, rain, snow, &
                            !
                            !>> IMPERVIOUS COVER PROCESS
                            time, runoff_sealed, aet_sealed, frac_sealed, storage_sealed, &
                            water_thresh_sealed, evap_coeff)

        !************************************
        !    (1) Declaring Variable function
        !************************************
            implicit none 

            INTEGER(i4), INTENT(IN)                  :: mp

            real(dp), dimension(:), intent(inout)    :: pet_calc, prec_calc

            !>> Canopy Interception.........
            real(dp), dimension(:), intent(inout)    :: interc_max !> Maximum interception
            real(dp), dimension(:), intent(inout)    :: interc     !> Interception
            real(dp), dimension(:), intent(inout)    :: throughfall
            real(dp), dimension(:), intent(inout)    :: aet_canopy

            !>> Snow melt or accumulation............
            real(dp), dimension(:), intent(inout) :: deg_day_incr
            real(dp), dimension(:), intent(inout) :: deg_day_max
            real(dp), intent(in) :: c2TSTu ! unit conversion
            real(dp), dimension(:), intent(inout) :: deg_day_noprec
            real(dp), dimension(:), intent(inout) :: temp_calc
            real(dp), dimension(:), intent(inout) :: temp_thresh
            real(dp), dimension(:), intent(inout) :: snowpack
            real(dp), dimension(:), intent(inout) :: deg_day
            real(dp), dimension(:), intent(inout) :: melt, prec_effect, rain, snow

            !>> IMPERVIOUS COVER PROCESS
            real(dp), intent(in) :: time
            integer(i4) :: month ! Month of current day [1-12]
            real(dp), dimension(:), intent(inout) :: runoff_sealed
            real(dp), dimension(:), intent(inout) :: aet_sealed
            real(dp), dimension(:), intent(in) :: frac_sealed
            real(dp), dimension(:), intent(inout) :: storage_sealed
            real(dp), dimension(:), intent(inout) :: water_thresh_sealed
            real(dp), dimension(:), intent(in) :: evap_coeff

        !************************************
        !    (2) Declaring Variable internal
        !************************************
            INTEGER(i4) :: kk !loop
            real(dp) :: tmp   ! temporary variable for misc (miscellaneous) use

        !************************************
        !    (3) Calculate dec2date
        !************************************
            call dec2date(time, mm = month)

        !***************************************************************************
        !    (4) Calculate Canopy Interception, Snow, IMPERVIOUS COVER PROCESS
        !***************************************************************************
            runoff_sealed = 0.0_dp
            aet_sealed = 0.0_dp
            
            !$OMP parallel default(shared) private(kk, tmp, aet_sealed) 
            !$OMP do SCHEDULE(STATIC)
            do kk = 1,mp
                !>> Canopy Interception.........
                call canopy_interc(pet_calc(kk), interc_max(kk), prec_calc(kk), & ! Intent IN
                                interc(kk), &      ! Intent INOUT
                                throughfall(kk), & ! Intent INOUT
                                aet_canopy(kk))    ! Intent OUT
                
                !>> Snow melt or accumulation............
                call snow_accum_melt(deg_day_incr(kk), deg_day_max(kk)*c2TSTu, & ! Intent IN
                                    deg_day_noprec(kk)*c2TSTu, prec_calc(kk), temp_calc(kk), temp_thresh(kk), & ! Intent IN
                                    throughfall(kk), & ! Intent IN
                                    snowpack(kk), &             ! Intent INOUT
                                    deg_day(kk), &              ! Intent OUT
                                    melt(kk), &                 ! Intent OUT
                                    prec_effect(kk), &          ! Intent OUT
                                    rain(kk), &                 ! Intent OUT
                                    snow(kk))                   ! Intent OUT

                ! !>> IMPERVIOUS COVER PROCESS
                if (frac_sealed(kk) > 0.0_dp) then
                    tmp = storage_sealed(kk) + prec_effect(kk)
            
                    if (tmp > water_thresh_sealed(kk)) then
                        runoff_sealed(kk) = tmp - water_thresh_sealed(kk)
                        storage_sealed = water_thresh_sealed(kk)
                    else
                        runoff_sealed(kk) = 0.0_dp
                        storage_sealed(kk) = tmp
                    end if
            
                    ! aET from sealed area is propotional to the available water content
                    if(water_thresh_sealed(kk) .gt. eps_dp) then
                        aet_sealed = (pet_calc(kk) / evap_coeff(month) - aet_canopy(kk)) * &
                                     (storage_sealed(kk) / water_thresh_sealed(kk))
                        ! numerical problem
                        if (aet_sealed(kk) < 0.0_dp) aet_sealed(kk) = abs(0.0_dp)
                    else
                        aet_sealed(kk) = huge(1.0_dp)
                    end if
            
                    ! sealed storage updata
                    if (storage_sealed(kk) .gt. aet_sealed(kk)) then
                        storage_sealed(kk) = storage_sealed(kk) - aet_sealed(kk)
                    else
                        aet_sealed(kk) = storage_sealed(kk)
                        storage_sealed(kk) = 0.0_dp
                    end if
                end if
              
            end do
            !$OMP end do
            !$OMP end parallel

    END SUBROUTINE upper_fluxes

    SUBROUTINE soil_fluxes(& !>> AET Soil calculation
                            mp, n, froots, jarvis_thresh_c1, aet_soil, pet_calc, &
                            soil_moist, soil_moist_FC, wilting_point, soil_moist_sat, &
                            aet_canopy, L1_soilStressFactor, &
                            !
                            !>> Runoff Unsaturated Zone
                            c2TSTu, k1, kp, k0, &
                            alpha, karst_loss, &
                            infiltration, unsat_thresh, &
                            satStorage, unsatStorage, slow_interflow, fast_interflow, &
                            perc, fastInter_thresh, &
                            !>> Runoff Saturated Zone
                            k2, baseflow, &
                            !>> Total Runoff
                            fSealed1, runoff_sealed, total_runoff, &
                            !>> Neutron
                            horizon_depth, snowpack, interc, &
                            neutron_integral_AFast, & 
                            No_count, bulkDens, latticeWater, COSMICL3, neutrons)
        
        !************************************
        !    (1) Declaring Variable function
        !************************************
            use mo_kind, only : i4, dp
            use mo_sli_numbers, only: zero, half, one, two, thousand, e5, e3
            use mo_common_variables, only : processMatrix

            use mo_soil_moisture, only : feddes_et_reduction, jarvis_et_reduction
            use mo_runoff, only : L1_total_runoff, runoff_sat_zone, runoff_unsat_zone
            use mo_neutrons, only : COSMIC, DesiletsN0


            implicit none 

            INTEGER(i4), INTENT(IN)                  :: mp, n

            !>> AET Soil Calculation........................................
            real(dp),  dimension(:, :),        intent(inout)   :: froots
            real(dp),  dimension(:, :),        intent(inout)   :: aet_soil
            real(dp),  dimension(:, :),        intent(inout)   :: soil_moist, soil_moist_FC, wilting_point, soil_moist_sat
            real(dp),  dimension(:, :),        intent(inout)   :: L1_soilStressFactor

            real(dp), dimension(:),           intent(inout)    :: aet_canopy
            real(dp),  dimension(:),          intent(inout)    :: pet_calc
            real(dp),  dimension(:),          intent(inout)    :: jarvis_thresh_c1

            real(dp) :: soil_stress_factor !  PET reduction factor according to actual soil moisture

            !>> Runoff Unsaturated Zone........................................
            real(dp), intent(in) :: c2TSTu !unit conversion

            real(dp), dimension(:), intent(inout) :: k1
            real(dp), dimension(:), intent(inout) :: kp
            real(dp), dimension(:), intent(inout) :: k0
            real(dp), dimension(:), intent(inout) :: alpha 
            real(dp), dimension(:), intent(inout) :: karst_loss
            real(dp), dimension(:), intent(in) :: infiltration
            real(dp), dimension(:), intent(inout) :: unsat_thresh, fastInter_thresh
            real(dp), dimension(:), intent(inout) :: satStorage
            real(dp), dimension(:), intent(inout) :: unsatStorage
            real(dp), dimension(:), intent(inout) :: slow_interflow
            real(dp), dimension(:), intent(inout) :: fast_interflow
            real(dp), dimension(:), intent(inout) :: perc

            !>> Runoff Saturated Zone........................................
            real(dp), dimension(:), intent(inout) :: k2, baseflow

            !>> Total Runoff........................................
            real(dp), dimension(:), intent(inout) :: fSealed1, runoff_sealed, total_runoff

            !>> neutron count
            real(dp), dimension(:), intent(inout)    :: interc     !> Interception
            real(dp), dimension(:), intent(in) :: horizon_depth
            real(dp), dimension(:), intent(inout)   :: snowpack
            real(dp), dimension(:), intent(in) :: neutron_integral_AFast
            real(dp), dimension(:), intent(inout)   ::  No_count
            real(dp), dimension(:,:), intent(inout) ::  bulkDens
            real(dp), dimension(:,:), intent(inout) ::  latticeWater
            real(dp), dimension(:,:), intent(inout) ::  COSMICL3
            real(dp), dimension(:), intent(inout) :: neutrons

        !************************************
        !    (2) Declaring Variable internal
        !************************************
            INTEGER(i4) :: kk, hh !loop

            real(dp), dimension(:), allocatable :: unsatStorage_ori, interflow, storage_interflow
            real(dp), dimension(:,:), allocatable :: weightSM, lostSM

        !************************************
        !    (3) AET Soil Calculation
        !************************************
            aet_soil = zero

            !$OMP parallel default(shared) private(kk, hh, soil_stress_factor) 
            !$OMP do SCHEDULE(STATIC)
            do kk = 1,mp
              do hh = 1,n 

                ! AET calculation.................
                aet_soil(kk,hh) = pet_calc(kk) - aet_canopy(kk) ! First layer
                
                if (hh /= 1) aet_soil(kk,hh) = aet_soil(kk,hh) - sum(aet_soil(kk, 1:hh-1), mask = (aet_soil(kk, 1:hh-1) > 0.0_dp)) ! remaining layers

                select case(processMatrix(3, 1))
                ! FEDDES EQUATION: https://doi.org/10.1016/0022-1694(76)90017-2
                case(1, 4, 5, 7, 8 ,81, 82, 51, 71, 711, 712)
                    soil_stress_factor = feddes_et_reduction(soil_moist(kk,hh), soil_moist_FC(kk,hh), & 
                                                            wilting_point(kk,hh), froots(kk,hh))
                ! JARVIS EQUATION: https://doi.org/10.1016/0022-1694(89)90050-4
                case(2,3)
                    !!!!!!!!! INTRODUCING STRESS FACTOR FOR SOIL MOISTURE ET REDUCTION !!!!!!!!!!!!!!!!!
                    soil_stress_factor = jarvis_et_reduction(soil_moist(kk,hh), soil_moist_sat(kk,hh), wilting_point(kk,hh), &
                                        froots(kk,hh), jarvis_thresh_c1(kk))
                end select
                
                aet_soil(kk,hh) = aet_soil(kk,hh) * soil_stress_factor
                L1_soilStressFactor(kk,hh) = soil_stress_factor

                ! avoid numerical error
                if(aet_soil(kk,hh) < 0.0_dp) aet_soil(kk,hh) = 0.0_dp

                ! reduce SM state
                if(soil_moist(kk,hh) > aet_soil(kk,hh)) then
                    soil_moist(kk,hh) = soil_moist(kk,hh) - aet_soil(kk,hh)
                else
                    aet_soil(kk,hh) = soil_moist(kk,hh) - eps_dp
                    soil_moist(kk,hh) = eps_dp
                end if

                ! avoid numerical error of underflow
                if(soil_moist(kk,hh) < eps_dp) soil_moist(kk,hh) = eps_dp
              end do
            end do
            !$OMP end do
            !$OMP end parallel
        !************************************
        !    (4) Runoff Calculation
        !************************************
            select case(processMatrix(6, 1))
            case (1)
                !................................................
                ! Unsaturated Runoff Calculation 
                !................................................
                !$OMP parallel default(shared) private(kk)
                !$OMP do SCHEDULE(STATIC)
                do kk = 1, mp
                    call runoff_unsat_zone(c2TSTu / k1(kk), c2TSTu / kp(kk), c2TSTu / k0(kk), alpha(kk), karst_loss(kk), & ! Intent IN
                                            infiltration(kk), unsat_thresh(kk), & ! Intent IN
                                            satStorage(kk), unsatStorage(kk), & ! Intent INOUT
                                            slow_interflow(kk), fast_interflow(kk), perc(kk)) 
                end do
                !$OMP end do
                !$OMP end parallel

                !................................................
                ! Saturated Runoff Calculation
                !................................................
                !$OMP parallel default(shared) private(kk)
                !$OMP do SCHEDULE(STATIC)
                do kk = 1, mp
                    call runoff_sat_zone(c2TSTu / k2(kk), & ! Intent IN
                                        satStorage(kk), & ! Intent INOUT
                                        baseflow(kk)) ! Intent OUT 
                end do 
                !$OMP end do
                !$OMP end parallel

            case (2)
                allocate( unsatStorage_ori(size(unsatStorage)))
                allocate( interflow(size(unsatStorage)))
                allocate( storage_interflow(size(unsatStorage)))
                allocate( weightSM(mp,n))
                allocate( lostSM(mp,n))

                !$OMP parallel default(shared) private(kk, hh)
                !$OMP do SCHEDULE(STATIC)
                do kk = 1,mp
                    !...................................................
                    ! CALCULATE UNSAT STORAGE
                    !...................................................
                    unsatStorage(kk) = 0._dp
                    unsatStorage_ori(kk) = 0._dp
                    do hh = 1,n
                        unsatStorage(kk) = unsatStorage(kk) + soil_moist(kk, hh)
                    end do
                    unsatStorage_ori(kk) = unsatStorage(kk)

                    !...................................................
                    ! FAST & SLOW Interflow
                    !...................................................
                    fast_interflow(kk) = 0.0_dp
                    storage_interflow(kk) = 0.0_dp

                    if(unsatStorage(kk) > unsat_thresh(kk)) then
                        storage_interflow(kk) = unsatStorage(kk) - unsat_thresh(kk)

                        ! FAST INTERFLOW WITH THRESHOLD BEHAVIOUR
                        if(storage_interflow(kk) > fastInter_thresh(kk)) then 
                            fast_interflow(kk) = MIN(c2TSTu/k0(kk)*storage_interflow(kk), & 
                                                    (storage_interflow(kk) - eps_dp))
    
                            storage_interflow(kk) = storage_interflow(kk) - fast_interflow(kk)
                        end if

                        ! SLOW PERMANENT INTERFLOW
                        slow_interflow(kk) = 0.0_dp
                        if(storage_interflow(kk) > eps_dp) then
                            slow_interflow(kk) = min(c2TSTu/k1(kk)*(storage_interflow(kk)**(1.0_dp+alpha(kk))), &
                                                    (storage_interflow(kk) - eps_dp))
                        end if
                    end if

                    !...................................................
                    ! UPDATE theta
                    !...................................................
                    interflow(kk) = slow_interflow(kk)+fast_interflow(kk)
                    unsatStorage(kk) = unsatStorage(kk) - interflow(kk)

                    do hh = 1,n
                        weightSM(kk,hh) = (soil_moist(kk,hh)/unsatStorage_ori(kk))
                        lostSM(kk,hh) = interflow(kk)*weightSM(kk,hh)

                        soil_moist(kk,hh) = soil_moist(kk,hh) - lostSM(kk,hh)

                        if(soil_moist(kk,hh) < eps_dp) soil_moist(kk,hh) = eps_dp 
                    end do

                    !...................................................
                    ! PERCOLATION FROM SOIL LAYER TO THE SATURATED ZONE
                    !...................................................
                    perc(kk) = infiltration(kk)

                    !...................................................
                    ! SATURATED STORAGE
                    !...................................................
                    ! Taking into account for the KARSTIC aquifers
                    !*** karstic loss gain or loss if Karstic aquifer is present
                    satStorage(kk) = satStorage(kk) + perc(kk)*karst_loss(kk)

                    !**************************************************************************************
                    ! Baseflow
                    !**************************************************************************************
                    if (botbc=="groundwater") then
                        baseflow(kk) = L1_gwbaseflow(kk)/nTstepDay
                    else
                        if (satStorage(kk)  > 0.0_dp) then
                            baseflow(kk) = c2TSTu/k2(kk)*satStorage(kk)
                            satStorage(kk) = satStorage(kk) - baseflow(kk)
                        else
                            baseflow(kk) = 0.0_dp
                            satStorage(kk) = 0.0_dp
                        end if
                    end if

                end do
                !$OMP end do
                !$OMP end parallel

                deallocate(unsatStorage_ori)
                deallocate(weightSM)
                deallocate(lostSM)
                deallocate(interflow)
                deallocate(storage_interflow)

            END select
        !************************************
        !    (5) Total Runoff Calculation
        !************************************
            !$OMP parallel default(shared) private(kk)
            !$OMP do SCHEDULE(STATIC)
            do kk = 1, mp
                call L1_total_runoff(fSealed1(kk), fast_interflow(kk), slow_interflow(kk), baseflow(kk), & ! Intent IN
                                        runoff_sealed(kk), & ! Intent IN
                                        total_runoff(kk))
            end do
            !$OMP end do
            !$OMP end parallel
        
        !************************************
        !    (6) Neutron Count
        !************************************
            !$OMP parallel default(shared) private(kk)
            !$OMP do SCHEDULE(STATIC)
            do kk = 1, mp
                ! DESLET
                if ( processMatrix(10, 1) .EQ. 1 ) &
                    call DesiletsN0( soil_moist(kk,1:n-1),& ! Intent IN
                    horizon_depth(1:n-1),                  & ! Intent IN
                    bulkDens(kk,1:n-1),                     & ! Intent IN
                    latticeWater(kk,1:n-1), No_count(kk),    & ! Intent IN
                    neutrons(kk) )                            ! Intent INOUT

                ! COSMIC
                if ( processMatrix(10, 1) .EQ. 2 ) &
                    call COSMIC( soil_moist(kk,1:n-1), horizon_depth(1:n-1),&
                    neutron_integral_AFast(:), &  ! Intent IN
                    interc(kk), snowpack(kk),  &  ! Intent IN
                    No_count(kk), bulkDens(kk,1:n-1), &  ! Intent IN
                    latticeWater(kk,1:n-1), COSMICL3(kk,1:n-1),  & ! Intent IN
                    neutrons(kk)  ) ! Intent INOUT
            end do
            
            !$OMP end do
            !$OMP end parallel

    END SUBROUTINE soil_fluxes

END MODULE mo_sli_mhm